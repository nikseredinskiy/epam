package by.training.task.action;

import by.training.task.entity.Hardware;
import by.training.task.entity.InputHardware;
import by.training.task.entity.MultiMedHardware;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nik on 22.10.2015.
 */
public class HardwareDOMBuilder {



    private Set<Hardware> hardwares;
    private DocumentBuilder documentBuilder;

    public HardwareDOMBuilder(){
        this.hardwares = new HashSet<Hardware>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
        }
    }

    public Set<Hardware> getHardwares(){
        return hardwares;
    }

    public void buildSetHardwares(String fileName){
        Document doc = null;
        try {
            doc = documentBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList hardwaresList = root.getElementsByTagName("hardware");
            NodeList inputHardwareList = root.getElementsByTagName("inputhardware");
            NodeList multiMedHardwareList = root.getElementsByTagName("multimedhardware");

            for (int i = 0; i < hardwaresList.getLength(); i++) {
                Element hardwareElement = (Element) hardwaresList.item(i);
                Hardware hardware = buildHardware(hardwareElement, "hardware");
                hardwares.add(hardware);
            }
            for (int i = 0; i < multiMedHardwareList.getLength(); i++) {
                Element multiMedHardwareElement = (Element) multiMedHardwareList.item(i);
                Hardware hardware = buildHardware(multiMedHardwareElement, "multimedhardware");
                hardwares.add(hardware);
            }
            for (int i = 0; i < inputHardwareList.getLength(); i++) {
                Element inputHardwareElement = (Element) inputHardwareList.item(i);
                Hardware hardware = buildHardware(inputHardwareElement, "inputhardware");
                hardwares.add(hardware);
            }


        }catch(SAXException e){
        }catch(IOException e){
        }
    }

    private Hardware buildHardware(Element hardwareElement, String hardwareType){
        Hardware hardware;
        switch (hardwareType){
            case "hardware":
                hardware = new Hardware();
                break;
            case "inputhardware":
                hardware = new InputHardware();
                break;
            case "multimedhardware":
                hardware = new MultiMedHardware();
                break;
            default:
                hardware = new Hardware();
                break;
        }

        hardware.setId(hardwareElement.getAttribute("id"));
        hardware.setName(getElementTextContent(hardwareElement, "name"));
        hardware.setOrigin(getElementTextContent(hardwareElement, "origin"));
        hardware.setPrice(Integer.parseInt(getElementTextContent(hardwareElement, "price")));
        hardware.setCritical(Boolean.valueOf(getElementTextContent(hardwareElement, "critical")));

        Hardware.Type type = hardware.getType();
        Element typeElement = (Element) hardwareElement.getElementsByTagName("type").item(0);
        type.setPeripheral(Boolean.valueOf(getElementTextContent(typeElement, "peripheral")));
        type.setEnergyConsuming(Integer.parseInt(getElementTextContent(typeElement, "energy-consuming")));
        type.setHasCooler(Boolean.valueOf(getElementTextContent(typeElement, "has-cooler")));
        type.setPortType(getElementTextContent(typeElement, "port-type"));

        if("inputhardware".equals(hardwareType)){
            ((InputHardware) hardware).setKeyCount(Integer.parseInt(getElementTextContent(hardwareElement, "keys-count")));
            ((InputHardware) hardware).setHasAdditionalKey(Boolean.parseBoolean(getElementTextContent(hardwareElement, "has-additional-keys")));
            ((InputHardware) hardware).setHasNumPad(Boolean.parseBoolean(getElementTextContent(hardwareElement, "has-numpad")));
        }

        if("multimedhardware".equals(hardwareType)){
            ((MultiMedHardware) hardware).setSpeakersCount(Integer.parseInt(getElementTextContent(hardwareElement, "speakers-count")));
            ((MultiMedHardware) hardware).setHasSub(Boolean.valueOf(getElementTextContent(hardwareElement, "has-sub")));
            ((MultiMedHardware) hardware).setHasDolby(Boolean.valueOf(getElementTextContent(hardwareElement, "has-dolby")));
        }

        hardware.setType(type);

        return hardware;
    }

    private static String getElementTextContent(Element element, String elementName){
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();

        return text;
    }
}
