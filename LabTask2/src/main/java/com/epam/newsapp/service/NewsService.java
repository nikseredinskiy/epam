package com.epam.newsapp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.dao.NewsDAO;
import com.epam.newsapp.entity.News;
import com.epam.newsapp.entity.SearchCriteria;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.exception.ServiceExeption;

@Service
public class NewsService {
	
	@Autowired
	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public long insertNews(News news) throws ServiceExeption {
		long id;
		try {
			id = this.newsDAO.insertNews(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
		
		return id;
	}

	public long updateNews(News news) throws ServiceExeption {
		news.setModifitationDate(new Date());
		try {
			return this.newsDAO.updateNews(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while update()", e);
		}
	}

	
	public void deleteNews(long newsId) throws ServiceExeption {
		try {
			this.newsDAO.deleteNews(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}

	public List<News> getAllNews() throws ServiceExeption {
		try {
			return this.newsDAO.getAllNews();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllNews()", e);
		}
	}

	public News getNews(long newsId) throws ServiceExeption {
		try {
			return this.newsDAO.getNews(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNews()", e);
		}
	}

	public List<News> getNewsByCommCount() throws ServiceExeption {
		try {
			return this.newsDAO.getNewsByCommCount();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsByCommCount()", e);
		}
	}

	public int countAllNews() throws ServiceExeption {
		try {
			return this.newsDAO.countAllNews();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while countAllNews()", e);
		}
	}
	
	public List<News> getNewsBySeacrh(SearchCriteria searchCriteria, long pageNum, long newsOnPage) throws ServiceExeption{
		try {
			return this.newsDAO.getNewsBySeacrh(searchCriteria, pageNum, newsOnPage);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsBySearch()", e);
		}
	}
	
	public int countNewsBySeacrh(SearchCriteria searchCriteria) throws ServiceExeption{
		try {
			return this.newsDAO.countNewsBySeacrh(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while countNewsBySearch()", e);
		}
	}
	
	public List<News> getNewsOnPage(long pageNum, long newsOnPage) throws ServiceExeption{
		try {
			return this.newsDAO.getNewsOnPage(pageNum, newsOnPage);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsOnPage()", e);
		}
	}

}
