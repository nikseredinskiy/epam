package com.epam.newsappclient.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsappclient.entity.NewsVO;
import com.epam.newsappclient.exception.ServiceExeption;
import com.epam.newsappclient.service.AuthorService;
import com.epam.newsappclient.service.NewsService;
import com.epam.newsappclient.service.NewsVOService;
import com.epam.newsappclient.service.TagService;
import com.epam.newsappclient.util.Parameter;

@Component
public class NewsListCommand implements ActionCommand{
	
	@Autowired
	NewsVOService newsVOService;
	
	@Autowired
	NewsService newsService;
	
	@Autowired
	AuthorService authorService;
	
	@Autowired
	TagService tagService;

	private Logger logger = Logger.getLogger(NewsListCommand.class);
	
	public String execute(HttpServletRequest request) {
		List<NewsVO> newsList = new ArrayList<NewsVO>();
		double newsCount;
		try {
			Long page = Long.parseLong(request.getParameter(Parameter.PAGE));
			
			newsList = newsVOService.getNewsOnPage(page, Parameter.NEWS_ON_PAGE);
			newsCount = (double) newsService.countAllNews();
			
			request.setAttribute(Parameter.NEWS_LIST, newsList);
			request.setAttribute(Parameter.AUTHORS, authorService.getAllAuthors());
			request.setAttribute(Parameter.TAGS, tagService.getAllTags());
			request.setAttribute(Parameter.OPERATION, Parameter.VIEW_NEWS);
			request.setAttribute(Parameter.PAGE_COUNT, Math.ceil(newsCount / Parameter.NEWS_ON_PAGE));
			request.setAttribute(Parameter.CURRENT_PAGE, page);
		} catch (ServiceExeption e) {
			logger.error("Service Exception in NewsListCommand " + e, e);
		}
		
		return Parameter.NEWS_LIST_PAGE;
	}
	
}
