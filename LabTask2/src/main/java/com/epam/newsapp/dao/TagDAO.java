package com.epam.newsapp.dao;

import java.util.List;

import com.epam.newsapp.entity.Tag;
import com.epam.newsapp.exception.DAOException;

public interface TagDAO {
	/**
	 * @param tag
	 * @return generated tag id
	 * @throws DAOException
	 */
	long createTag(Tag tag) throws DAOException;
	
	Long update(Tag tag) throws DAOException;

	/**
	 * Insert tag and news link in NEWS_TAG
	 * 
	 * @param newsId
	 * @param tags
	 * @throws DAOException
	 */
	void insertNewsTag(long newsId, List<Tag> tags) throws DAOException;
	
	void updateNewsTag(long newsId, List<Tag> tags) throws DAOException;

	/**
	 * Delete link between tag and news from NEWS_TAG
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsTags(long newsId) throws DAOException;
	
	void deleteNTByTagId(Long tagId) throws DAOException;
	
	void delete(Long tagId) throws DAOException;

	/**
	 * @param newsId
	 * @return List of tags
	 * @throws DAOException
	 */
	List<Tag> getNewsTag(long newsId) throws DAOException;
	
	/**
	 * 
	 * @return List of all tags
	 * @throws DAOException
	 */
	List<Tag> getAllTags() throws DAOException;

}
