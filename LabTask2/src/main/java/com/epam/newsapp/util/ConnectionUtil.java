package com.epam.newsapp.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class ConnectionUtil {

	private static Logger logger = Logger.getLogger(ConnectionUtil.class);

	public static void close(ResultSet rs, PreparedStatement ps,
			Connection conn, DataSource ds) {
		try {
			rs.close();
		} catch (SQLException e) {
			logger.error("Error while close(rs, ps, conn)", e);
		}
		close(ps, conn, ds);
	}

	public static void close(ResultSet rs, Statement st, Connection conn,
			DataSource ds) {
		try {
			rs.close();
		} catch (SQLException e) {
			logger.error("Error while close(rs, st, conn)", e);
		}
		close(st, conn, ds);
	}

	public static void close(PreparedStatement ps, Connection conn,
			DataSource ds) {
		try {
			ps.close();
		} catch (SQLException e) {
			logger.error("Error while close(ps, conn)", e);
		}
		close(conn, ds);
	}

	public static void close(Statement st, Connection conn, DataSource ds) {
		try {
			st.close();
		} catch (SQLException e) {
			logger.error("Error while close(st, conn)", e);
		}
		close(conn, ds);
	}

	public static void close(Connection conn, DataSource ds) {
		DataSourceUtils.releaseConnection(conn, ds);
	}
}
