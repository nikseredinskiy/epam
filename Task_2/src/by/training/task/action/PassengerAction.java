package by.training.task.action;

import by.training.task.entity.Passenger;
import by.training.task.entity.WaitingHall;
import org.apache.log4j.Logger;

/**
 * Created by Nik on 30.09.2015.
 */
public class PassengerAction {
    private Passenger passenger;
    private WaitingHall waitingHall;
    private static Logger logger = Logger.getLogger(PassengerAction.class);

    public PassengerAction(Passenger passenger){
        this.passenger = passenger;
        this.waitingHall = passenger.getWaitingHall();
    }

    public void goWaitingHall(){
        logger.debug("Passenger " + passenger.getId() + " from " + passenger.getPlainId() + " goes to Waiting hall");

        waitingHall.addPassenger(passenger);
    }

    public void boarding(){
        logger.debug("Passenger " + passenger.getId() + " from " + passenger.getPlainId() +  " is waiting " + passenger.getTicket().getPlainId() + " plain");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onPlain(){
        waitingHall.removePassenger(passenger);
        passenger.setState(Passenger.State.LEFT);
    }

    public void leaving(){
        logger.debug("Passenger " + passenger.getId() + " from " + passenger.getPlainId() + " leave airport");
        passenger.setState(Passenger.State.LEFT);
    }

}
