package by.training.task.controller;

import by.training.task.action.HardwareDOMBuilder;
import by.training.task.action.HardwareSAXBuilder;
import by.training.task.action.HardwareStAXBuilder;
import by.training.task.action.ParseAction;
import by.training.task.entity.Hardware;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Created by Nik on 26.10.2015.
 */

@WebServlet("/hardwares")
public class HardwareListServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParseAction parseAction = new ParseAction();
        String type = req.getParameter("parseType");
        String path = getServletContext().getRealPath("/data/hardware.xml");
        Set<Hardware> hardwares;

        if (type == null) {
            hardwares = null;
        } else {
            req.setAttribute("type", type.toUpperCase());
            hardwares = parseAction.parseXML(type, path);
        }

        req.setAttribute("hardwares", hardwares);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/hardware-list.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
