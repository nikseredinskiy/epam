package com.epam.task.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;







import com.epam.task.dao.CommentDAO;
import com.epam.task.entity.Comment;
import com.epam.task.exception.DAOException;
import com.epam.task.util.ConnectionUtil;

@Repository
public class CommentDAOImpl implements CommentDAO{
	
	@Autowired
	private DataSource dataSource;
	
	private final String SQL_INSERT = "INSERT INTO COMMENTS VALUES(COMMENT_ID_SEQ.NEXTVAL,?,?,SYSDATE)";
	private final String SQL_DELETE_BY_ID = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final String SQL_DELETE = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private final String SQL_GET_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";
	private final String SQL_UPDATE = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ? WHERE COMMENT_ID = ?";
	
	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	public Long add(Comment comment) throws DAOException{
		Long id = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT,new String [] {"COMMENT_ID"});
			ps.setLong(1, comment.getNewsID());
			ps.setString(2, comment.getCommentText());
			
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while add()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
	}
	
	

	public Long update(Comment comment) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setLong(1, comment.getNewsID());
			ps.setString(2, comment.getCommentText());
			ps.setLong(3, comment.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while updateComment()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return comment.getId();
	}

	public void delete(Comment comment) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE_BY_ID);
			ps.setLong(1, comment.getId());
						
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);

		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsComments(Long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, newsId);
						
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteNewsComment()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public List<Comment> getNewsComments(Long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> comments = new ArrayList<Comment>();

		try {
			Comment comment = null;
			
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				comment = new Comment(rs.getLong("COMMENT_ID"), rs.getLong("NEWS_ID"), rs.getString("COMMENT_TEXT"), rs.getDate("CREATION_DATE"));
				
				comments.add(comment);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsComments()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return comments;
	}

}
