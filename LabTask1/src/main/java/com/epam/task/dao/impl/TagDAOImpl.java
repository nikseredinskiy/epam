package com.epam.task.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;






import com.epam.task.dao.TagDAO;
import com.epam.task.entity.Tag;
import com.epam.task.exception.DAOException;
import com.epam.task.util.ConnectionUtil;

@Repository
public class TagDAOImpl implements TagDAO{
	
	@Autowired
	private DataSource dataSource;
	
	private final String SQL_CREATE = "INSERT INTO TAG VALUES(TAG_ID_SEQ.NEXTVAL,?)";
	private final String SQL_INSERT = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?,?)";
	private final String SQL_DELETE = "DELETE FROM NEWS_TAG WHERE NEWS_TAG.NEWS_ID = ?";
	private final String SQL_GET_BY_NEWS_ID = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM NEWS_TAG LEFT JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_ID = ?";
	private final String SQL_UPDATE = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	private final String SQL_DELETE_BY_ID = "DELETE FROM TAG WHERE TAG_ID = ?";
	
	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}
	
	public Long create(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long id = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_CREATE, new String [] {"TAG_ID"});

			ps.setString(1, tag.getName());
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while create()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
		
	}
	
	
	public Long update(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while updateTag()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return tag.getId();	}

	public void delete(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE_BY_ID);
			ps.setLong(1, tag.getId());
						
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteTag()", e);

		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}

	public void insertNewsTag(Long newsId, List<Tag> tags) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT);
						
			for (Iterator<Tag> iterator = tags.iterator(); iterator.hasNext();) {
				Tag tag = (Tag) iterator.next();
				ps.setLong(1, newsId);
				ps.setLong(2, tag.getId());
				ps.executeUpdate();
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsTags(Long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
						
			ps.setLong(1, newsId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteNewsTag()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public List<Tag> getNewsTag(Long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Tag> tags = new ArrayList<Tag>();

		try {
			Tag tag = null;
			
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				tag = new Tag(rs.getLong("TAG_ID"), rs.getString("TAG_NAME"));
				
				tags.add(tag);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsTag()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return tags;
	}

}
