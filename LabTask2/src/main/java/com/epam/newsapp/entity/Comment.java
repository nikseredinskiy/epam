package com.epam.newsapp.entity;

import java.util.Date;

public class Comment {
	private Long id;
	private Long newsID;
	private String commentText;
	private Date creationDate;
	
	public Comment(){
	
	}
	
	public Comment(Long id, Long newsID, String commentText, Date creationDate) {
		super();
		this.id = id;
		this.newsID = newsID;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}
	
	public Comment(Long newsID, String commentText, Date creationDate) {
		super();
		this.newsID = newsID;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsID() {
		return newsID;
	}

	public void setNewsID(Long newsID) {
		this.newsID = newsID;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", newsID=" + newsID + ", commentText="
				+ commentText + ", creationDate=" + creationDate + "]";
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsID == null) ? 0 : newsID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsID == null) {
			if (other.newsID != null)
				return false;
		} else if (!newsID.equals(other.newsID))
			return false;
		return true;
	}
	
	
			
}
