package com.epam.newsappclient.entity;

import java.util.List;

public class SearchCriteria {
	private Long authorId;
	private List<Long> tagId;
	
	public SearchCriteria(){
		
	}
	
	public SearchCriteria(Long authorId, List<Long> tagId) {
		super();
		this.authorId = authorId;
		this.tagId = tagId;
	}
	
	public SearchCriteria(List<Long> tagId) {
		super();
		this.tagId = tagId;
	}

	public SearchCriteria(Long authorId) {
		super();
		this.authorId = authorId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagId() {
		return tagId;
	}

	public void setTagId(List<Long> tagId) {
		this.tagId = tagId;
	}

	
}
