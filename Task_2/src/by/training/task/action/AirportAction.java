package by.training.task.action;

import by.training.task.entity.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Semaphore;

/**
 * Created by Nik on 29.09.2015.
 */
public class AirportAction {
    private static Airport airport;
    private static Logger logger = Logger.getLogger(AirportAction.class);

    public AirportAction(Airport airport){
        this.airport = airport;
    }

    public void launchAirport() throws InterruptedException {
        launchPlain(airport.getPlains());
    }

    public void launchPlain(ArrayList<Plain> plains){
        for(Plain plain:plains){
            logger.debug("Plain " + plain.getId() + " arrived");
            ArrayList<Terminal> terminals = airport.getTerminals();

            Iterator<Terminal> terminalIterator = terminals.iterator();
            boolean isFind = false;
            Terminal terminal;

            while(!isFind){
                terminal = terminalIterator.next();
                Semaphore terminalSemaphore = terminal.getSemaphore();
                if(terminalSemaphore.tryAcquire()){
                    plain.setTerminal(terminal);
                    isFind = true;
                }else{
                    if(!terminalIterator.hasNext()){
                        terminalIterator = terminals.iterator();
                    }
                }
            }
            new Thread(plain).start();
        }
    }

    public static void landingPassenger(Plain plain){
        WaitingHall waitingHall = airport.getWaitingHall();
        ArrayList<Passenger> passengers = plain.getPassengers();

        for(Passenger passenger:passengers){
            if(passenger.getId()%2 == 0){
                passenger.setState(Passenger.State.LEAVING);
            } else {
                passenger.setState(Passenger.State.WAITING);
                passenger.setWaitingHall(waitingHall);
            }
            new Thread(passenger).start();
        }

        plain.removePassengers();
    }

    public static void boardingPassenger(Plain plain){
        WaitingHall waitingHall = airport.getWaitingHall();
        ArrayList<Passenger> passengers = waitingHall.getPassengers();

        for(Passenger passenger:passengers){
            if(passenger.getTicket().getPlainId() == plain.getId()){
                passenger.setPlainId(plain.getId());
                plain.addPassenger(passenger);
                passenger.setState(Passenger.State.ON_PLAIN);
            }
        }
    }
}
