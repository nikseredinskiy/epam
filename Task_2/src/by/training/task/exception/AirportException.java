package by.training.task.exception;

/**
 * Created by Nik on 07.10.2015.
 */
public class AirportException extends Exception {
    public AirportException() {
    }

    public AirportException(String message) {
        super(message);
    }

    public AirportException(String message, Throwable cause) {
        super(message, cause);
    }

    public AirportException(Throwable cause) {
        super(cause);
    }

    public AirportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
