package com.epam.task.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.task.entity.Author;
import com.epam.task.entity.Comment;
import com.epam.task.entity.News;
import com.epam.task.entity.NewsVO;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.entity.Tag;

public class TestUtil {

	public static News createNews(Long id) {
		News news = new News(id, "News" + id + " Title", "News" + id + " STxt",
				"News" + id + " FTxt", new Date(), new Date());

		return news;
	}

	public static Author createAuthor(Long id) {
		Author author = new Author(id, "Author" + id, null);

		return author;
	}

	public static Comment createComment(Long id, Long newsId) {
		Comment comment = new Comment(id, newsId, "comment" + id, new Date());

		return comment;
	}

	public static Tag createTag(Long id) {
		Tag tag = new Tag(id, "tag" + id);

		return tag;
	}

	public static SearchCriteria createSearchCriteria(Long id) {
		Author author1 = createAuthor(id);
		SearchCriteria searchCriteria = new SearchCriteria(author1.getId());

		return searchCriteria;
	}

	public static NewsVO createNewsVO(Long id) {
		News news1 = new News(id, "News" + id + " Title",
				"News" + id + " STxt", "News" + id + " FTxt", new Date(),
				new Date());

		Tag tag1 = createTag(id);
		Tag tag2 = createTag(id + 1);

		List<Tag> tagList1 = new ArrayList<Tag>();
		tagList1.add(tag1);
		tagList1.add(tag2);

		Author author1 = createAuthor(id);

		Comment comm1 = createComment(1L, id);
		Comment comm2 = createComment(2L, id);
		List<Comment> commList1 = new ArrayList<Comment>();
		commList1.add(comm1);
		commList1.add(comm2);

		NewsVO newsVO1 = new NewsVO(news1, author1, tagList1, commList1);

		return newsVO1;
	}

}
