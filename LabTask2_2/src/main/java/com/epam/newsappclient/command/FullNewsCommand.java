package com.epam.newsappclient.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsappclient.entity.NewsVO;
import com.epam.newsappclient.exception.ServiceExeption;
import com.epam.newsappclient.service.NewsVOService;
import com.epam.newsappclient.util.Parameter;

public class FullNewsCommand implements ActionCommand {

	@Autowired
	NewsVOService newsVOService;
	
	private Logger logger = Logger.getLogger(FullNewsCommand.class);

	public String execute(HttpServletRequest request) {
		NewsVO news = null;
		try {
			news = newsVOService.getNews(Long.parseLong(request.getParameter(Parameter.NEWS_ID)));
			request.setAttribute(Parameter.NEWS_VO, news);
			request.setAttribute(Parameter.CURRENT_PAGE, request.getParameter(Parameter.PAGE));
		} catch (ServiceExeption e) {
			logger.error("Service Exception in FullNewsCommand " + e, e);
		}
		return Parameter.NEWS_PAGE;
	}

}
