--------------------------------------------------------
--  File created - Friday-March-04-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence AUTHOR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NIK"."AUTHOR_ID_SEQ"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 140 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COMMENT_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NIK"."COMMENT_ID_SEQ"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 140 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NEWS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NIK"."NEWS_ID_SEQ"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 160 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAG_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NIK"."TAG_ID_SEQ"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 120 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USER_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NIK"."USER_ID_SEQ"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 100 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE "NIK"."AUTHOR" 
   (	"AUTHOR_ID" NUMBER(20,0), 
	"AUTHOR_NAME" VARCHAR2(30 BYTE), 
	"EXPIRED" TIMESTAMP (6)
   );
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "NIK"."COMMENTS" 
   (	"COMMENT_ID" NUMBER(20,0), 
	"NEWS_ID" NUMBER(20,0), 
	"COMMENT_TEXT" VARCHAR2(100 BYTE), 
	"CREATION_DATE" TIMESTAMP (6)
   );
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "NIK"."NEWS" 
   (	"NEWS_ID" NUMBER(20,0), 
	"TITLE" VARCHAR2(30 BYTE), 
	"SHORT_TEXT" VARCHAR2(100 BYTE), 
	"FULL_TEXT" VARCHAR2(2000 BYTE), 
	"CREATION_DATE" TIMESTAMP (6) DEFAULT NULL, 
	"MODIFICATION_DATE" DATE DEFAULT NULL
   );
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE "NIK"."NEWS_AUTHOR" 
   (	"NEWS_ID" NUMBER(20,0), 
	"AUTHOR_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE "NIK"."NEWS_TAG" 
   (	"NEWS_ID" NUMBER(20,0), 
	"TAG_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "NIK"."ROLES" 
   (	"USER_ID" NUMBER(20,0), 
	"ROLE_NAME" VARCHAR2(50 BYTE)
   );
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "NIK"."TAG" 
   (	"TAG_ID" NUMBER(20,0), 
	"TAG_NAME" VARCHAR2(30 BYTE)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "NIK"."USERS" 
   (	"USER_ID" NUMBER(20,0), 
	"USER_NAME" VARCHAR2(50 BYTE), 
	"LOGIN" VARCHAR2(30 BYTE), 
	"PASSWORD" VARCHAR2(30 BYTE)
   );

--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NIK"."NEWS_PK" ON "NIK"."NEWS" ("NEWS_ID");
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NIK"."COMMENTS_PK" ON "NIK"."COMMENTS" ("COMMENT_ID");
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NIK"."AUTHOR_PK" ON "NIK"."AUTHOR" ("AUTHOR_ID");
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NIK"."TAG_PK" ON "NIK"."TAG" ("TAG_ID");
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NIK"."USERS_PK" ON "NIK"."USERS" ("USER_ID");
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "NIK"."TAG" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID");
  ALTER TABLE "NIK"."TAG" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "NIK"."TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "NIK"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID");
  ALTER TABLE "NIK"."COMMENTS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NIK"."COMMENTS" MODIFY ("COMMENT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NIK"."COMMENTS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NIK"."COMMENTS" MODIFY ("COMMENT_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "NIK"."ROLES" MODIFY ("ROLE_NAME" NOT NULL ENABLE);
  ALTER TABLE "NIK"."ROLES" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "NIK"."NEWS_AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "NIK"."NEWS_AUTHOR" MODIFY ("NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "NIK"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USER_ID");
  ALTER TABLE "NIK"."USERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "NIK"."USERS" MODIFY ("LOGIN" NOT NULL ENABLE);
  ALTER TABLE "NIK"."USERS" MODIFY ("USER_NAME" NOT NULL ENABLE);
  ALTER TABLE "NIK"."USERS" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "NIK"."NEWS_TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

  ALTER TABLE "NIK"."AUTHOR" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID");
  ALTER TABLE "NIK"."AUTHOR" MODIFY ("AUTHOR_NAME" NOT NULL ENABLE);
  ALTER TABLE "NIK"."AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "NIK"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID");
  ALTER TABLE "NIK"."NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NIK"."NEWS" MODIFY ("FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NIK"."NEWS" MODIFY ("SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NIK"."NEWS" MODIFY ("TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "NIK"."COMMENTS" ADD CONSTRAINT "NEWS_ID" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "NIK"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "NIK"."NEWS_AUTHOR" ADD CONSTRAINT "AUTHOR_FK" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES "NIK"."AUTHOR" ("AUTHOR_ID") ON DELETE SET NULL ENABLE;
  ALTER TABLE "NIK"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_FK" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "NIK"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "NIK"."NEWS_TAG" ADD CONSTRAINT "NEWS_FKEY" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "NIK"."NEWS" ("NEWS_ID") ENABLE;
  ALTER TABLE "NIK"."NEWS_TAG" ADD CONSTRAINT "TAG_FKEY" FOREIGN KEY ("TAG_ID")
	  REFERENCES "NIK"."TAG" ("TAG_ID") ON DELETE SET NULL ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "NIK"."ROLES" ADD CONSTRAINT "USER_FKEY" FOREIGN KEY ("USER_ID")
	  REFERENCES "NIK"."USERS" ("USER_ID") ENABLE;
