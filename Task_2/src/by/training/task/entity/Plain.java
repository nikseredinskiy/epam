package by.training.task.entity;

import by.training.task.action.PlainAction;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Nik on 29.09.2015.
 */
public class Plain implements Runnable{
    public enum State{ARRIVING, BOARDING, AT_TERMINAL, LEAVING, LEAVE}

    private int id;
    private int capacity;
    private Terminal terminal;
    private State state = State.ARRIVING;
    private ArrayList<Passenger> passengers = new ArrayList<Passenger>();
    private Lock lock = new ReentrantLock();
    private static Logger logger = Logger.getLogger(Plain.class);

    public Plain(){

    }

    public Plain(int id, int capacity) {
        this.id = id;
        this.capacity = capacity;
    }

    public int getId() {
        return id;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public int getCapacity() {
        return capacity;
    }

    public void addPassenger(Passenger passenger){
        lock.lock();
        this.passengers.add(passenger);
        lock.unlock();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void removePassengers(){
        this.passengers.clear();
    }

    @Override
    public void run() {
        PlainAction plainAction = new PlainAction(this);

        while(state != State.LEAVE){
            switch (state) {
                case ARRIVING:
                    plainAction.arriving();
                    break;
                case AT_TERMINAL:
                    plainAction.landed();
                    break;
                case BOARDING:
                    plainAction.boarding();
                    break;
                case LEAVING:
                    plainAction.leaving();
                    break;
                default:
                    logger.error("Something wrong");
                    break;
            }
        }
    }
}
