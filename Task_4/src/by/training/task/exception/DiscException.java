package by.training.task.exception;

/**
 * Created by Nik on 22.09.2015.
 */
public class DiscException extends Exception {
    public DiscException() {
    }

    public DiscException(String message) {
        super(message);
    }

    public DiscException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiscException(Throwable cause) {
        super(cause);
    }

    public DiscException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

