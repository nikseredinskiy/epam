package by.training.task.action;

import by.training.task.entity.Hardware;
import by.training.task.entity.InputHardware;
import by.training.task.entity.MultiMedHardware;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import org.xml.sax.Attributes;

/**
 * Created by Nik on 20.10.2015.
 */
public class HardwareHandler extends DefaultHandler {
    private Set<Hardware> hardwares;
    private Hardware current = null;
    private HardwareEnum currentEnum = null;
    private EnumSet<HardwareEnum> withText;

    public HardwareHandler(){
        hardwares = new HashSet<Hardware>();
        withText = EnumSet.range(HardwareEnum.NAME, HardwareEnum.CRITICAL);

    }

    public Set<Hardware> getHardwares(){
        return hardwares;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs){
        switch (localName){
            case "hardware":
                current = new Hardware();
                current.setId(attrs.getValue(0));
                break;
            case "multimedhardware":
                current = new MultiMedHardware();
                current.setId(attrs.getValue(0));
                break;
            case "inputhardware":
                current = new InputHardware();
                current.setId(attrs.getValue(0));
                break;
            default:
                HardwareEnum temp = HardwareEnum.valueOf(localName.toUpperCase().replace('-', '_'));
                if(withText.contains(temp)){
                    currentEnum = temp;
                }
        }
    }

    public void endElement(String uri, String localName, String qName){
        if("hardware".equals(localName) || "multimedhardware".equals(localName) || "inputhardware".equals(localName)){
            hardwares.add(current);
        }
    }

    public void characters(char[] ch, int start, int length){
        String s = new String(ch, start, length).trim();
        if(currentEnum != null){
            switch (currentEnum){
                case NAME:
                    current.setName(s);
                    break;
                case ORIGIN:
                    current.setOrigin(s);
                    break;
                case PRICE:
                    current.setPrice(Integer.parseInt(s));
                    break;
                case PERIPHERAL:
                    current.getType().setPeripheral(Boolean.valueOf(s));
                    break;
                case ENERGY_CONSUMING:
                    current.getType().setEnergyConsuming(Integer.parseInt(s));
                    break;
                case HAS_COOLER:
                    current.getType().setHasCooler(Boolean.valueOf(s));
                    break;
                case PORT_TYPE:
                    current.getType().setPortType(s);
                    break;
                case CRITICAL:
                    current.setCritical(Boolean.valueOf(s));
                    break;
                case SPEAKERS_COUNT:
                    ((MultiMedHardware) current).setSpeakersCount(Integer.parseInt(s));
                    break;
                case HAS_SUB:
                    ((MultiMedHardware) current).setHasSub(Boolean.valueOf(s));
                    break;
                case HAS_DOLBY:
                    ((MultiMedHardware) current).setHasDolby(Boolean.valueOf(s));
                    break;
                case KEYS_COUNT:
                    ((InputHardware) current).setKeyCount(Integer.parseInt(s));
                    break;
                case HAS_NUMPAD:
                    ((InputHardware) current).setHasNumPad(Boolean.valueOf(s));
                    break;
                case HAS_ADDITIONAL_KEYS:
                    ((InputHardware) current).setHasAdditionalKey(Boolean.valueOf(s));
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }

        currentEnum = null;
    }
}
