<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 26.10.2015
  Time: 5:54
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hardwares List</title>
</head>
<body>
<form action="/hardwares" method="post">
    <select name="parseType">
        <option value="sax">SAX</option>
        <option value="dom">DOM</option>
        <option value="stax">StAX</option>
    </select>
    <input type="submit" value="Parse">
    <c:if test="${not empty type}">
        Parsed with ${type} parser
    </c:if>
</form>
<c:if test="${not empty hardwares}">
    <table>
        <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Origin</th>
            <th>Price</th>
        </tr>
        <c:forEach items="${hardwares}" var="hardware">
            <tr>
                <td>${hardware.getClass().getSimpleName()}</td>
                <td>${hardware.name}</td>
                <td>${hardware.origin}</td>
                <td>${hardware.price}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>
