package com.epam.task.dao;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.config.DBUnitConfig;
import com.epam.task.entity.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
public class AuthorDAOTest extends DBUnitConfig {

	@Autowired
	private AuthorDAO authorDAO;

	public AuthorDAOTest() {
		super();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/author/author-data.xml"));

		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	@Test
	public void testCreateAuthor() throws Exception {
		authorDAO.create(new Author("Jon", new Date()));

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/author/author-data-expected.xml"));
		
		ReplacementDataSet rDataSet = new ReplacementDataSet(expectedData);
		DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
		rDataSet.addReplacementObject("[create_date]", dateFormat.format(new Date()));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "AUTHOR_ID" };
		Assertion.assertEqualsIgnoreCols(rDataSet, actualData, "AUTHOR", ignore);
	}
	
	@Test
	public void testDeleteAuthor() throws Exception {
		authorDAO.delete(new Author(2L, "Paul", new Date()));

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/author/author-del-data-expected.xml"));
		
		ReplacementDataSet rDataSet = new ReplacementDataSet(expectedData);

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "AUTHOR_ID" };
		Assertion.assertEqualsIgnoreCols(rDataSet, actualData, "AUTHOR", ignore);
	}
	
	
	@Test
	public void testInsertNewsAuthor() throws Exception{
		authorDAO.insertNewsAuthor(4L, new Author(2L, "Paul", new Date()));
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/author/author-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {};
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, "NEWS_AUTHOR", ignore);
	}
	
	@Test
	public void testDeletetNewsAuthor() throws Exception{
		authorDAO.deleteNewsAuthor(3L);
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/author/author-na-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {};
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, "NEWS_AUTHOR", ignore);
	}
}
