package com.epam.spring.core.logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.epam.spring.core.beans.Event;

public class FileEventLogger implements EventLogger{
	
	private String filename;
	private File file;
	
	public void init() throws IOException{
		this.file = new File(filename);
		
		if(!file.canWrite()){
			throw new IOException();
		}
	}
	
	public void logEvent(Event event) {
		try {
			FileUtils.writeStringToFile(file, event.toString(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	

}
