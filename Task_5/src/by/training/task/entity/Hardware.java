package by.training.task.entity;

/**
 * Created by Nik on 14.10.2015.
 */
public class Hardware {

    private String id;
    private String name;
    private String origin;
    private int price;
    private boolean isCritical;
    private Type type = new Type();

    public static class Type{
        private boolean peripheral;
        private int energyConsuming;
        private boolean hasCooler;
        private String portType;

        public Type(){

        }

        public Type(boolean peripheral, int energyConsuming, boolean hasCooler, String portType) {
            this.peripheral = peripheral;
            this.energyConsuming = energyConsuming;
            this.hasCooler = hasCooler;
            this.portType = portType;
        }

        public boolean isPeripheral() {
            return peripheral;
        }

        public void setPeripheral(boolean peripheral) {
            this.peripheral = peripheral;
        }

        public int getEnergyConsuming() {
            return energyConsuming;
        }

        public void setEnergyConsuming(int energyConsuming) {
            this.energyConsuming = energyConsuming;
        }

        public boolean isHasCooler() {
            return hasCooler;
        }

        public void setHasCooler(boolean hasCooler) {
            this.hasCooler = hasCooler;
        }

        public String getPortType() {
            return portType;
        }

        public void setPortType(String portType) {
            this.portType = portType;
        }

        @Override
        public String toString() {
            return  "peripheral=" + peripheral +
                    ", energyConsuming=" + energyConsuming +
                    ", hasCooler=" + hasCooler +
                    ", portType='" + portType + '\'' +
                    '}';
        }
    }

    public Hardware(){

    }

    public Hardware(String name, String origin, int price, boolean isCritical, Type type) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.isCritical = isCritical;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isCritical() {
        return isCritical;
    }

    public void setCritical(boolean isCritical) {
        this.isCritical = isCritical;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return  '\n' + "Hardware{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", isCritical=" + isCritical +
                ", type= {" + type +
                "}";
    }
}
