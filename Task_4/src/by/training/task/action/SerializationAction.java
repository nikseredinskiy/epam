package by.training.task.action;

import by.training.task.entity.Disc;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by Nik on 17.10.2015.
 */
public class SerializationAction {

    private static Logger logger = Logger.getLogger(SerializationAction.class);

    public boolean serialization(Disc disc, String fileName){
        boolean flag = false;
        File f = new File(fileName);
        ObjectOutputStream ostream = null;
        try{
            FileOutputStream fos = new FileOutputStream(f);
            if(fos != null){
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(disc);
                flag = true;
            }

        } catch (FileNotFoundException e) {
            logger.error("File not found while writing", e);
        } catch (IOException e) {
            logger.error("Can't write in file", e);
        } finally {
            try{
                if(ostream != null){
                    ostream.close();
                }
            } catch (IOException e) {
                logger.error("Can't close file", e);
            }
        }
        return flag;
    }

    public Disc deserialization(String fileName) throws InvalidObjectException {
        File fr = new File(fileName);
        ObjectInputStream istream = null;
        try{
            FileInputStream fis = new FileInputStream(fr);
            istream = new ObjectInputStream(fis);
            Disc disc = (Disc) istream.readObject();

            return disc;

        } catch (FileNotFoundException e) {
            logger.error("Can't find file while reading", e);
        } catch (IOException e) {
            logger.error("Can't read file", e);
        } catch (ClassNotFoundException e) {
            logger.error("Class doesn't exist", e);
        } finally {
            try{
                if(istream != null){
                    istream.close();
                }
            } catch (IOException e) {
                logger.error("Can't close file", e);
            }
        }

        throw new InvalidObjectException("Can't serialize object from file");
    }

}
