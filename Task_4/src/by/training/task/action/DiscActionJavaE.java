package by.training.task.action;

import by.training.task.entity.Disc;
import by.training.task.entity.Song;
import by.training.task.exception.DiscException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Nik on 12.10.2015.
 */
public class DiscActionJavaE {

    static Logger logger = Logger.getLogger(DiscActionJavaE.class);

    public static void recordDisk(Disc disc, ArrayList<Song> songs) throws DiscException {
        Function<ArrayList<Song>, Integer> countWeight = DiscAction::calculateWeight;
        int weight = countWeight.apply(songs);

        Function<ArrayList<Song>, Integer> countLength = DiscAction::calculateLength;
        int length = countLength.apply(songs);

        Predicate<Disc> predicate = d -> d.getFreeSpace() > weight;
        Consumer<Disc> freeSpaceCons = d -> d.setFreeSpace(d.getFreeSpace() - weight);
        Consumer<Disc> lengthCons = d -> d.setLength(d.getLength() + length);
        Consumer<Disc> songsCons = d -> d.setSongs(songs);

        if(predicate.test(disc)){
            songsCons.accept(disc);
            freeSpaceCons.accept(disc);
            lengthCons.accept(disc);

            logger.debug("Songs successfully added");
        } else {
            throw new DiscException("Not enough space for record");
        }
    }

    public static Song findInInterval(Disc disc, int begin, int end){
        Optional<Disc> optional = checkDisk(disc);

        if(optional.isPresent()) {
            ArrayList<Song> songs = disc.getSongs();
            int startPosition = 0;

            Song returnSong = null;
            for (Song song : songs) {
                if ((startPosition >= begin) && (song.getLength() + startPosition <= end)) {
                    logger.debug("Song between " + begin + " and " + end + " has been found");
                    returnSong = song;
                    break;
                } else {
                    startPosition = startPosition + song.getLength();
                }
            }
            return returnSong;
        } else {
            logger.error("Disk is empty while findInInterval");
        }
        return null;
    }

    public static void sortByGenre(Disc disc){
        Optional<Disc> optional = checkDisk(disc);

        if(optional.isPresent()){
            ArrayList<Song> songs = disc.getSongs();

            Collections.sort(songs, (a, b) -> a.getGenre().compareTo(b.getGenre()));
        } else {
            logger.error("Disc is empty while sorting");
        }
    }

    private static Optional<Disc> checkDisk(Disc disc){
        if(disc == null||disc.getLength() == 0||disc.getSongs().isEmpty()){
            return Optional.empty();
        }
        return Optional.of(disc);
    }
}
