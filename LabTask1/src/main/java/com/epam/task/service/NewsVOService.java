package com.epam.task.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import com.epam.task.entity.Author;
import com.epam.task.entity.Comment;
import com.epam.task.entity.News;
import com.epam.task.entity.NewsVO;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.entity.Tag;
import com.epam.task.exception.ServiceExeption;

@Service
@Transactional(rollbackFor = Exception.class)
public class NewsVOService {

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private CommentService commentService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * Insert news with it's comments tags and author links
	 * 
	 * @param newsVO
	 * @return generated news id
	 * @throws ServiceExeption
	 */
	public Long insert(NewsVO newsVO) throws ServiceExeption {
		Long newsId = null;
		try {
			newsId = this.newsService.insertNews(newsVO.getNews());
			this.authorService.insertNewsAuthor(newsId, newsVO.getAuthor());
			this.tagService.insertNewsTag(newsId, newsVO.getTags());
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while insert()", e);
		}

		return newsId;
	}

	/**
	 * Delete news with it's comments tags and author links
	 * 
	 * @param newsVO
	 * @throws ServiceExeption
	 */
	public void delete(NewsVO newsVO) throws ServiceExeption {
		try {
			this.authorService.deleteNewsAuthor(newsVO.getNews().getId());
			this.tagService.deleteNewsTag(newsVO.getNews().getId());
			this.commentService.deleteNewsComment(newsVO.getNews().getId());
			this.newsService.deleteNews(newsVO.getNews());
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}

	/**
	 * Get list of news with it's comments tags and author
	 * 
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getAllNews() throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getAllNews();
			for (News temp : news) {
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while delete()", e);
		}

		return newsVOList;
	}

	/**
	 * Return news by id with it's comments tags and author
	 * 
	 * @param newsId
	 * @return NewsVO
	 * @throws ServiceExeption
	 */
	public NewsVO getNews(Long newsId) throws ServiceExeption {
		NewsVO newsVO = null;

		try {
			News news = newsService.getNews(newsId);
			Author author = authorService.getNewsAuthor(newsId);
			List<Tag> tags = tagService.getNewsTag(newsId);
			List<Comment> comments = commentService.getNewsComments(newsId);

			newsVO = new NewsVO(news, author, tags, comments);
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNews()", e);
		}

		return newsVO;
	}

	/**
	 * Return list of NewsVO(news + comments + tags + author) sorted by comments
	 * count
	 * 
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getNewsByCommCount() throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getNewsByCommCount();
			for (News temp : news) {
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNewsByCommCount()", e);
		}
		return newsVOList;
	}

	/**
	 * Return list of NewsVO(news + comments + tags + author) according to
	 * searchCriteria that may include authorId and list of tags
	 * 
	 * @param searchCriteria
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getNewsBySeacrh(SearchCriteria searchCriteria)
			throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getNewsBySeacrh(searchCriteria);
			for (News temp : news) {
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNewsBySeacrh()", e);
		}

		return newsVOList;
	}

}
