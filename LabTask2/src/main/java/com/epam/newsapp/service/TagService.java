package com.epam.newsapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.dao.TagDAO;
import com.epam.newsapp.entity.Tag;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.exception.ServiceExeption;

@Service
@Transactional
public class TagService {
	
	@Autowired
	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public Long createTag(Tag tag) throws ServiceExeption{
		Long id;
		try {
			id = this.tagDAO.createTag(tag);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while create()", e);
		}
		
		return id;
	}
	
	public Long updateTag(Tag tag) throws ServiceExeption{
		Long id;
		try {
			id = this.tagDAO.update(tag);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while update()", e);
		}
		
		return id;
	}
	
	public void deleteTag(Long tagId) throws ServiceExeption{
		try {
			this.tagDAO.deleteNTByTagId(tagId);
			this.tagDAO.delete(tagId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}
	
	public void insertNewsTag(Long newsId, List<Tag> tags) throws ServiceExeption{
		try {
			this.tagDAO.insertNewsTag(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public void updateNewsTag(Long newsId, List<Tag> tags) throws ServiceExeption{
		try {
			this.tagDAO.updateNewsTag(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public List<Tag> getNewsTag(Long newsId) throws ServiceExeption{
		try {
			return this.tagDAO.getNewsTag(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsTag()", e);
		}
	}
	

	public void deleteNewsTag(Long newsId) throws ServiceExeption{
		try {
			this.tagDAO.deleteNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsTag()", e);
		}
	}
	
	public List<Tag> getAllTags() throws ServiceExeption{
		try {
			return this.tagDAO.getAllTags();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllTags()", e);
		}
	}

}
