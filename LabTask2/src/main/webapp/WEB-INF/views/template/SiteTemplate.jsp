<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Default tiles template</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<style type="text/css">
body {
	margin: 0px;
	padding: 0px;
	height: 100%;
	overflow: hidden;
}

.page {
	min-height: 100%;
	position: relative;
}

.header {
	padding: 10px;
	width: 100%;
	text-align: center;
}

.content {
	padding: 10px;
	padding-bottom: 20px; /* Height of the footer element */
	overflow: hidden;
}

.menu {
	padding: 20px 10px 0px 10px;
	width: 200px;
	float: left;
}

.body {
	margin: 20px 10px 0px 250px;
}

.footer {
	clear: both;
	position: absolute;
	bottom: 0;
	left: 0;
	text-align: center;
	width: 100%;
	height: 20px;
}

.active{
	font-weight: bold;
}
</style>
</head>

<body>
	<div class="page">
		<tiles:insertAttribute name="header" />
		<div class="content">
			<tiles:insertAttribute name="menu" />
			<tiles:insertAttribute name="body" />
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>

<script>
	$(".actions-tag-list-show").click(function() {

		$("tr").each(function() {
			$(".active input").prop('disabled', true);
			$(this).removeClass("active");
			$(".actions-tag-list-edit").css('display', 'none');
			$(".actions-tag-list-show").css('display', 'inline');
		});

		$(this).parent('td').parent('tr').addClass("active");
		$(".active .actions-tag-list-show").css('display', 'none');
		$(".active .actions-tag-list-edit").css('display', 'inline');

		$(".active input").prop('disabled', false);
	});

	$(".actions-tag-list-edit.cancel").click(function() {
		$(".active input").prop('disabled', 'true');
		$("tr").each(function() {
			$(this).removeClass("active");
		});

		$(".actions-tag-list-edit").css('display', 'none');
		$(".actions-tag-list-show").css('display', 'inline');

	});
	
	$(function() {
		var lastTab = localStorage.getItem('lastTab');
		$("#" + lastTab).addClass("active");
		localStorage.removeItem("lastTab");
	});
	
	$(".menuLink").click(function() {
		 localStorage.setItem('lastTab', $(this).attr('id'));		
	});
</script>
</html>
