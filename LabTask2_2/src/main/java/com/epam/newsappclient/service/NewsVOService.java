package com.epam.newsappclient.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsappclient.entity.Author;
import com.epam.newsappclient.entity.Comment;
import com.epam.newsappclient.entity.News;
import com.epam.newsappclient.entity.NewsVO;
import com.epam.newsappclient.entity.SearchCriteria;
import com.epam.newsappclient.entity.Tag;
import com.epam.newsappclient.exception.ServiceExeption;
import com.epam.newsappclient.service.AuthorService;
import com.epam.newsappclient.service.CommentService;
import com.epam.newsappclient.service.NewsService;
import com.epam.newsappclient.service.TagService;

@Service
public class NewsVOService {

	@Autowired
	private NewsService newsService;
	
	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private CommentService commentService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * Insert news with it's comments tags and author links
	 * 
	 * @param newsVO
	 * @return generated news id
	 * @throws ServiceExeption
	 */
	public long insert(NewsVO newsVO) throws ServiceExeption {
		long newsId;
		try {
			newsId = this.newsService.insertNews(newsVO.getNews());
			this.authorService.insertNewsAuthor(newsId, newsVO.getAuthor());
			this.tagService.insertNewsTag(newsId, newsVO.getTags());
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while insert()", e);
		}

		return newsId;
	}
	
	public long update(NewsVO newsVO) throws ServiceExeption {
		long newsId = -1;
		try {
			newsId = this.newsService.updateNews(newsVO.getNews());
			this.authorService.updateNewsAuthor(newsId, newsVO.getAuthor());
			this.tagService.deleteNewsTag(newsId);
			this.tagService.insertNewsTag(newsId, newsVO.getTags());
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while update()", e);
		}

		return newsId;
	}

	/**
	 * Delete news with it's comments tags and author links
	 * 
	 * @param newsVO
	 * @throws ServiceExeption
	 */
	public void delete(Long newsId) throws ServiceExeption {
		try {
			this.authorService.deleteNewsAuthor(newsId);
			this.tagService.deleteNewsTag(newsId);
			this.commentService.deleteNewsComment(newsId);
			this.newsService.deleteNews(newsId);
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}

	/**
	 * Get list of news with it's comments tags and author
	 * 
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getAllNews() throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getAllNews();
			for (Iterator<News> iterator = news.iterator(); iterator.hasNext();) {
				News temp = (News) iterator.next();
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while delete()", e);
		}

		return newsVOList;
	}

	/**
	 * Return news by id with it's comments tags and author
	 * 
	 * @param newsId
	 * @return NewsVO
	 * @throws ServiceExeption
	 */
	public NewsVO getNews(Long newsId) throws ServiceExeption {
		NewsVO newsVO = null;

		try {
			News news = newsService.getNews(newsId);
			Author author = authorService.getNewsAuthor(newsId);
			List<Tag> tags = tagService.getNewsTag(newsId);
			List<Comment> comments = commentService.getNewsComments(newsId);

			newsVO = new NewsVO(news, author, tags, comments);
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNews()", e);
		}

		return newsVO;
	}

	/**
	 * Return list of NewsVO(news + comments + tags + author) sorted by comments
	 * count
	 * 
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getNewsByCommCount() throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getNewsByCommCount();
			for (Iterator<News> iterator = news.iterator(); iterator.hasNext();) {
				News temp = (News) iterator.next();
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNewsByCommCount()", e);
		}
		return newsVOList;
	}

	/**
	 * Return list of NewsVO(news + comments + tags + author) according to
	 * searchCriteria that may include authorId and list of tags
	 * 
	 * @param searchCriteria
	 * @return List of NewsVO
	 * @throws ServiceExeption
	 */
	public List<NewsVO> getNewsBySeacrh(SearchCriteria searchCriteria, long pageNum, long newsOnPage)
			throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getNewsBySeacrh(searchCriteria, pageNum, newsOnPage);
			for (Iterator<News> iterator = news.iterator(); iterator.hasNext();) {
				News temp = (News) iterator.next();
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNewsBySeacrh()", e);
		}

		return newsVOList;
	}
	
	public List<NewsVO> getNewsOnPage(long pageNum, long newsOnPage)
			throws ServiceExeption {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();

		try {
			List<News> news = newsService.getNewsOnPage(pageNum, newsOnPage);
			for (Iterator<News> iterator = news.iterator(); iterator.hasNext();) {
				News temp = (News) iterator.next();
				Author author = authorService.getNewsAuthor(temp.getId());
				List<Tag> tags = tagService.getNewsTag(temp.getId());
				List<Comment> comments = commentService.getNewsComments(temp
						.getId());

				NewsVO newsVO = new NewsVO(temp, author, tags, comments);
				newsVOList.add(newsVO);
			}
		} catch (ServiceExeption e) {
			throw new ServiceExeption("Error while getNewsBySeacrh()", e);
		}

		return newsVOList;
	}

}
