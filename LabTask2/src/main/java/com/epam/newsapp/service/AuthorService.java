package com.epam.newsapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.dao.AuthorDAO;
import com.epam.newsapp.entity.Author;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.exception.ServiceExeption;

@Service
public class AuthorService {
	
	@Autowired
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public Long createAuthor(Author author) throws ServiceExeption{
		Long id;
		try {
			 id = this.authorDAO.createAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while create() " + e, e);
		}
		
		return id;
	}
	
	public long updateAuthor(Author author) throws ServiceExeption{
		Long id;
		try {
			 id = this.authorDAO.updateAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while updateAuthor() " + e, e);
		}
		
		return id;
	}
	
	public void expireAuthor(Author author) throws ServiceExeption{
		try {
			 this.authorDAO.expireAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while updateAuthor() " + e, e);
		}
	}
	
	public Author getNewsAuthor(long newsId) throws ServiceExeption{
		try {
			return this.authorDAO.getNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsAuthor()", e);
		}
	}
	
	public void insertNewsAuthor(long newsId, Author author) throws ServiceExeption{
		try {
			this.authorDAO.insertNewsAuthor(newsId, author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public void updateNewsAuthor(long newsId, Author author) throws ServiceExeption{
		try {
			this.authorDAO.updateNewsAuthor(newsId, author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	
	public void deleteNewsAuthor(long newsId) throws ServiceExeption{
		try {
			this.authorDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsAuthor()", e);
		}
	}
	
	public List<Author> getAllAuthors() throws ServiceExeption{
		try{
			return this.authorDAO.getAllAuthors();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllAuthors()", e);
		}
	}
}
