package by.training.task.action;

import by.training.task.entity.Hardware;
import by.training.task.entity.InputHardware;
import by.training.task.entity.MultiMedHardware;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nik on 25.10.2015.
 */
public class HardwareStAXBuilder {

    static Logger logger = Logger.getLogger(HardwareStAXBuilder.class);

    private HashSet<Hardware> hardwares = new HashSet<Hardware>();
    private XMLInputFactory inputFactory;

    public HardwareStAXBuilder(){
        inputFactory = XMLInputFactory.newInstance();
    }

    public Set<Hardware> getHardwares(){
        return hardwares;
    }

    public void buildSetHardwares(String fileName){
        FileInputStream inputStream = null;
        XMLStreamReader reader = null;
        String name;
        try{
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);
            while(reader.hasNext()){
                int type = reader.next();
                if(type == XMLStreamConstants.START_ELEMENT){
                    name = reader.getLocalName();
                    Hardware hardware;
                    switch (HardwareEnum.valueOf(name.toUpperCase())){
                        case HARDWARE:
                            hardware = buildHardware(reader, "hardware");
                            hardwares.add(hardware);
                            break;
                        case MULTIMEDHARDWARE:
                            hardware = buildHardware(reader, "multimedhardware");
                            hardwares.add(hardware);
                            break;
                        case INPUTHARDWARE:
                            hardware = buildHardware(reader, "inputhardware");
                            hardwares.add(hardware);
                            break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("File not found", e);
        } catch (XMLStreamException e) {
            logger.error("XMLStreamException", e);
        } finally {
            try{
                if(inputStream != null){
                    inputStream.close();
                }
            } catch (IOException e) {
                logger.error("Impossible to close the file" + fileName + " :", e);
            }
        }
    }

    private Hardware buildHardware(XMLStreamReader reader, String hardwareType) throws XMLStreamException{
        Hardware hardware;
        switch (hardwareType){
            case "hardware":
                hardware = new Hardware();
                break;
            case "multimedhardware":
                hardware = new MultiMedHardware();
                break;
            case "inputhardware":
                hardware = new InputHardware();
                break;
            default:
                hardware = new Hardware();
        }
        hardware.setId(reader.getAttributeValue(null, HardwareEnum.ID.getValue()));

        String name;
        while(reader.hasNext()){
            int type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (HardwareEnum.valueOf(name.toUpperCase().replace("-","_"))) {
                        case NAME:
                            hardware.setName(getXMLText(reader));
                            break;
                        case ORIGIN:
                            hardware.setOrigin(getXMLText(reader));
                            break;
                        case PRICE:
                            name = getXMLText(reader);
                            hardware.setPrice(Integer.parseInt(name));
                            break;
                        case CRITICAL:
                            name = getXMLText(reader);
                            hardware.setCritical(Boolean.valueOf(name));
                            break;
                        case TYPE:
                            hardware.setType(getXMLType(reader));
                            break;
                        case SPEAKERS_COUNT:
                            name = getXMLText(reader);
                            ((MultiMedHardware) hardware).setSpeakersCount(Integer.parseInt(name));
                            break;
                        case HAS_SUB:
                            name = getXMLText(reader);
                            ((MultiMedHardware) hardware).setHasSub(Boolean.valueOf(name));
                            break;
                        case HAS_DOLBY:
                            name = getXMLText(reader);
                            ((MultiMedHardware) hardware).setHasDolby(Boolean.valueOf(name));
                            break;
                        case KEYS_COUNT:
                            name = getXMLText(reader);
                            ((InputHardware) hardware).setKeyCount(Integer.parseInt(name));
                            break;
                        case HAS_NUMPAD:
                            name = getXMLText(reader);
                            ((InputHardware) hardware).setHasNumPad(Boolean.valueOf(name));
                            break;
                        case HAS_ADDITIONAL_KEYS:
                            name = getXMLText(reader);
                            ((InputHardware) hardware).setHasAdditionalKey(Boolean.valueOf(name));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    switch (HardwareEnum.valueOf(name.toUpperCase().replace("-", "_"))){
                        case HARDWARE:
                            return hardware;
                        case MULTIMEDHARDWARE:
                            return hardware;
                        case INPUTHARDWARE:
                            return hardware;
                    }
            }
        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private Hardware.Type getXMLType(XMLStreamReader reader) throws XMLStreamException{
        Hardware.Type hardwareType = new Hardware.Type();
        int type;
        String name;
        while(reader.hasNext()){
            type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (HardwareEnum.valueOf(name.toUpperCase().replace("-","_"))){
                        case PERIPHERAL:
                            name = getXMLText(reader);
                            hardwareType.setPeripheral(Boolean.valueOf(name));
                            break;
                        case ENERGY_CONSUMING:
                            name = getXMLText(reader);
                            hardwareType.setEnergyConsuming(Integer.parseInt(name));
                            break;
                        case HAS_COOLER:
                            name = getXMLText(reader);
                            hardwareType.setHasCooler(Boolean.valueOf(name));
                            break;
                        case PORT_TYPE:
                            hardwareType.setPortType(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if(HardwareEnum.valueOf(name.toUpperCase().replace("-", "_")) == HardwareEnum.TYPE){
                        return hardwareType;
                    }
            }
        }
        throw new XMLStreamException("Unknown element in tag Type");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException{
        String text = null;
        if(reader.hasNext()){
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
