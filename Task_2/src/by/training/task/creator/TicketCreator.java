package by.training.task.creator;

import by.training.task.entity.Plain;
import by.training.task.entity.Ticket;

import java.util.ArrayList;

/**
 * Created by Nik on 29.09.2015.
 */
public class TicketCreator {
    public ArrayList<Ticket> createTicket(){
        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        Plain plain = new Plain(1, 10);

        tickets.add(new Ticket(plain));
        tickets.add(new Ticket(plain));
        tickets.add(new Ticket(plain));

        return tickets;
    }
}
