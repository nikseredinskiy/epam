package com.epam.task.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;




import com.epam.task.dao.AuthorDAO;
import com.epam.task.entity.Author;
import com.epam.task.exception.DAOException;
import com.epam.task.util.ConnectionUtil;

@Repository
public class AuthorDAOImpl implements AuthorDAO {
	
	@Autowired
	private DataSource dataSource;
	
	private final String SQL_CREATE = "INSERT INTO AUTHOR VALUES(AUTHOR_ID_SEQ.NEXTVAL,?,?)";
	private final String SQL_INSERT = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES(?,?)";
	private final String SQL_DELETE = "DELETE FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.NEWS_ID = ?";
	private final String SQL_GET_AUTHOR = "SELECT * FROM NEWS_AUTHOR NA INNER JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID WHERE NEWS_ID = ?";
	private final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
	private final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR.AUTHOR_ID = ?";
	private final String SQL_UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";

	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	
	public Long create(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long id = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_CREATE, new String [] {"AUTHOR_ID"});

			ps.setString(1, author.getName());
			
			if(author.getExpired() != null){
				ps.setDate(2, new java.sql.Date(author.getExpired().getTime()));
			} else {
				ps.setDate(2, null);
			}
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while create()" + e, e);
		} finally{
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
	}
	
	

	public Long update(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, author.getName());
			ps.setDate(2, new java.sql.Date(author.getExpired().getTime()));
			ps.setLong(3, author.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while update()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return author.getId();
	}


	public void delete(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE_AUTHOR);

			ps.setLong(1, author.getId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteAuthor()" + e, e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}


	public List<Author> getAllAuthors() throws DAOException {
		Connection conn = null;
		Author author = null;
		Statement st = null;
		ResultSet rs = null;
		List<Author> authorList = new ArrayList<Author>();

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
				
			rs = st.executeQuery(SQL_GET_ALL_AUTHORS);
			while(rs.next()){
				author = new Author(rs.getLong("AUTHOR_ID"), rs.getString("AUTHOR_NAME"), rs.getDate("EXPIRED"));
				authorList.add(author);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteAuthor()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}
		
		return authorList;
	}


	public void insertNewsAuthor(Long newsId, Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT);

			ps.setLong(1, newsId);
			ps.setLong(2, author.getId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsAuthor(Long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);

			ps.setLong(1, newsId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public Author getNewsAuthor(Long newsId) throws DAOException{
		Connection conn = null;
		Author author = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_GET_AUTHOR);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				author = new Author(rs.getLong("AUTHOR_ID"), rs.getString("AUTHOR_NAME"), rs.getDate("EXPIRED"));
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return author;
	}
}
