package by.training.task.action;

import by.training.task.entity.Disc;
import by.training.task.entity.Song;
import by.training.task.exception.DiscException;
import by.training.task.main.Runner;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;

/**
 * Created by Nik on 22.09.2015.
 */
public class DiscAction {

    private static Logger logger = Logger.getLogger(DiscAction.class);

    public static void recordDisk(Disc disc, ArrayList<Song> songs) throws DiscException {
        int weight = calculateWeight(songs);
        int length = calculateLength(songs);

        if(disc.getFreeSpace() > weight){
            disc.setSongs(songs);

            int spaceLeft = disc.getFreeSpace() - weight;
            disc.setFreeSpace(spaceLeft);
            disc.setLength(length);

            logger.debug("Songs successfully added");
        } else {
            throw new DiscException("Not enough space for record");
        }
    }

    public static Song findInInterval(Disc disc, int begin, int end){
        ArrayList<Song> songs = disc.getSongs();
        int startPosition = 0;

        Song returnSong = null;
        for(Song song:songs){
            if((startPosition >= begin) && (song.getLength() + startPosition <= end)){
                logger.debug("Song between " + begin + " and " + end + " has been found");
                returnSong = song;
                break;
            } else {
                startPosition = startPosition + song.getLength();
            }
        }
        return returnSong;
    }

    public static void sortByGenre(Disc disc){
        ArrayList<Song> songs = disc.getSongs();

        Collections.sort(songs, new Song.GenreComparator());
        //Collections.sort(songs, (a, b) -> a.getGenre().compareTo(b.getGenre()));
    }

    public static int calculateWeight(ArrayList<Song> songs){
        int weight = 0;
        for(Song song:songs){
            weight = weight + song.getSize();
        }

        return weight;
    }

    public static int calculateLength(ArrayList<Song> songs){
        int length = 0;
        for(Song song:songs){
            length = length + song.getLength();
        }

        return length;
    }
}
