package com.epam.task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.epam.task.dao.TagDAO;
import com.epam.task.entity.Tag;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;

@Service
public class TagService {
	
	@Autowired
	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public Long createTag(Tag tag) throws ServiceExeption{
		Long id = null;
		try {
			id = this.tagDAO.create(tag);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while create()", e);
		}
		
		return id;
	}
	
	public void insertNewsTag(Long newsId, List<Tag> tags) throws ServiceExeption{
		try {
			this.tagDAO.insertNewsTag(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public List<Tag> getNewsTag(Long newsId) throws ServiceExeption{
		try {
			return this.tagDAO.getNewsTag(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsTag()", e);
		}
	}
	

	public void deleteNewsTag(Long newsId) throws ServiceExeption{
		try {
			this.tagDAO.deleteNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsTag()", e);
		}
	}

}
