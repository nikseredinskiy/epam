package com.epam.newsappclient.util;

public class Parameter {
	
	public static final long NEWS_ON_PAGE = 4;
	public static final String NEWS_LIST = "newsList";
	public static final String AUTHORS = "authors";
	public static final String TAGS = "tags";
	public static final String OPERATION = "operation";
	public static final String VIEW_NEWS = "viewNews";
	public static final String SEARCH_RESULT = "searchResult";
	public static final String PAGE_COUNT = "pageCount";
	
	public static final String NEWS_LIST_PAGE = "WEB-INF/views/newsList.jsp";
	public static final String NEWS_PAGE = "WEB-INF/views/news.jsp";
	public static final String HOME_PAGE = "WEB-INF/views/home.jsp";
	
	public static final String NEWS_ID = "newsId";
	public static final String COMMENT_TEXT = "commentText";
	public static final String NEWS_VO = "newsVO";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String PAGE = "page";
	
	public static final String AUTHOR_ID = "authorId";
	public static final String TAG_ID = "tagId";
	
	public static final String COMMAND = "command";
	public static final String EMPTY_COMMAND = "emptyCommand";
}
