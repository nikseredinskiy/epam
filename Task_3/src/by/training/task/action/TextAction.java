package by.training.task.action;

import by.training.task.entity.Component;
import by.training.task.entity.CompositePart;
import by.training.task.entity.LeafPart;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Nik on 10.10.2015.
 */
public class TextAction {

    static Logger logger = Logger.getLogger(TextAction.class);

    public static String assembleText(CompositePart fullText){
        String text = "";
        for(int i = 0; i < fullText.getComponentList().size(); i++){

            LeafPart.LeafType type = fullText.getComponent(i).getLeafType();
            if(type == LeafPart.LeafType.LISTING){
                LeafPart listing = (LeafPart)fullText.getComponent(i);
                text = text + listing.toString() + "\n";
                i++;
            }

            for(int j = 0; j < fullText.getComponent(i).getComponentList().size(); j++){
                for(int k = 0; k < fullText.getComponent(i).getComponent(j).getComponentList().size(); k++){
                    LeafPart word = (LeafPart)fullText.getComponent(i).getComponent(j).getComponent(k);
                    text = text + word.toString();
                    if(word.getLeafType().equals(LeafPart.LeafType.SIGN)){
                        text = text + " ";
                    }
                }
            }
            text = text + "\n";
        }

        return text;
    }

    public static ArrayList<Component> orderBySize(CompositePart fullText){
        Comparator<Component> comp = (o1, o2) ->
                Integer.compare(o1.getComponentList().size(), o2.getComponentList().size());

        ArrayList<Component> sentence = new ArrayList<Component>();

        for(int i = 0; i < fullText.getComponentList().size(); i++){

            LeafPart.LeafType type = fullText.getComponent(i).getLeafType();
            if(type == LeafPart.LeafType.LISTING){
                i++;
            }
            for(int j = 0; j < fullText.getComponent(i).getComponentList().size(); j++){
                sentence.add(fullText.getComponent(i).getComponent(j));
            }
        }

        Collections.sort(sentence, comp);

        sentence.forEach(logger::debug);

        return sentence;
    }

    public static HashMap<String, Integer> containCount(CompositePart fullText){
        HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
        for(int i = 0; i < fullText.getComponentList().size(); i++){
            LeafPart.LeafType type = fullText.getComponent(i).getLeafType();
            if(type == LeafPart.LeafType.LISTING){
                i++;
            }
            for(int j = 0; j < fullText.getComponent(i).getComponentList().size(); j++){
                for(int k = 0; k < fullText.getComponent(i).getComponent(j).getComponentList().size(); k++){
                    LeafPart word = (LeafPart) fullText.getComponent(i).getComponent(j).getComponent(k);
                    if(word.getLeafType() != LeafPart.LeafType.SIGN){
                        String text = word.toString().trim().toLowerCase();
                        if(wordCount.containsKey(text)){
                            Integer count = wordCount.get(text);
                            count ++;
                            wordCount.put(text, count);
                        } else {
                            wordCount.put(text, 1);
                        }
                    }
                }
            }
        }

        wordCount = sortByValueAndKey(wordCount);

        Set<Map.Entry<String, Integer>> set = wordCount.entrySet();
        Iterator<Map.Entry<String, Integer>> i = set.iterator();
        while(i.hasNext()){
            Map.Entry<String, Integer> me = i.next();
            logger.debug(me.getKey() + " " + me.getValue());
        }

        return wordCount;
    }

    private static HashMap<String, Integer> sortByValueAndKey(HashMap<String, Integer> wordCount){
        List<Map.Entry<String, Integer>> list = new LinkedList(wordCount.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>(){
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue() == o2.getValue() ? o1.getKey().compareTo(o2.getKey()): o1.getValue()-o2.getValue();
            }
        });

        HashMap<String, Integer> sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put((String)entry.getKey(), (Integer)entry.getValue());
        }

        return sortedHashMap;
    }

}
