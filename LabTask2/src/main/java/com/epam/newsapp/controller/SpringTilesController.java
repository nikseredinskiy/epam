package com.epam.newsapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsapp.entity.Author;
import com.epam.newsapp.entity.Comment;
import com.epam.newsapp.entity.NewsVO;
import com.epam.newsapp.entity.SearchCriteria;
import com.epam.newsapp.entity.Tag;
import com.epam.newsapp.exception.ServiceExeption;
import com.epam.newsapp.service.AuthorService;
import com.epam.newsapp.service.CommentService;
import com.epam.newsapp.service.NewsService;
import com.epam.newsapp.service.NewsVOService;
import com.epam.newsapp.service.TagService;

@Controller
public class SpringTilesController {

	@Autowired
	NewsVOService newsVOService;

	@Autowired
	NewsService newsService;

	@Autowired
	AuthorService authorService;

	@Autowired
	TagService tagService;

	@Autowired
	CommentService commentService;

	private final long NEWS_ON_PAGE = 4;
	
     
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcom(Locale locale, Model model) {

		return "home";
	}
	
	@RequestMapping(value = "/home" , method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		return "home";
	}
	
	@RequestMapping(value = "/login" , method = RequestMethod.GET)
	public String login(Locale locale, Model model) {

		return "login";
	}
	
	
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(Model model) {

		return "about";
	}

	@RequestMapping(value = "/viewNews/page/{pageId}", method = RequestMethod.GET)
	public ModelAndView viewNews(Model model, @PathVariable("pageId") int pageId) {
		Map<String, List<?>> attributes = new HashMap<String, List<?>>();
		double newsCount = 0;

		try {
			List<NewsVO> newsList = newsVOService.getNewsOnPage(pageId,
					NEWS_ON_PAGE);
			newsCount = (double) newsService.countAllNews();

			attributes.put("news", newsList);
			attributes.put("authors", authorService.getAllAuthors());
			attributes.put("tags", tagService.getAllTags());
		} catch (ServiceExeption e) {

		}
		model.addAttribute("operation", "viewNews");
		model.addAttribute("currentPage", pageId);
		model.addAttribute("pageCount", Math.ceil(newsCount / NEWS_ON_PAGE));
		model.addAttribute("searchCriteria", new SearchCriteria());
		return new ModelAndView("viewNews", attributes);
	}

	@RequestMapping(value = "/viewNews/{id}", method = RequestMethod.GET)
	public ModelAndView viewFullNews(Model model,
			@PathVariable("id") Long newsId) {
		Map<String, NewsVO> news = new HashMap<String, NewsVO>();

		try {
			news.put("newsVO", newsVOService.getNews(newsId));
		} catch (ServiceExeption e) {

		}
		Comment comment = new Comment();
		comment.setNewsID(newsId);

		model.addAttribute("comment", comment);
		return new ModelAndView("viewFullNews", news);
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public ModelAndView addNews(Model model) {
		Map<String, List<?>> attributes = new HashMap<String, List<?>>();
		try {
			attributes.put("authors", authorService.getAllAuthors());
			attributes.put("tags", tagService.getAllTags());

			model.addAttribute("newsVO", new NewsVO());
		} catch (ServiceExeption e) {

		}
		return new ModelAndView("addNews", attributes);
	}

	@RequestMapping(value = "/saveNews", method = RequestMethod.POST)
	public String saveNewsVO(@Valid NewsVO newsVO, BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			return "redirect:/addNews";
		}

		List<Tag> tagList = newsVO.getTags();
		List<Tag> newTagList = new ArrayList<Tag>();

		for (Iterator<Tag> iterator = tagList.iterator(); iterator.hasNext();) {
			Tag tag = (Tag) iterator.next();
			Tag temp = new Tag(Long.parseLong(tag.getName()), "");

			newTagList.add(temp);
		}
		newsVO.setTags(newTagList);

		try {
			long id = newsVOService.insert(newsVO);
			return "redirect:/viewNews/" + id;
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.GET)
	public ModelAndView addAuthor(Model model) {
		Map<String, List<Author>> attributes = new HashMap<String, List<Author>>();
		try {
			attributes.put("authors", authorService.getAllAuthors());

			model.addAttribute("author", new Author());
		} catch (ServiceExeption e) {

		}
		return new ModelAndView("addAuthor", attributes);
	}

	@RequestMapping(value = "/saveAuthor", method = RequestMethod.POST)
	public String saveAuthor(@Valid Author author, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		try {
			authorService.createAuthor(author);
			return "redirect:/addAuthor";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
	public String updateAuthor(Author author, BindingResult bindingResult) {
		
		try {
			authorService.updateAuthor(author);
			return "redirect:/addAuthor";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(value = "/expireAuthor", method = RequestMethod.POST)
	public String expireAuthor(Author author, BindingResult bindingResult) {
		
		try {
			authorService.expireAuthor(author);
			return "redirect:/addAuthor";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.GET)
	public ModelAndView addTag(Model model) {
		Map<String, List<Tag>> attributes = new HashMap<String, List<Tag>>();
		try {
			attributes.put("tags", tagService.getAllTags());

			model.addAttribute("tag", new Tag());
		} catch (ServiceExeption e) {

		}
		return new ModelAndView("addTag", attributes);
	}

	@RequestMapping(value = "/saveTag", method = RequestMethod.POST)
	public String saveTag(Tag tag, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		try {
			tagService.createTag(tag);
			return "redirect:/addTag";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(value = "/updateTag", method = RequestMethod.POST)
	public String updateTag(Tag tag, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		try {
			tagService.updateTag(tag);
			return "redirect:/addTag";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
	public String deleteTag(Tag tag, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		try {
			tagService.deleteTag(tag.getId());
			return "redirect:/addTag";
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}

	@RequestMapping(value = "/saveComment", method = RequestMethod.POST)
	public String saveComment(Comment comment, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		try {
			commentService.addComment(comment);
			return "redirect:/viewNews/" + comment.getNewsID();
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}

	@RequestMapping(value = "/viewNews/{newsId}/deleteComment/{commentId}", method = RequestMethod.POST)
	public String deleteComment(Model model,
			@PathVariable("newsId") Long newsId,
			@PathVariable("commentId") Long commentId) {

		try {
			commentService.deleteComment(commentId);
		} catch (ServiceExeption e) {
			return "error";
		}
		Comment comment = new Comment();
		comment.setNewsID(newsId);

		model.addAttribute("comment", comment);
		return "redirect:/viewNews/" + newsId;
	}

	@RequestMapping(value = "/editNews/{id}", method = RequestMethod.GET)
	public ModelAndView editNews(Model model, @PathVariable("id") Long newsId) {

		try {
			model.addAttribute("newsVO", newsVOService.getNews(newsId));
			model.addAttribute("authors", authorService.getAllAuthors());
			model.addAttribute("tags", tagService.getAllTags());
		} catch (ServiceExeption e) {

		}

		return new ModelAndView("editNews");
	}

	@RequestMapping(value = "/updateNews", method = RequestMethod.POST)
	public String updateNewsVO(NewsVO newsVO, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "errorPage";
		}

		List<Tag> tagList = newsVO.getTags();
		List<Tag> newTagList = new ArrayList<Tag>();

		for (Iterator<Tag> iterator = tagList.iterator(); iterator.hasNext();) {
			Tag tag = (Tag) iterator.next();
			Tag temp = new Tag(Long.parseLong(tag.getName()), "");

			newTagList.add(temp);
		}
		newsVO.setTags(newTagList);

		try {
			long id = newsVOService.update(newsVO);
			return "redirect:/viewNews/" + id;
		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(value = "/searchResult/page/{pageId}", method = RequestMethod.GET)
	public ModelAndView searchCriteria(Model model, SearchCriteria searchCriteria,
			BindingResult bindingResult, @PathVariable("pageId") int pageId) {
		Map<String, List<?>> attributes = new HashMap<String, List<?>>();
		double newsCount = 0;
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("error");
		}
				
		try {
			if(searchCriteria.getAuthorId() != null || searchCriteria.getTagId() != null){
				attributes.put("news", newsVOService.getNewsBySeacrh(
						searchCriteria, pageId, NEWS_ON_PAGE));
				
				newsCount = newsService.countNewsBySeacrh(searchCriteria);
				
				attributes.put("authors", authorService.getAllAuthors());
				attributes.put("tags", tagService.getAllTags());
				
				model.addAttribute("operation", "searchResult");
				model.addAttribute("pageCount", Math.ceil(newsCount / NEWS_ON_PAGE));
				model.addAttribute("searchCriteria", searchCriteria);
				
				return new ModelAndView("viewNews", attributes);
			} else {
				return viewNews(model, pageId);
			}

		} catch (ServiceExeption e) {
			e.printStackTrace();
		}
		return new ModelAndView("error");
	}
	
	@RequestMapping(value = "/deleteNews/{id}", method = RequestMethod.GET)
	public String deleteNews(Model model, @PathVariable("id") Long newsId) {

		try {
			newsVOService.delete(newsId);
			
		} catch (ServiceExeption e) {

		}

		return "redirect:/viewNews/page/1";
	}
}
