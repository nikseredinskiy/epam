package by.training.task.main;

import by.training.task.action.DiscActionFun;
import by.training.task.action.SerializationAction;
import by.training.task.creator.DiscCreatorFun;
import by.training.task.entity.Disc;
import by.training.task.exception.DiscException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.InvalidObjectException;

/**
 * Created by Nik on 22.09.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Runner.class);
    public static final int BEGIN_MOMENT = 4;
    public static final int END_MOMENT = 6;

    public static void main(String[] args) {
        try {
            //Disc disc = DiscCreator.createMusicDisc();
            Disc disc = DiscCreatorFun.createMusicDisk();

            //logger.debug(DiscAction.findInInterval(disc, BEGIN_MOMENT, END_MOMENT));
            logger.debug(DiscActionFun.findInInterval(disc, BEGIN_MOMENT, END_MOMENT));

            //DiscAction.sortByGenre(disc);
            DiscActionFun.sortByGenre(disc);

            String file = "data/disk.data";
            SerializationAction sa = new SerializationAction();
            boolean status = sa.serialization(disc, file);

            Disc resultDisk = null;
            try{
                resultDisk = sa.deserialization(file);
            } catch (InvalidObjectException e) {
                logger.error("Error while deserialization", e);
            }

            logger.debug(resultDisk);
        } catch (DiscException e) {
            logger.error("Exception while disc creating process",e);
        }
    }
}
