package by.training.task.action;

import by.training.task.creator.TextCreator;
import by.training.task.entity.CompositePart;
import by.training.task.entity.LeafPart;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nik on 05.10.2015.
 */
public class ParserAction {

    public static final String REGEX_FOR_PARAGRAPH = "([^\\s])(.+)([^(\\s*(Listing)([^\\t]+)(EndListing)\\s)])|\\s*(Listing)([^\\t]+)(EndListing)";
    public static final String REGEX_FOR_LISTING = "\\s*(Listing)([^\\t]+)(EndListing)";
    public static final String REGEX_FOR_SENTENCE = "([^\\s])([^(\\.|!|\\?)]+)(\\.|!|\\?)";
    public static final String REGEX_FOR_WORD_AND_SIGN = "([\\.,!\\?:;]{1})|([^\\s\\.,!\\?:;]+)(\\s*)";
    public static final String REGEX_FOR_SIGN = "((\\s)|([.,!?:;@]{1}))";

    static Logger logger = Logger.getLogger(ParserAction.class);

    public CompositePart parse(String path){
        String text = TextCreator.createText(path);

        CompositePart fullText = parseToParagraph(text);

        return fullText;
    }

    private CompositePart parseToParagraph(String text){
        CompositePart paragraphList = new CompositePart();
        Pattern patternParagraph = Pattern.compile(REGEX_FOR_PARAGRAPH);
        LeafPart paragraphLeaf;
        String paragraph;
        Matcher matcher = patternParagraph.matcher(text);

        while(matcher.find()){
            paragraph = matcher.group();
            if(Pattern.matches(REGEX_FOR_LISTING, paragraph)){
                paragraphLeaf = new LeafPart(paragraph, LeafPart.LeafType.LISTING);
                logger.debug("It's listing:" + paragraphLeaf);
                paragraphList.add(paragraphLeaf);
            } else {
                logger.debug("Paragraph:" + paragraph);
                paragraphList = parseToSentence(paragraphList, paragraph);
            }
        }

        return paragraphList;
    }

    private CompositePart parseToSentence(CompositePart paragraphList, String paragraph){
        CompositePart sentenceList = new CompositePart();
        Pattern patternSentence = Pattern.compile(REGEX_FOR_SENTENCE);
        Matcher matcher = patternSentence.matcher(paragraph);
        String sentence;

        while(matcher.find()){
            sentence = matcher.group();
            logger.debug("Sentence:" + sentence);
            sentenceList = parseToWordAndSign(sentenceList, sentence);

        }
        paragraphList.add(sentenceList);

        return paragraphList;
    }

    private CompositePart parseToWordAndSign(CompositePart sentenceList, String sentence){
        CompositePart wordAndSignList = new CompositePart();
        Pattern patternWord = Pattern.compile(REGEX_FOR_WORD_AND_SIGN);
        Matcher matcher = patternWord.matcher(sentence);
        LeafPart signLeaf;
        LeafPart wordLeaf;
        String wordAndSign;

        while (matcher.find()){
            wordAndSign = matcher.group();
            if(Pattern.matches(REGEX_FOR_SIGN, wordAndSign)){
                signLeaf = new LeafPart(wordAndSign, LeafPart.LeafType.SIGN);
                logger.debug("Sign:" + signLeaf);
                wordAndSignList.add(signLeaf);
            } else{
                logger.debug("Word:" + wordAndSign);
                wordLeaf = new LeafPart(wordAndSign, LeafPart.LeafType.WORD);
                wordAndSignList.add(wordLeaf);
            }
        }
        sentenceList.add(wordAndSignList);

        return sentenceList;
    }
}
