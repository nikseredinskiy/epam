package com.epam.newsappclient.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsappclient.util.Parameter;

public class EmptyCommand implements ActionCommand{

	public String execute(HttpServletRequest request) {
		return Parameter.HOME_PAGE;
	}
	

}
