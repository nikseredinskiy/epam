<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">

	<tiles:putAttribute name="title">
		<c:url value="${newsVO.getNews().getTitle()}" />|News Portal
	</tiles:putAttribute>
	
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="back">
				<a href="home?command=viewNews&page=${currentPage}">Back</a>
			</div>
			<div style="margin: 10px;">
				<p>
					<b> <c:out value="${newsVO.getNews().getTitle()}" />
					</b> (by
					<c:out value="${newsVO.getAuthor().getName()}" />
					) <i> <c:out value="Creation date: " /> <c:out
							value="${newsVO.getNews().getCreationDate()}" />
					</i>
				</p>
				<p>
					<c:out value="${newsVO.getNews().getFullText()}" />
				</p>
				<p>
					<c:forEach items="${newsVO.getTags()}" var="tag">
						<i> <c:out value="${tag.getName()}" /> <c:out value="; " />
						</i>
					</c:forEach>
					<c:out value="Comments(" />
					<c:out value="${newsVO.getComments().size()}" />
					<c:out value=")" />
				</p>
				<p>----</p>
				<table>
					<c:forEach items="${newsVO.getComments()}" var="comment">
						<tr>
							<td><c:out value="${comment.getCreationDate()} " /></td>
						</tr>
						<tr>
							<td><c:out value="${comment.getCommentText()}" /></td>
						</tr>
					</c:forEach>
				</table>
				<p>
				<form method="post" action="/NewsClient/">
					<table>
						<tr>
							<td><c:out value="Comment: " /> <input type="hidden"
								name="newsId" value="${newsVO.getNews().getId()}"> <input
								type="hidden" name="command" value="addComment"> <input
								type="hidden" name="currentPage" value="${currentPage}"></td>
						</tr>
						<tr>
							<td><textarea name="commentText" cols="40" rows="5"
									placeholder="Text here..."></textarea></td>
						</tr>
						<tr>
							<td><input type="submit" value="Save" /></td>
						</tr>
					</table>
				</form>
				</p>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
