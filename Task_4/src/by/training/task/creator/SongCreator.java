package by.training.task.creator;

import by.training.task.entity.ClassicSong;
import by.training.task.entity.FolkSong;
import by.training.task.entity.PopSong;
import by.training.task.entity.Song;
import java.util.ArrayList;

/**
 * Created by Nik on 22.09.2015.
 */
public class SongCreator {
    public static ArrayList<Song> createSongs(){
        ArrayList<Song> songs = new ArrayList<Song>();

        songs.add(new PopSong("Let It Be", "Paul McCartney", "The Beatles", "Pop", 4, 8, "mp3"));
        songs.add(new PopSong("Yesterday", "Paul McCartney", "The Beatles", "Baroque Pop", 2, 3, "mp3"));
        songs.add(new PopSong("All You Need Is Love", "John Lennon", "The Beatles", "Pop", 4, 7, "mp3"));
        songs.add(new PopSong("Baby, You're a Rich Man", "Paul McCartney", "The Beatles", "Psychedelic pop", 3, 7, "mp3"));
        songs.add(new FolkSong("Kalinka Malinka", "Folk", 1, 2, "mp3", "Russia", 1860));
        songs.add(new ClassicSong("Symphony №5", "Classic", 3, 8, "mp3", "Ludwig van Beethoven", 1808, "symphony", "C Minor"));

        return songs;
    }
}
