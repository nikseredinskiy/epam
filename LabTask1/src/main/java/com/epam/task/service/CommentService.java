package com.epam.task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.epam.task.dao.CommentDAO;
import com.epam.task.entity.Comment;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;


@Service
public class CommentService {
	
	@Autowired
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public Long addComment(Comment comment) throws ServiceExeption{
		Long id = null;
		
		try {
			id = this.commentDAO.add(comment);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while add()", e);
		}
		
		return id;
	}
	
	public void deleteComment(Comment comment) throws ServiceExeption{
		try {
			this.commentDAO.delete(comment);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}
	
	
	public void deleteNewsComment(Long newsId) throws ServiceExeption{
		try {
			this.commentDAO.deleteNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsComent()", e);
		}
	}
	
	public List<Comment> getNewsComments(Long newsId) throws ServiceExeption{
		try {
			return this.commentDAO.getNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsComments()", e);
		}
	}
}
