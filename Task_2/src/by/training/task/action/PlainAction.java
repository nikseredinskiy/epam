package by.training.task.action;

import by.training.task.entity.Plain;
import by.training.task.entity.Terminal;
import org.apache.log4j.Logger;

import java.util.concurrent.Semaphore;

/**
 * Created by Nik on 29.09.2015.
 */
public class PlainAction{

    private Plain plain;
    private Semaphore terminalSemaphore;
    private Terminal terminal;

    private static Logger logger = Logger.getLogger(PlainAction.class);

    public PlainAction(Plain plain){
        this.terminal = plain.getTerminal();
        this.plain = plain;
    }

    public void arriving(){
        logger.debug("Plain " + plain.getId() + " find " + terminal.getId() + " terminal");
        logger.debug("Plain " + plain.getId() + " goes to " + terminal.getId() + " terminal");
        if(plain.getPassengers().isEmpty()){
            plain.setState(Plain.State.BOARDING);
        }else{
            plain.setState(Plain.State.AT_TERMINAL);
        }
    }

    public void landed(){
        logger.debug("Success landing by " + plain.getId()+ " plain in " + plain.getTerminal().getId() + " terminal");
        terminal.setCurrentPlain(plain);
        try {
            Thread.sleep(4000);
            AirportAction.landingPassenger(plain);
        } catch (InterruptedException e){
            logger.error("Error in landing", e);
        }

        terminalSemaphore = terminal.getSemaphore();
        terminal.removeCurrentPlain();
        terminalSemaphore.release();
        plain.setTerminal(null);
        plain.setState(Plain.State.LEAVING);
    }

    public void boarding(){
        terminal.setCurrentPlain(plain);
        try {
            Thread.sleep(4000);
            AirportAction.boardingPassenger(plain);
        } catch (InterruptedException e){
            logger.error("Error while boarding", e);
        }
        terminalSemaphore = terminal.getSemaphore();
        terminal.removeCurrentPlain();
        terminalSemaphore.release();
        plain.setTerminal(null);
        plain.setState(Plain.State.LEAVING);
    }

    public void leaving(){
        logger.debug("Plain " + plain.getId() + " successfully leave");
        plain.setState(Plain.State.LEAVE);
    }
}
