package by.training.task.entity;

import by.training.task.action.PassengerAction;

/**
 * Created by Nik on 29.09.2015.
 */
public class Passenger implements Runnable{
    public enum State{NEW, WAITING, ON_PLAIN, BOARDING, LEAVING, LEFT}

    private int id;
    private Ticket ticket;
    private WaitingHall waitingHall;
    private State state = State.NEW;
    private int plainId;

    public Passenger(){

    }

    public Passenger(int id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public int getPlainId() {
        return plainId;
    }

    public void setPlainId(int plainId) {
        this.plainId = plainId;
    }

    public void setWaitingHall(WaitingHall waitingHall){
        this.waitingHall = waitingHall;
    }

    public WaitingHall getWaitingHall() {
        return waitingHall;
    }

    public int getId(){
        return this.id;
    }

    public void setTickets(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "id=" + id +
                ", ticket = " + ticket.getPlainId() +
                ", plain = " + plainId +
                '}';
    }

    @Override
    public void run() {
        PassengerAction passengerAction = new PassengerAction(this);

        while(state != State.LEFT){
            switch (state) {
                case WAITING:
                    passengerAction.goWaitingHall();
                    this.setState(State.BOARDING);
                    break;
                case LEAVING:
                    passengerAction.leaving();
                    break;
                case BOARDING:
                    passengerAction.boarding();
                    break;
                case ON_PLAIN:
                    passengerAction.onPlain();
                    break;
                case LEFT:
                    break;
                default:
                    System.out.println("Something wrong");
                    break;
            }
        }

    }
}
