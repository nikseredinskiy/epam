package com.epam.spring.core.main;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.spring.core.beans.Client;
import com.epam.spring.core.beans.Event;
import com.epam.spring.core.beans.EventType;
import com.epam.spring.core.logger.EventLogger;

public class App {
	
	private Client client;
	private EventLogger eventLogger;
	private EventLogger defaultLogger;
	private Map<EventType, EventLogger> loggers;
	
	public void setDefaultLogger(EventLogger defaultLogger){
		this.defaultLogger = defaultLogger;
	}
	
	public App(Client client, EventLogger eventLogger, Map<EventType, EventLogger> loggers) {
		super();
		this.client = client;
		this.eventLogger = eventLogger;
		this.loggers = loggers;
	}

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		
		App app = (App) ctx.getBean("app");
		Event event = (Event) ctx.getBean("event");
		event.setMsg("Someone like you DEFAULT LOGGER");
		
		app.logEvent(null, event);
		
	}
	
	private void logEvent(EventType type, Event event){
		/*EventLogger logger = loggers.get(type);
		
		if (logger == null){
			logger = eventLogger;
		}*/
		
		EventLogger logger = defaultLogger;
		logger.logEvent(event);		
	}

}
