package by.training.task.creator;

import by.training.task.action.DiscAction;
import by.training.task.entity.Disc;
import by.training.task.exception.DiscException;

/**
 * Created by Nik on 22.09.2015.
 */
public class DiscCreator {
    public static Disc createMusicDisc() throws DiscException {
        Disc disc = new Disc();

        DiscAction.recordDisk(disc, SongCreator.createSongs());
        return disc;
    }
}
