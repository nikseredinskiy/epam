package by.training.task.entity;

import java.util.ArrayList;

/**
 * Created by Nik on 05.10.2015.
 */
public interface Component {
    void add(Component component);
    void remove(Component component);
    Component getComponent(int index);
    ArrayList<Component> getComponentList();
    LeafPart.LeafType getLeafType();

}
