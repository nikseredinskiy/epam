package com.epam.spring.core.logger;

import java.util.Iterator;
import java.util.List;

import com.epam.spring.core.beans.Event;

public class CombinedEventLogger implements EventLogger{
	
	private List<EventLogger> loggers;
		
	public CombinedEventLogger(List<EventLogger> loggers) {
		super();
		this.loggers = loggers;
	}

	public void logEvent(Event event) {
		for (Iterator<EventLogger> iterator = loggers.iterator(); iterator.hasNext();) {
			EventLogger eventLogger = (EventLogger) iterator.next();
			eventLogger.logEvent(event);
		}
	}

}
