package com.epam.task.dao;

import java.util.List;

import com.epam.task.entity.News;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.exception.DAOException;

public interface NewsDAO {
	/**
	 * @param news
	 * @return generated news id
	 * @throws DAOException
	 */
	Long insert(News news) throws DAOException;

	/**
	 * @param news
	 * @throws DAOException
	 */
	void update(News news) throws DAOException;

	/**
	 * @param news
	 * @throws DAOException
	 */
	void delete(News news) throws DAOException;

	/**
	 * @return List of all news
	 * @throws DAOException
	 */
	List<News> getAllNews() throws DAOException;

	/**
	 * @param newsId
	 * @return news
	 * @throws DAOException
	 */
	News getNews(Long newsId) throws DAOException;

	/**
	 * Return list of news sorted by most commented news
	 * 
	 * @return List of news
	 * @throws DAOException
	 */
	List<News> getNewsByCommCount() throws DAOException;

	/**
	 * @return number of all news
	 * @throws DAOException
	 */
	int countAllNews() throws DAOException;

	/**
	 * Return list of news according to searchCriteria that may include authorId
	 * and list of tags
	 * 
	 * @param searchCriteria
	 * @return List of news
	 * @throws DAOException
	 */
	List<News> getNewsBySeacrh(SearchCriteria searchCriteria)
			throws DAOException;
}
