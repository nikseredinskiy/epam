package by.training.task.main;

import by.training.task.action.DiscAction;
import by.training.task.creator.DiscCreator;
import by.training.task.entity.Disc;
import by.training.task.exception.DiscException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Nik on 22.09.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Runner.class);
    public static final int BEGIN_MOMENT = 4;
    public static final int END_MOMENT = 6;

    public static void main(String[] args) {
        try {
            Disc disc = DiscCreator.createMusicDisc();

            logger.debug(DiscAction.findInInterval(disc, BEGIN_MOMENT, END_MOMENT));

            DiscAction.sortByGenre(disc);
            logger.debug(disc);

        } catch (DiscException e) {
            logger.error("Exception while disc creating process",e);
        }
    }
}
