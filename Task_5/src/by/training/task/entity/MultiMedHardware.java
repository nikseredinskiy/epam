package by.training.task.entity;

/**
 * Created by Nik on 17.10.2015.
 */
public class MultiMedHardware extends Hardware {
    private int speakersCount;
    private boolean hasSub;
    private boolean hasDolby;

    public MultiMedHardware(){

    }

    public MultiMedHardware(int speakersCount, boolean hasSub, boolean hasDolby) {
        this.speakersCount = speakersCount;
        this.hasSub = hasSub;
        this.hasDolby = hasDolby;
    }

    public MultiMedHardware(String name, String origin, int price, boolean isCritical, Type type, int speakersCount, boolean hasSub, boolean hasDolby) {
        super(name, origin, price, isCritical, type);
        this.speakersCount = speakersCount;
        this.hasSub = hasSub;
        this.hasDolby = hasDolby;
    }

    public int getSpeakersCount() {
        return speakersCount;
    }

    public void setSpeakersCount(int speakersCount) {
        this.speakersCount = speakersCount;
    }

    public boolean isHasSub() {
        return hasSub;
    }

    public void setHasSub(boolean hasSub) {
        this.hasSub = hasSub;
    }

    public boolean isHasDolby() {
        return hasDolby;
    }

    public void setHasDolby(boolean hasDolby) {
        this.hasDolby = hasDolby;
    }

    @Override
    public String toString() {
        return super.toString() + " MulltiMedHardware{" +
                "speakersCount=" + speakersCount +
                ", hasSub=" + hasSub +
                ", hasDolby=" + hasDolby +
                '}';
    }
}
