package com.epam.task.service;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.AuthorDAO;
import com.epam.task.entity.Author;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;
import com.epam.task.util.TestUtil;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
	private static Logger logger = Logger.getLogger(AuthorServiceTest.class);
	
	private AuthorDAO authorDAO;
	
	@Autowired
	private AuthorService authorService;
	
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	@Before
	public void setupMock(){
		authorDAO = mock(AuthorDAO.class);
		authorService.setAuthorDAO(authorDAO);
		
	}
	
	@Test
	public void createTest(){
		Author author1 = TestUtil.createAuthor(1L);
		Author author2 = TestUtil.createAuthor(2L);
		
		try {
			when(authorDAO.create(author1)).thenReturn(author1.getId());
			when(authorDAO.create(author2)).thenReturn(author2.getId());
		} catch (DAOException e) {
			e.printStackTrace();
		}
		
		try {
			assertEquals(authorService.createAuthor(author1), author1.getId());
		} catch (ServiceExeption e) {
			logger.error("Error while createTest()", e);
		}
	}
	
	@Test
	public void getNewsAuthorTest(){
		Author author1 = TestUtil.createAuthor(1L);
		Author author2 = TestUtil.createAuthor(2L);
		
		try {
			when(authorDAO.create(author1)).thenReturn(author1.getId());
			when(authorDAO.create(author2)).thenReturn(author2.getId());
			when(authorDAO.getNewsAuthor(1L)).thenReturn(author1);
			when(authorDAO.getNewsAuthor(2L)).thenReturn(author2);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		
		try {
			assertEquals(authorService.getNewsAuthor(1L), author1);
			assertEquals(authorService.getNewsAuthor(2L), author2);
		} catch (ServiceExeption e) {
			logger.error("Error while getNewsAuthorTest()", e);
		}
	}
}
