package com.epam.newsappclient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsappclient.dao.CommentDAO;
import com.epam.newsappclient.entity.Comment;
import com.epam.newsappclient.exception.DAOException;
import com.epam.newsappclient.exception.ServiceExeption;

@Service
public class CommentService {
	
	@Autowired
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public long addComment(Comment comment) throws ServiceExeption{
		long id;
		
		try {
			id = commentDAO.addComment(comment);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while add()", e);
		}
		
		return id;
	}
	
	public void deleteComment(long commentId) throws ServiceExeption{
		try {
			commentDAO.deleteComment(commentId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}
	
	
	public void deleteNewsComment(long newsId) throws ServiceExeption{
		try {
			commentDAO.deleteNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsComent()", e);
		}
	}
	
	public List<Comment> getNewsComments(long newsId) throws ServiceExeption{
		try {
			List<Comment> comments = commentDAO.getNewsComments(newsId);
			
			return comments;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsComments()", e);
		}
	}
}
