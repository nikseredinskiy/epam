package by.training.task.action;

import by.training.task.entity.Hardware;

import java.util.Set;

/**
 * Created by Nik on 01.11.2015.
 */
public class ParseAction {

    public Set<Hardware> parseXML(String type, String path) {
        Set<Hardware> hardwares;

        switch (type) {
            case "sax":
                HardwareSAXBuilder saxBuilder = new HardwareSAXBuilder();
                saxBuilder.buildSetHardwares(path);
                hardwares = saxBuilder.getHardwares();
                break;
            case "dom":
                HardwareDOMBuilder domBuilder = new HardwareDOMBuilder();
                domBuilder.buildSetHardwares(path);
                hardwares = domBuilder.getHardwares();
                break;
            case "stax":
                HardwareStAXBuilder stAXBuilder = new HardwareStAXBuilder();
                stAXBuilder.buildSetHardwares(path);
                hardwares = stAXBuilder.getHardwares();
                break;
            default:
                hardwares = null;
                break;
        }

        return hardwares;
    }
}
