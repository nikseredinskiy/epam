package by.training.task.creator;

import by.training.task.entity.Plain;

import java.util.ArrayList;

/**
 * Created by Nik on 29.09.2015.
 */
public class PlainCreator {
    public ArrayList<Plain> createPlain(){
        ArrayList<Plain> plains = new ArrayList<Plain>();

        plains.add(new Plain(1, 5));
        plains.add(new Plain(2, 5));
        plains.add(new Plain(3, 5));
        plains.add(new Plain(4, 5));
        plains.add(new Plain(5, 5));

        return plains;
    }
}
