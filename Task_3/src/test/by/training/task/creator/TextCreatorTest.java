package test.by.training.task.creator;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Nik on 12.10.2015.
 */
public class TextCreatorTest {

    static Logger logger = Logger.getLogger(TextCreatorTest.class);

    @Test
    public void createTextTest(){
        String path = "textTest.txt";
        String actual = "";
        String expected = "Text for test.";
        try {
            FileInputStream inFile = new FileInputStream(path);
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            actual = new String(str);
        } catch (FileNotFoundException e) {
            logger.error("Can't find file!", e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Can't read text!", e);
            e.printStackTrace();
        }
        Assert.assertEquals(expected, actual);
    }
}
