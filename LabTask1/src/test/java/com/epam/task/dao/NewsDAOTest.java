package com.epam.task.dao;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.config.DBUnitConfig;
import com.epam.task.entity.News;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
public class NewsDAOTest extends DBUnitConfig {

	@Autowired
	private NewsDAO newsDAO;

	public NewsDAOTest() {
		super();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/news/news-data.xml"));

		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	@Test
	public void testInsertNews() throws Exception{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		Date date = new Date();
		newsDAO.insert(new News("Title", "shortText", "fullText",date, date));

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/news/news-data-expected.xml"));
		
		ReplacementDataSet rDataSet = new ReplacementDataSet(expectedData);
		rDataSet.addReplacementObject("[create_date]", dateFormat.format(date));
		rDataSet.addReplacementObject("[modification_date]", dateFormat.format(date));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "NEWS_ID", "CREATION_DATE", "MODIFICATION_DATE"};
		Assertion.assertEqualsIgnoreCols(rDataSet, actualData, "NEWS", ignore);
	}

	@Test
	public void testUpdateNews() throws Exception {
		Date date = new Date();
		newsDAO.update(new News(1L, "New Title", "New SHTT", "UPD FLLT", date,
				date));

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/news/news-upd-data-expected.xml"));

		ReplacementDataSet rDataSet = new ReplacementDataSet(expectedData);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		rDataSet.addReplacementObject("[modification_date]",
				dateFormat.format(date));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {};
		Assertion.assertEqualsIgnoreCols(rDataSet, actualData, "NEWS", ignore);
	}

	@Test
	public void testDeleteNews() throws Exception {
		newsDAO.delete(new News(1L, "New Title", "New SHTT", "UPD FLLT",
				new Date(), new Date()));

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/news/news-del-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {};
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, "NEWS",
				ignore);
	}

	@Test
	public void testGetAllNews() throws Exception {
		newsDAO.getAllNews();

		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/news/news-data.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {};
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, "NEWS",
				ignore);
	}

}
