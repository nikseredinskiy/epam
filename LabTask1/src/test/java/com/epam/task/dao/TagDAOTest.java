package com.epam.task.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.config.DBUnitConfig;
import com.epam.task.entity.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
public class TagDAOTest extends DBUnitConfig{
	
	@Autowired
	private TagDAO tagDAO;
	
	public TagDAOTest(){
		super();
	}
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/tag/tag-data.xml"));

		tester.setDataSet(beforeData);
		tester.onSetup();
	}
	
	@Test
	public void testCreateTag() throws Exception{
		tagDAO.create(new Tag("F1"));
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/tag/tag-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "TAG_ID" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"TAG", ignore);
	}
	
	@Test
	public void testDeleteTag() throws Exception{
		tagDAO.delete(new Tag(2L, "SPORT"));
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/tag/tag-del-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "TAG_ID" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"TAG", ignore);
	}
	
	@Test
	public void testInsertNewsTag() throws Exception{
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1L, "SOCCER")); tags.add((new Tag(2L, "SPORT")));
		tagDAO.insertNewsTag(4L, tags);
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/tag/tag-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"NEWS_TAG", ignore);
	}
	
	@Test
	public void testDeleteNewsTag() throws Exception{
		tagDAO.deleteNewsTags(3L);
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/tag/tag-nt-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"NEWS_TAG", ignore);
	}
}
