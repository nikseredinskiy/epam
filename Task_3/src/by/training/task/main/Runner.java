package by.training.task.main;

import by.training.task.action.ParserAction;
import by.training.task.action.TextAction;
import by.training.task.entity.CompositePart;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Nik on 06.10.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        String path = "text.txt";
        ParserAction parserAction = new ParserAction();

        CompositePart fullText = parserAction.parse(path);
        logger.debug("Full: \n" + TextAction.assembleText(fullText));

        TextAction.orderBySize(fullText);
        TextAction.containCount(fullText);
    }
}
