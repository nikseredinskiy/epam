package com.epam.task.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.entity.Author;
import com.epam.task.entity.Comment;
import com.epam.task.entity.News;
import com.epam.task.entity.NewsVO;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.entity.Tag;
import com.epam.task.exception.ServiceExeption;
import com.epam.task.util.TestUtil;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsVOServiceTest {
	private static Logger logger = Logger.getLogger(NewsVOServiceTest.class);
	
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private NewsVOService newsVOService;
	
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setNewsVOService(NewsVOService newsVOService) {
		this.newsVOService = newsVOService;
	}

	@Before
	public void setupMock() {
		newsService = mock(NewsService.class);
		authorService = mock(AuthorService.class);
		tagService = mock(TagService.class);
		commentService = mock(CommentService.class);
		
		newsVOService.setNewsService(newsService);
		newsVOService.setAuthorService(authorService);
		newsVOService.setTagService(tagService);
		newsVOService.setCommentService(commentService);

	}
	
	@Test
	public void insertTest(){
		NewsVO newsVO1 = TestUtil.createNewsVO(1L);
		
		try {
			when(newsService.insertNews(newsVO1.getNews())).thenReturn(newsVO1.getNews().getId());
			assertEquals(newsVOService.insert(newsVO1), newsVO1.getNews().getId());
		} catch (ServiceExeption e) {
			logger.error("Error while insertTest()", e);
		}
	}
	
	@Test
	public void getAllNewsTest(){
		List<News> newsList = new ArrayList<News>();
		newsList.add(TestUtil.createNews(1L));
		newsList.add(TestUtil.createNews(2L));
		
		List<NewsVO> newsVO = new ArrayList<NewsVO>();
		newsVO.add(new NewsVO(newsList.get(0), TestUtil.createAuthor(1L), null, null));
		newsVO.add(new NewsVO(newsList.get(1), TestUtil.createAuthor(2L), null, null));
		try {
			when(newsService.getAllNews()).thenReturn(newsList);
			when(authorService.getNewsAuthor(1L)).thenReturn(TestUtil.createAuthor(1L));
			when(authorService.getNewsAuthor(2L)).thenReturn(TestUtil.createAuthor(2L));
			when(tagService.getNewsTag(1L)).thenReturn(null);
			when(commentService.getNewsComments(1L)).thenReturn(null);
			when(tagService.getNewsTag(2L)).thenReturn(null);
			when(commentService.getNewsComments(2L)).thenReturn(null);
			
			assertEquals(newsVOService.getAllNews(), newsVO);
		} catch (ServiceExeption e) {
			logger.error("Error while getAllNewsTest()", e);
		}
	}
	
	@Test
	public void getNewsTest(){
		News news1 = TestUtil.createNews(1L);		
		Author author1 = TestUtil.createAuthor(1L);
		List<Tag> tagList1 = new ArrayList<Tag>();
		tagList1.add(TestUtil.createTag(1L));
		tagList1.add(TestUtil.createTag(2L));
		
		List<Comment> commList1 = new ArrayList<Comment>();
		commList1.add(TestUtil.createComment(1L, 1L));
		commList1.add(TestUtil.createComment(2L, 1L));
		
		NewsVO newsVO1 = new NewsVO(news1, author1, tagList1, commList1);
		
		try {
			when(newsService.getNews(1L)).thenReturn(news1);
			when(authorService.getNewsAuthor(1L)).thenReturn(author1);
			when(tagService.getNewsTag(1L)).thenReturn(tagList1);
			when(commentService.getNewsComments(1L)).thenReturn(commList1);
			assertEquals(newsVOService.getNews(1L), newsVO1);
		} catch (ServiceExeption e) {
			logger.error("Error while getNewsTest()", e);
		}
	}
	
	@Test
	public void getNewsByCommCountTest(){
		List<News> newsList = new ArrayList<News>();
		News news1 = TestUtil.createNews(1L);
		News news2 = TestUtil.createNews(2L);
		newsList.add(news1);
		newsList.add(news2);
		
		List<Comment> commList2 = new ArrayList<Comment>();
		commList2.add(TestUtil.createComment(3L, 2L));
		
		List<NewsVO> newsVO = new ArrayList<NewsVO>();
		newsVO.add(new NewsVO(news1, TestUtil.createAuthor(1L), null, null));
		newsVO.add(new NewsVO(news2, TestUtil.createAuthor(2L), null, commList2));
		
		try {
			when(newsService.getNewsByCommCount()).thenReturn(newsList);
			when(authorService.getNewsAuthor(1L)).thenReturn(TestUtil.createAuthor(1L));
			when(tagService.getNewsTag(1L)).thenReturn(null);
			when(commentService.getNewsComments(1L)).thenReturn(null);
			
			when(authorService.getNewsAuthor(2L)).thenReturn(TestUtil.createAuthor(2L));
			when(tagService.getNewsTag(2L)).thenReturn(null);
			when(commentService.getNewsComments(2L)).thenReturn(commList2);
			
			assertEquals(newsVOService.getNewsByCommCount(), newsVO);
		} catch (ServiceExeption e) {
			logger.error("Error while getNewsByCommCountTest()", e);
		}
	}
	
	@Test
	public void getNewsBySeacrhTest(){
		SearchCriteria searchCriteria = TestUtil.createSearchCriteria(1L);
		
		List<NewsVO> newsVOSearch = new ArrayList<NewsVO>();
		
		List<News> newsListSearch = new ArrayList<News>();
		News news = TestUtil.createNews(1L);
		newsListSearch.add(news);
		
		Author author1 = TestUtil.createAuthor(1L);
		List<Tag> tagList1 = new ArrayList<Tag>();
		tagList1.add(TestUtil.createTag(1L));
		tagList1.add(TestUtil.createTag(2L));
		
		List<Comment> commList1 = new ArrayList<Comment>();
		commList1.add(TestUtil.createComment(1L, 1L));
		commList1.add(TestUtil.createComment(2L, 1L));
		
		newsVOSearch.add(new NewsVO(news, author1, tagList1, commList1));
		
		try {
			when(newsService.getNewsBySeacrh(searchCriteria)).thenReturn(newsListSearch);
		
			when(authorService.getNewsAuthor(1L)).thenReturn(author1);
			when(tagService.getNewsTag(1L)).thenReturn(tagList1);
			when(commentService.getNewsComments(1L)).thenReturn(commList1);
			assertEquals(newsVOService.getNewsBySeacrh(searchCriteria), newsVOSearch);
		} catch (ServiceExeption e) {
			logger.error("Error while getNewsBySeacrhTest()", e);
		}
	}
}
