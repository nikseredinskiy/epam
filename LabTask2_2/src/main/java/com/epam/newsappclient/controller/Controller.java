package com.epam.newsappclient.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsappclient.command.ActionCommand;
import com.epam.newsappclient.util.Parameter;

@SuppressWarnings("serial")
public class Controller extends HttpServlet {
	private ApplicationContext context;

	@Override
	public void init() {
		context = new ClassPathXmlApplicationContext("root-context.xml");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String commandName = request.getParameter(Parameter.COMMAND);

		if (commandName == null) {
			commandName = Parameter.EMPTY_COMMAND;
		}

		ActionCommand command = (ActionCommand) context.getBean(commandName);
		String page = command.execute(request);
		
		request.getRequestDispatcher(page).forward(request, response);
	}

}
