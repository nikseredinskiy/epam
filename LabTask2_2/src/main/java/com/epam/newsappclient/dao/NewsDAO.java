package com.epam.newsappclient.dao;

import java.util.List;

import com.epam.newsappclient.entity.News;
import com.epam.newsappclient.entity.SearchCriteria;
import com.epam.newsappclient.exception.DAOException;

public interface NewsDAO {
	/**
	 * @param news
	 * @return generated news id
	 * @throws DAOException
	 */
	long insertNews(News news) throws DAOException;

	/**
	 * @param news
	 * @throws DAOException
	 */
	long updateNews(News news) throws DAOException;

	/**
	 * @param news
	 * @throws DAOException
	 */
	void deleteNews(long newsId) throws DAOException;

	/**
	 * @return List of all news
	 * @throws DAOException
	 */
	List<News> getAllNews() throws DAOException;

	/**
	 * @param newsId
	 * @return news
	 * @throws DAOException
	 */
	News getNews(long newsId) throws DAOException;

	/**
	 * Return list of news sorted by most commented news
	 * 
	 * @return List of news
	 * @throws DAOException
	 */
	List<News> getNewsByCommCount() throws DAOException;

	/**
	 * @return number of all news
	 * @throws DAOException
	 */
	int countAllNews() throws DAOException;

	/**
	 * Return list of news according to searchCriteria that may include authorId
	 * and list of tags
	 * 
	 * @param searchCriteria
	 * @return List of news
	 * @throws DAOException
	 */
	List<News> getNewsBySeacrh(SearchCriteria searchCriteria, long pageNum,
			long newsOnPage) throws DAOException;

	int countNewsBySeacrh(SearchCriteria searchCriteria) throws DAOException;

	/**
	 * Return news on pageNum page
	 * 
	 * @param pageNum
	 * @return List of News
	 * @throws DAOException
	 */
	List<News> getNewsOnPage(long pageNum, long newsOnPage) throws DAOException;
}
