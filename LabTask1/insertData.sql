--------------------------------------------------------
--  File created - Friday-March-04-2016   
--------------------------------------------------------

REM INSERTING into NIK.AUTHOR
SET DEFINE OFF;
Insert into NIK.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (1,'Nik Seredinskiy',to_timestamp('08-FEB-16 07.32.30.519000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (2,'James Allen',to_timestamp('12-FEB-14 09.33.19.498000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (120,'Mickey Mouse',null);
Insert into NIK.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (121,'Thad Castle',null);
REM INSERTING into NIK.COMMENTS
SET DEFINE OFF;
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (108,126,'King!',to_timestamp('29-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (1,2,'Really great news!',to_timestamp('08-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (2,2,'Okey, note that',to_timestamp('08-FEB-16 01.07.36.410000000 PM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (117,125,'And forget to do his math homework',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (5,3,'Hulk power!!!',to_timestamp('24-FEB-16 01.46.43.028000000 PM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (109,126,'Golden states for champs!!!',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (105,2,'They cancel it until Spanish GP',to_timestamp('29-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (121,127,'First!!! Oh wait...',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (116,127,'Very interesting, tnx',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (119,128,'Hahahahah, very funny xD',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into NIK.COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (120,139,'Raikkonen 4 champs this year!!!',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));

REM INSERTING into NIK.NEWS
SET DEFINE OFF;
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (127,'Sugar in hot drinks','Experts analyse hot drinks.','They find out that there is a lot of sugar in the drinks. The worst are mochas and lattes.
In a can of Coke, there are 9 teaspoons of sugar. In Caffe Nero''s Caramel Latte, there are 13 teaspoons of sugar. In a Costa Chai Latte, there are 20 teaspoons of sugar. In a Starbucks?s hot mulled fruit drink, there are 25 teaspoons of sugar!',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('01-MAR-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (3,'Hulkenberg on top in Barcelona','Force India''s Nico Hulkenberg led the way on the third morning of pre-season Formula 1 testing.','Force India''s Nico Hulkenberg led the way on the third morning of pre-season Formula 1 testing, as Ferrari endured a frustrating start to Wednesday''s running.
 Kimi Raikkonen took over the Ferrari SF16-H after Sebastian Vettel topped two near-faultless days at Barcelona, but only managed four laps and did not record a time due to fuel system checks.
 On Pirelli''s medium tyre, Hulkenberg led the way at the lunchbreak with a time of 1m25.266s, a day after team-mate Sergio Perez declared the 2016 Force India had "big potential".
 Hulkenberg''s time was nearly three quarters of a second quicker than Kevin Magnussen on his debut for Renault, albeit with the Dane on soft rubber.
 Team-mate Jolyon Palmer had two frustrating days at the wheel of the RS16, suffering software issues on Monday and a turbo failure on Tuesday, managing only 79 laps in total.
',to_timestamp('24-FEB-16 01.43.13.731000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('24-FEB-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (2,'F1 new qualifying format','Formula 1 bosses have voted unanimously to deliver a dramatic overhaul of the qualifying format.','Formula 1 bosses have voted unanimously to deliver a dramatic overhaul of the qualifying format for the upcoming season. Following a meeting of the Strategy Group and F1 Commission in Geneva, the format change has been voted through to spice up the show. Qualifying will remain as a one-hour session, split into three segments, but drivers must be on track throughout each part until they get knocked out. Details on how the new qualifying format will work exactly have yet to be decided, but the general outline has been agreed.',to_timestamp('08-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('09-FEB-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (128,'Joke about a plane','Joke about a plane. This joke is about one woman.','This woman has blond hair. The woman bought a ticket for a plane. It was a ticket to Majorca. It was in tourist class, but this woman sat down in business class.
A stewardess told her that her ticket is in tourist class, but the woman didn?t want to change her place.
So, the stewardess went to the pilot. She asked him for help. The pilot went to the woman. He said something into her ear. The woman stood up and sat down in tourist class.
The stewardess didn?t understand and she asked the pilot, ?What did you tell her??
The pilot said, ?I told her that business class is not flying to Majorca.?',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('01-MAR-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (133,'qwe','qwe','qwe',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('01-MAR-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (135,'Kevin targets title','Kevin Magnussen targets title with Renault Sport Racing','Danish driver Kevin Magnussen believes he can win the world title on his return to Formula 1 with Renault Sport Racing.',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('01-MAR-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (125,'Ratford destroy Arsenal','MU won game against Arsenal on Old Traford','MU won the game against third Premier League club. Ratford scored two goals in the first half and winning goal was scored by Andrea Herrera. ',to_timestamp('29-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('29-FEB-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (126,'Another Stephen Curry record','Stephen Curry set another amazing record','Stephen Curry eventually set a new league record for three-pointers made in a single season',to_timestamp('29-FEB-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('29-FEB-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (139,'Raikkonen puts Ferrari on top','It proved to be a more successful morning for Raikkonen.','Kimi Raikkonen used the ultra soft tyres to set the pace on the fourth and final morning of the first pre-season Formula 1 test at Barcelona.
The Finn clocked a 1m23.477s in the Ferrari just before lunch to finish.',to_timestamp('01-MAR-16 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('01-MAR-16','DD-MON-RR'));
Insert into NIK.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (140,'asd','asd','asd',to_timestamp('04-MAR-16 02.53.20.487000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('04-MAR-16','DD-MON-RR'));

REM INSERTING into NIK.NEWS_AUTHOR
SET DEFINE OFF;
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (2,1);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (127,120);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (3,2);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (128,120);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (133,2);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (125,1);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (126,120);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (135,2);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (139,1);
Insert into NIK.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (140,1);

REM INSERTING into NIK.NEWS_TAG
SET DEFINE OFF;
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (2,1);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (125,2);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (3,1);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (3,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (125,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (2,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (126,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (126,100);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (127,101);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (128,103);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (133,1);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (133,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (135,1);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (135,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (139,1);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (139,4);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (139,101);
Insert into NIK.NEWS_TAG (NEWS_ID,TAG_ID) values (140,101);

REM INSERTING into NIK.ROLES
SET DEFINE OFF;

REM INSERTING into NIK.TAG
SET DEFINE OFF;
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (1,'Formula 1');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (2,'Soccer');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (3,'Hockey');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (4,'Sport');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (100,'Basketball');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (101,'News');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (102,'Auto');
Insert into NIK.TAG (TAG_ID,TAG_NAME) values (103,'Joke');
REM INSERTING into NIK.USERS
SET DEFINE OFF;
