package com.epam.task.service;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.NewsDAO;
import com.epam.task.entity.News;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;
import com.epam.task.service.NewsService;
import com.epam.task.util.TestUtil;

@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {
	private static Logger logger = Logger.getLogger(NewsServiceTest.class);

	private NewsDAO newsDAO;

	@Autowired
	private NewsService newsService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	@Before
	public void setupMock() {
		newsDAO = mock(NewsDAO.class);

		newsService.setNewsDAO(newsDAO);

	}

	@Test
	public void createTest() {
		News news1 = TestUtil.createNews(1L);

		try {
			when(newsDAO.insert(news1)).thenReturn(news1.getId());
		} catch (DAOException e) {
			logger.error("Error while createTest()", e);
		}
		try {
			assertEquals(newsService.insertNews(news1), news1.getId());
		} catch (ServiceExeption e) {
			logger.error("Error while createTest()", e);
		}
	}

	@Test
	public void countAllNewsTest() {
		List<News> newsList = new ArrayList<News>();
		newsList.add(TestUtil.createNews(1L));
		newsList.add(TestUtil.createNews(2L));

		try {
			when(newsDAO.countAllNews()).thenReturn(newsList.size());
		} catch (DAOException e) {
			logger.error("Error while countAllNewsTest()", e);
		}
		
		try {
			assertEquals(newsService.countAllNews(), 2);
		} catch (ServiceExeption e) {
			logger.error("Error while countAllNewsTest()", e);
		}
	}

	@Test
	public void getAllNewsTest() {
		List<News> newsList = null;
		
		try {
			newsList = newsDAO.getAllNews();
		} catch (DAOException e) {
			logger.error("Error while getAllNewsTest()", e);
		}
		
		try {
			when(newsDAO.getAllNews()).thenReturn(newsList);
		} catch (DAOException e) {
			logger.error("Error while getAllNewsTest()", e);
		}

		for (int i = 0; i < newsList.size(); i++) {
			assertEquals("News" + (i + 1) + " Title", newsList.get(i)
					.getTitle());
		}
	}

	@Test
	public void getNewsTest() {
		News news = null;
		News news1 = TestUtil.createNews(1L);
		
		try {
			when(newsDAO.getNews(new Long(1))).thenReturn(news1);
		} catch (DAOException e) {
			e.printStackTrace();
		}

		try {
			news = newsDAO.getNews(new Long(1));
		} catch (DAOException e) {
			logger.error("Error while getNewsTest()", e);
		}

		assertEquals(new Long(1), news.getId());
		assertEquals("News1 Title", news.getTitle());
		assertEquals("News1 STxt", news.getShortText());
		assertEquals("News1 FTxt", news.getFullText());
	}
}
