package com.epam.newsappclient.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsappclient.dao.AuthorDAO;
import com.epam.newsappclient.entity.Author;
import com.epam.newsappclient.exception.DAOException;
import com.epam.newsappclient.util.ConnectionUtil;

@Repository
public class AuthorDAOImpl implements AuthorDAO {
	
	@Autowired
	
	private DataSource dataSource;
	
	private final String CREATE = "INSERT INTO AUTHOR VALUES(AUTHOR_ID_SEQ.NEXTVAL,?,?)";
	private final String INSERT = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES(?,?)";
	private final String UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
	private final String EXPIRE = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private final String DELETE = "DELETE FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.NEWS_ID = ?";
	private final String GET_AUTHOR = "SELECT NEWS_ID, A.AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM NEWS_AUTHOR NA INNER JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID WHERE NEWS_ID = ?";
	private final String GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";

	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	
	public long createAuthor(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = -1;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(CREATE, new String [] {"AUTHOR_ID"});

			ps.setString(1, author.getName());
			
			if(author.getExpired() != null){
				ps.setDate(2, new java.sql.Date(author.getExpired().getTime()));
			} else {
				ps.setDate(2, null);
			}
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while create()" + e, e);
		} finally{
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
	}
	
	public long updateAuthor(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, author.getName());
			ps.setLong(2, author.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while update()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return author.getId();
	}
	
	public void expireAuthor(Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(EXPIRE);
			ps.setDate(1, new java.sql.Date(new Date().getTime()));
			ps.setLong(2, author.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while expireAuthor()" + e, e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}

	public void insertNewsAuthor(long newsId, Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(INSERT);

			ps.setLong(1, newsId);
			ps.setLong(2, author.getId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void updateNewsAuthor(long newsId, Author author) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(UPDATE);

			ps.setLong(1, author.getId());
			ps.setLong(2, newsId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while update()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsAuthor(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE);

			ps.setLong(1, newsId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public Author getNewsAuthor(long newsId) throws DAOException{
		Connection conn = null;
		Author author = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(GET_AUTHOR);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				author = new Author(rs.getInt("AUTHOR_ID"), rs.getString("AUTHOR_NAME"), rs.getDate("EXPIRED"));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsAuthor()" + e, e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return author;
	}
	
	public List<Author> getAllAuthors() throws DAOException{
		Connection conn = null;
		Author author = null;
		Statement st = null;
		ResultSet rs = null;
		List<Author> authorList = new ArrayList<Author>();

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
				
			rs = st.executeQuery(GET_ALL_AUTHORS);
			while(rs.next()){
				author = new Author(rs.getInt("AUTHOR_ID"), rs.getString("AUTHOR_NAME"), rs.getDate("EXPIRED"));
				
				if(author.getExpired() == null){
					authorList.add(author);
				}
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getAllAuthors()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}
		
		return authorList;
	}
}
