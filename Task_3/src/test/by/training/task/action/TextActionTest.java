package test.by.training.task.action;

import by.training.task.action.ParserAction;
import by.training.task.action.TextAction;
import by.training.task.entity.Component;
import by.training.task.entity.CompositePart;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Nik on 12.10.2015.
 */
public class TextActionTest {

    @Test
    public void orderBySize(){
        String path = "textTest.txt";
        ParserAction parserAction = new ParserAction();
        CompositePart fullTextTest = parserAction.parse(path);

        ArrayList<Component> actual =  TextAction.orderBySize(fullTextTest);
        ArrayList<Component> expected = new ArrayList<Component>(){
            {

            }
        };
    }

    @Test
    public void containCount(){
        String path = "textTest.txt";
        ParserAction parserAction = new ParserAction();
        CompositePart fullTextTest = parserAction.parse(path);

        HashMap<String, Integer> actual = TextAction.containCount(fullTextTest);
        HashMap<String, Integer> expected = new HashMap<String, Integer>(){
            {
                this.put("test", 1);
                this.put("for", 1);
                this.put("text", 1);
                this.put(".", 1);
            }
        };

        Assert.assertEquals(expected, actual);
    }
}
