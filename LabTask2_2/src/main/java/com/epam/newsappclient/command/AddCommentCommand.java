package com.epam.newsappclient.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsappclient.entity.Comment;
import com.epam.newsappclient.exception.ServiceExeption;
import com.epam.newsappclient.service.CommentService;
import com.epam.newsappclient.service.NewsVOService;
import com.epam.newsappclient.util.Parameter;
import com.epam.newsappclient.util.Validator;

public class AddCommentCommand implements ActionCommand{
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	NewsVOService newsVOService;
	
	private Logger logger = Logger.getLogger(AddCommentCommand.class);

	public String execute(HttpServletRequest request) {
		Comment comment = new Comment();
		
		Long newsId = Long.parseLong(request.getParameter(Parameter.NEWS_ID));
		String commentText = request.getParameter(Parameter.COMMENT_TEXT);
		
		if(Validator.isValid(commentText)){
			comment.setCommentText(commentText);
			comment.setNewsID(newsId);
			
			try {
				commentService.addComment(comment);
			} catch (ServiceExeption e) {
				logger.error("Service exception in AddCommentCommand" + e, e);
			}
		}
		
		try {
			request.setAttribute(Parameter.NEWS_VO, newsVOService.getNews(newsId));
			request.setAttribute(Parameter.CURRENT_PAGE, request.getParameter(Parameter.CURRENT_PAGE));
		} catch (ServiceExeption e) {
			logger.error("Service exception in AddCommentCommand" + e, e);
		}
		
		return Parameter.NEWS_PAGE;
	}
	

}
