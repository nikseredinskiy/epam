<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:insertDefinition name="loginTemplate">
	<tiles:putAttribute name="body">
		<spring:url var="authUrl" value="/static/j_spring_security_check" />
		<form method="post" class="signin" action="${authUrl}">
				<table>
					<tr>
						<th><label for="username_or_email">Username or Email</label></th>
						<td><input id="username_or_email" name="j_username"
							type="text" />
					</tr>
					<tr>
						<th><label for="password">Password</label></th>
						<td><input id="password" name="j_password" type="password" />
					</tr>
					<tr>
						<th></th>
						<td><input name="commit" type="submit" value="Sign In" /></td>
					</tr>
				</table>
		</form>
	</tiles:putAttribute>
</tiles:insertDefinition>