package by.training.task.entity;

import java.util.Comparator;

/**
 * Created by Nik on 22.09.2015.
 */
public abstract class Song{
    private String name;
    private String genre;
    private int length;
    private int size;
    private String format;

    public Song(){

    }

    public Song(String name, String genre, int length, int size, String format) {
        this.name = name;
        this.genre = genre;
        this.length = length;
        this.size = size;
        this.format = format;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return  "\n" +
                '\'' + name + '\'' +
                ", '" + genre + '\'' +
                ", " + length +
                "min, " + size +
                "mb, '" + format + '\'';
    }

    public static class GenreComparator implements Comparator<Song> {
        @Override
        public int compare(Song s1, Song s2) {
            return s1.getGenre().compareTo(s2.getGenre());
        }
    }
}
