package by.training.task.action;

/**
 * Created by Nik on 20.10.2015.
 */
public enum HardwareEnum {
    HARDWARES("hardwares"),
    ID("id"),
    HARDWARE("hardware"),
    INPUTHARDWARE("inputhardware"),
    MULTIMEDHARDWARE("multimedhardware"),
    NAME("name"),
    ORIGIN("origin"),
    PRICE("price"),
    PERIPHERAL("peripheral"),
    ENERGY_CONSUMING("energy-consuming"),
    HAS_COOLER("has-cooler"),
    PORT_TYPE("port-type"),
    CRITICAL("critical"),
    TYPE("type"),
    SPEAKERS_COUNT("speakers-count"),
    HAS_SUB("has-sub"),
    HAS_DOLBY("has-dolby"),
    KEYS_COUNT("keys-count"),
    HAS_NUMPAD("has-numpad"),
    HAS_ADDITIONAL_KEYS("has-additional-keys");

    private String value;

    private HardwareEnum(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
