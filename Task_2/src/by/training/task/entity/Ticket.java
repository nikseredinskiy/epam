package by.training.task.entity;

/**
 * Created by Nik on 29.09.2015.
 */
public class Ticket {
    private int plainId;

    public Ticket(){

    }

    public Ticket(int plainId){
        this.plainId = plainId;
    }

    public int getPlainId(){
        return this.plainId;
    }

}
