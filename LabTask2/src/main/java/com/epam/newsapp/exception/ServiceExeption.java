package com.epam.newsapp.exception;

public class ServiceExeption extends Exception{

	public ServiceExeption() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceExeption(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ServiceExeption(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceExeption(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServiceExeption(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
