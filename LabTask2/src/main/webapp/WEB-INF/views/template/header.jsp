<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="header">NEWS PORTAL</div>
<div>
	<sec:authorize access="isAuthenticated()">
		<spring:url var="logoutUrl" value="/static/j_spring_security_logout" />
		<a href="${logoutUrl}">Logout</a>
	</sec:authorize>
</div>
<hr />

