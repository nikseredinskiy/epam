package by.training.task.main;

import by.training.task.action.AirportAction;
import by.training.task.creator.AirportCreate;
import by.training.task.entity.Airport;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Nik on 29.09.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Runner.class);
    public static void main(String[] args) {
        AirportCreate airportCreate = new AirportCreate();
        Airport airport = airportCreate.createAirport();

        AirportAction airportAction = new AirportAction(airport);
        try{
            airportAction.launchAirport();
        }catch (InterruptedException e){
            logger.error("Creating problems", e);
        }
    }
}
