<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="menu">
Menu
    <ul>
        <li>
            <spring:url value="/home" var="homeUrl" htmlEscape="true"/>
            <a class="menuLink" id="home" href="${homeUrl}">Home</a>
        </li>
        <li>
            <spring:url value="/viewNews/page/1" var="viewNewsUrl" htmlEscape="true"/>
            <a class="menuLink" id="viewNews" href="${viewNewsUrl}">View news</a>
        </li>
        <li>
            <spring:url value="/addNews" var="addNewsUrl" htmlEscape="true"/>
            <a class="menuLink" id="addNews" href="${addNewsUrl}">Add news</a>
        </li>
        <li>
            <spring:url value="/addAuthor" var="addAuthorUrl" htmlEscape="true"/>
            <a class="menuLink" id="addAuthor" href="${addAuthorUrl}">Add/Update Author</a>
        </li>
        <li>
            <spring:url value="/addTag" var="addTagUrl" htmlEscape="true"/>
            <a class="menuLink" id="addTag" href="${addTagUrl}">Add/Update Tag</a>
        </li>
        <li>
            <spring:url value="/about" var="aboutUrl" htmlEscape="true"/>
            <a class="menuLink" id="about" href="${aboutUrl}">About</a>
        </li>
    </ul>
</div>