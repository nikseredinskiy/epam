package com.epam.task.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.task.dao.NewsDAO;
import com.epam.task.entity.News;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.exception.DAOException;
import com.epam.task.util.ConnectionUtil;

@Repository
public class NewsDAOImpl implements NewsDAO {

	@Autowired
	private DataSource dataSource;

	private final String SQL_INSERT = "INSERT INTO NEWS VALUES(NEWS_ID_SEQ.NEXTVAL,?,?,?,CURRENT_TIMESTAMP,SYSDATE)";
	private final String SQL_UPDATE = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = SYSDATE WHERE NEWS_ID = ?";
	private final String SQL_DELETE = "DELETE NEWS WHERE NEWS_ID = ?";
	private final String SQL_GET_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
	private final String SQL_GET_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private final String SQL_GET_NEWS_BY_COMMENT_COUNT = "SELECT NEWS.*, COUNT(*) AS TOTAL_COUNT FROM COMMENTS INNER JOIN NEWS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY TOTAL_COUNT DESC";

	private final String SEARCH_CRITERIA = "SELECT NT.NEWS_ID FROM NEWS_TAG NT INNER JOIN NEWS_AUTHOR NA ON NA.NEWS_ID = NT.NEWS_ID ";

	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	public Long insert(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long id = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, new String[] { "NEWS_ID" });
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());

			ps.executeUpdate();

			rs = ps.getGeneratedKeys();
			if (rs.next() && rs != null) {
				id = Long.parseLong(rs.getString(1));
			}

		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}

		return id;
	}

	public void update(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setLong(4, news.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while update()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}

	public void delete(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, news.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}

	public List<News> getAllNews() throws DAOException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			News news = null;

			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();

			rs = st.executeQuery(SQL_GET_ALL_NEWS);
			while (rs.next()) {
				news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while getAllNews()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}
		return newsList;
	}

	public News getNews(Long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		News news = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_GET_NEWS);
			ps.setLong(1, newsId);

			rs = ps.executeQuery();
			if (rs.next()) {
				news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNews()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		return news;
	}

	public List<News> getNewsByCommCount() throws DAOException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			News news = null;

			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();

			rs = st.executeQuery(SQL_GET_NEWS_BY_COMMENT_COUNT);
			while (rs.next()) {
				news = new News(rs.getLong("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNewsByCommCount()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}

		return newsList;
	}

	public int countAllNews() throws DAOException {
		try {
			return getAllNews().size();
		} catch (DAOException e) {
			throw new DAOException("Error while countAllNews()", e);
		}

	}

	public List<News> getNewsBySeacrh(SearchCriteria searchCriteria)
			throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<News> newsList = new ArrayList<News>();
		String sql = SEARCH_CRITERIA;
		boolean isAuthor = false;
		boolean isTags = false;
		int position = 1;

		Long authorId = searchCriteria.getAuthorId();
		if (authorId != 0) {
			sql += "WHERE NA.AUTHOR_ID = ? ";
			isAuthor = true;
		}

		List<Long> tagId = searchCriteria.getTagId();
		if (tagId != null) {
			isTags = true;

			for (int i = 0; i < tagId.size(); i++) {
				sql += "INTERSECT " + SEARCH_CRITERIA + " WHERE NT.TAG_ID = ? ";
			}
		}

		try {
			News news = null;

			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(sql);
			if (isAuthor) {
				ps.setLong(position, authorId);
				position++;
			}

			if (isTags) {
				for (int i = 0; i < tagId.size(); i++) {
					ps.setLong(position, tagId.get(i));
					position++;
				}
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				news = getNews(rs.getLong("NEWS_ID"));

				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNewsBySeacrh()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}

		return newsList;
	}
}
