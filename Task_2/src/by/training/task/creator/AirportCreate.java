package by.training.task.creator;

import by.training.task.entity.*;

import java.util.ArrayList;

/**
 * Created by Nik on 29.09.2015.
 */
public class AirportCreate {

    private final int WAITING_HALL_CAPACITY = 100;
    private final int PASSANGERS_COUNT = 30 ;

    public Airport createAirport(){
        PlainCreator plainCreator = new PlainCreator();
        PassengerCreator passengerCreator = new PassengerCreator();
        TerminalCreator terminalCreator = new TerminalCreator();
        WaitingHallCreator waitingHallCreator = new WaitingHallCreator();

        ArrayList<Plain> plains = plainCreator.createPlain();
        ArrayList<Passenger> passengers = passengerCreator.createPassenger(PASSANGERS_COUNT);

        int currentPass = 0;
        for(Plain plain:plains){
            if(plain.getId() % 2 == 0){
                for(int i = currentPass; i < plain.getCapacity() + currentPass; i++){
                    Passenger passenger = passengers.get(i);
                    Ticket ticket = new Ticket(plain.getId() + 1);
                    passenger.setTickets(ticket);
                    plain.addPassenger(passenger);
                    passenger.setPlainId(plain.getId());
                }
                currentPass = currentPass + plain.getCapacity();
            }
        }

        ArrayList<Terminal> terminals = terminalCreator.createTerminal();
        WaitingHall waitingHall = waitingHallCreator.createWaitingHall(WAITING_HALL_CAPACITY);

        return Airport.getInstance(plains, terminals, waitingHall);

    }
}
