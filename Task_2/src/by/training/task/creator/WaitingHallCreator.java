package by.training.task.creator;

import by.training.task.entity.WaitingHall;

/**
 * Created by Nik on 29.09.2015.
 */
public class WaitingHallCreator {
    public WaitingHall createWaitingHall(int capacity){
        WaitingHall waitingHall = new WaitingHall(0, capacity);

        return waitingHall;
    }
}
