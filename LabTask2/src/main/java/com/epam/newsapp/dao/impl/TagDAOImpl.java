package com.epam.newsapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsapp.dao.TagDAO;
import com.epam.newsapp.entity.Tag;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.util.ConnectionUtil;

@Repository
public class TagDAOImpl implements TagDAO{
	
	@Autowired
	private DataSource dataSource;
	
	private final String CREATE = "INSERT INTO TAG VALUES(TAG_ID_SEQ.NEXTVAL,?)";
	private final String INSERT = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?,?)";
	private final String UPDATE = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	private final String DELETE_BY_NEWS_ID = "DELETE FROM NEWS_TAG WHERE NEWS_TAG.NEWS_ID = ?";
	private final String DELETE_BY_TAG_ID = "DELETE FROM NEWS_TAG WHERE NEWS_TAG.TAG_ID = ?";
	private final String DELETE_BY_ID = "DELETE FROM TAG WHERE TAG_ID = ?";
	private final String GET_BY_NEWS_ID = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM NEWS_TAG LEFT JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_ID = ?";
	private final String GET_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG";
	
	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}
	
	public long createTag(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = -1;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(CREATE, new String [] {"TAG_ID"});

			ps.setString(1, tag.getName());
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while create()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
		
	}
	
	public Long update(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while updateTag()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return tag.getId();	}
	
	public void insertNewsTag(long newsId, List<Tag> tags) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(INSERT);
						
			for (Iterator<Tag> iterator = tags.iterator(); iterator.hasNext();) {
				Tag tag = (Tag) iterator.next();
				ps.setLong(1, newsId);
				ps.setLong(2, tag.getId());
				ps.addBatch();
				
			}
			
			ps.executeBatch();
			
		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void updateNewsTag(long newsId, List<Tag> tags) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(UPDATE);
						
			for (Iterator<Tag> iterator = tags.iterator(); iterator.hasNext();) {
				Tag tag = (Tag) iterator.next();
				ps.setLong(1, tag.getId());
				ps.setLong(2, newsId);
				ps.addBatch();
			}
			
			ps.executeBatch();
			
		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsTags(long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE_BY_NEWS_ID);
						
			ps.setLong(1, newsId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteNewsTag()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNTByTagId(Long tagId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE_BY_TAG_ID);
						
			ps.setLong(1, tagId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteNTByTagId()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void delete(Long tagId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE_BY_ID);
						
			ps.setLong(1, tagId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public List<Tag> getNewsTag(long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Tag> tags = new ArrayList<Tag>();

		try {
			Tag tag = null;
			
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				tag = new Tag(rs.getLong("TAG_ID"), rs.getString("TAG_NAME"));
				
				tags.add(tag);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsTag()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return tags;
	}
	
	public List<Tag> getAllTags() throws DAOException{
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<Tag> tags = new ArrayList<Tag>();

		try {
			Tag tag = null;
			
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
				
			rs = st.executeQuery(GET_ALL_TAGS);
			while(rs.next()){
				tag = new Tag(rs.getLong("TAG_ID"), rs.getString("TAG_NAME"));
				
				tags.add(tag);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsTag()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}
		
		return tags;
	}

}
