package by.training.task.creator;

import by.training.task.entity.Passenger;
import by.training.task.entity.Plain;

import java.util.ArrayList;

/**
 * Created by Nik on 29.09.2015.
 */
public class PassengerCreator {
    public ArrayList<Passenger> createPassenger(int count){
        ArrayList<Passenger> passengers = new ArrayList<Passenger>();

        for(int i = 0; i < count; i++){
            passengers.add(new Passenger(i));
        }

        return passengers;
    }
}
