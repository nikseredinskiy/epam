package com.epam.task.dao;

import java.util.List;

import com.epam.task.entity.Tag;
import com.epam.task.exception.DAOException;

public interface TagDAO {
	/**
	 * @param tag
	 * @return generated tag id
	 * @throws DAOException
	 */
	Long create(Tag tag) throws DAOException;
	
	/**
	 * 
	 * @param tag
	 * @return tag id
	 * @throws DAOException
	 */
	Long update(Tag tag) throws DAOException;
	
	/**
	 * 
	 * @param tag
	 * @throws DAOException
	 */
	void delete(Tag tag) throws DAOException;

	/**
	 * Insert tag and news link in NEWS_TAG
	 * 
	 * @param newsId
	 * @param tags
	 * @throws DAOException
	 */
	void insertNewsTag(Long newsId, List<Tag> tags) throws DAOException;

	/**
	 * Delete link between tag and news from NEWS_TAG
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsTags(Long newsId) throws DAOException;

	/**
	 * @param newsId
	 * @return List of tags
	 * @throws DAOException
	 */
	List<Tag> getNewsTag(Long newsId) throws DAOException;

}
