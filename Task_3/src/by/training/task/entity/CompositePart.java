package by.training.task.entity;

import java.util.ArrayList;

/**
 * Created by Nik on 05.10.2015.
 */
public class CompositePart implements Component {

    private ArrayList<Component> partList = new  ArrayList<Component>();

    public CompositePart(){

    }

    @Override
    public void add(Component component) {
        partList.add(component);
    }

    @Override
    public void remove(Component component) {
        partList.remove(component);
    }

    @Override
    public Component getComponent(int index) {
        return partList.get(index);
    }

    public ArrayList<Component> getComponentList(){
        return this.partList;
    }

    @Override
    public LeafPart.LeafType getLeafType() {
        return null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "CompositePart{" +
                "partList=" + partList +
                '}';
    }
}
