package com.epam.task.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.epam.task.dao.TagDAO;
import com.epam.task.entity.Tag;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;
import com.epam.task.util.TestUtil;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {
	private static Logger logger = Logger.getLogger(TagServiceTest.class);
	
	private TagDAO tagDAO;
	@Autowired
	private TagService tagService;
	
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@Before
	public void setupMock(){
		tagDAO = mock(TagDAO.class);
		
		tagService.setTagDAO(tagDAO);
		
	}
	
	@Test
	public void createTest(){
		Tag tag = TestUtil.createTag(1L);
		
		try {
			when(tagDAO.create(tag)).thenReturn(tag.getId());
		} catch (DAOException e) {
			e.printStackTrace();
		}
		
		try {
			assertEquals(tagService.createTag(tag), tag.getId());
		} catch (ServiceExeption e) {
			logger.error("Error while createTest()", e);
		}
	}
	
	@Test
	public void getNewsTag(){
		
		List<Tag> tagList1 = new ArrayList<Tag>();
		List<Tag> tagList2 = new ArrayList<Tag>();
		tagList1.add(TestUtil.createTag(1L));
		tagList1.add(TestUtil.createTag(2L));
		
		tagList2.add(TestUtil.createTag(3L));
		
		try {
			when(tagDAO.getNewsTag(1L)).thenReturn(tagList1);
			when(tagDAO.getNewsTag(2L)).thenReturn(tagList2);
		} catch (DAOException e) {
			logger.error("Error while getNewsTest()", e);
		}

		try {
			assertEquals(tagService.getNewsTag(1L), tagList1);
			assertEquals(tagService.getNewsTag(2L), tagList2);
		} catch (ServiceExeption e) {
			
		}
	}
}
