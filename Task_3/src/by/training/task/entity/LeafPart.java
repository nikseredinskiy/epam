package by.training.task.entity;

import java.util.ArrayList;

/**
 * Created by Nik on 05.10.2015.
 */
public class LeafPart implements Component {

    public enum LeafType {SIGN, WORD, LISTING}
    private String text;
    private LeafType leafType;

    public LeafPart(String text, LeafType leafType){
        this.text = text;
        this.leafType = leafType;
    }

    @Override
    public void add(Component component) {
        System.out.println("Can't add in here");
    }

    @Override
    public void remove(Component component) {
        System.out.println("Nothing to remove");
    }

    @Override
    public Component getComponent(int index) {
        return this;
    }

    @Override
    public ArrayList<Component> getComponentList(){
        return null;
    }

    public LeafType getLeafType(){
        return this.leafType;
    }

    @Override
    public String toString() {
        return text;
    }
}
