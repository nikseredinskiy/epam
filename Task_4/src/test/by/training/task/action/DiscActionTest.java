package test.by.training.task.action;

import by.training.task.action.DiscAction;
import by.training.task.creator.DiscCreator;
import by.training.task.entity.Disc;
import by.training.task.entity.PopSong;
import by.training.task.entity.Song;
import by.training.task.exception.DiscException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Nik on 25.09.2015.
 */
public class DiscActionTest {

    private final int BEGIN_MOMENT = 4;
    private final int END_MOMENT = 6;

    @Test
    public void findInIntervalTest() throws DiscException{
        Disc disc = new Disc();
        DiscAction.recordDisk(disc, createTestSongs());

        int startPosition = 0;

        Song actual = DiscAction.findInInterval(disc, BEGIN_MOMENT, END_MOMENT);
        Song expected = null;
        for(Song song:disc.getSongs()){
            if((startPosition >= BEGIN_MOMENT) && (startPosition + song.getLength() <= END_MOMENT)){
                expected = song;
            } else {
                startPosition = startPosition + song.getLength();
            }
        }
        Assert.assertSame("Test failed. Not same songs.", expected, actual);
    }

    @Test
    public void calculateWeightTest(ArrayList<Song> songs){
        int expected = DiscAction.calculateWeight(songs);
        int actual = 0;

        for(Song song:songs){
            actual = actual + song.getSize();
        }

        Assert.assertEquals("Weight calculation test failed ",expected, actual, 0.1);
    }

    @Test
    public void sortByGenreTest() throws DiscException{
        Disc disc = new Disc();
        DiscAction.recordDisk(disc, createTestSongs());
        DiscAction.sortByGenre(disc);

        ArrayList<Song> sortedSongs = disc.getSongs();

        for(int i = 0; i < sortedSongs.size() - 2; i++){
            Assert.assertTrue(sortedSongs.get(i).getGenre().compareTo(sortedSongs.get(i + 1).getGenre()) <= 0);
        }
    }

    private ArrayList<Song> createTestSongs() throws DiscException{
        ArrayList<Song> songs = new ArrayList<Song>();

        songs.add(new PopSong("Let It Be", "Paul McCartney", "The Beatles", "Pop", 4, 8, "mp3"));
        songs.add(new PopSong("Yesterday", "Paul McCartney", "The Beatles", "Baroque Pop", 2, 3, "mp3"));
        songs.add(new PopSong("All You Need Is Love", "John Lennon", "The Beatles", "Pop", 4, 7, "mp3"));

        return songs;
    }
}
