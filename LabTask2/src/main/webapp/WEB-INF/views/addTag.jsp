<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div style="margin: 10px;">
				<table>
					<c:forEach items="${tags}" var="tag">
						<tr>
							<sf:form method="post" modelAttribute="tag"
								action="/NewsApp/updateTag">
								<sf:input path="id" value="${tag.id}" style="display: none;" />
								<td><c:out value="Tag: " /></td>
								<td><sf:input path="name" value="${tag.name}"
										disabled="true" /></td>
								<td><button type="button" class="actions-tag-list-show">Edit</button></td>
								<td><button onclick="this.form.submit();"
										class="actions-tag-list-edit" style="display: none;">Update</button></td>
							</sf:form>
							<sf:form method="post" modelAttribute="tag"
								action="/NewsApp/deleteTag">
								<sf:input path="id" value="${tag.id}" style="display: none;" />
								<td><button onclick="this.form.submit();"
										class="actions-tag-list-edit" style="display: none;">Delete</button></td>
							</sf:form>
							<td><button type="button"
									class="actions-tag-list-edit cancel" style="display: none;">Cancel</button></td>
						</tr>
					</c:forEach>
					<sf:form method="post" modelAttribute="tag"
						action="/NewsApp/saveTag">
						<tr>
							<td><c:out value="Tag: " /></td>
							<td><sf:input path="name" /></td>
							<td><input type="submit" value="Save" /></td>
						</tr>
					</sf:form>
				</table>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>