package by.training.task.entity;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Nik on 29.09.2015.
 */
public class Airport {
    private ArrayList<Plain> plains;
    private ArrayList<Terminal> terminals;
    private WaitingHall waitingHall;
    private static Lock lock = new ReentrantLock();

    private static volatile Airport instance;

    public static Airport getInstance(ArrayList<Plain> plains, ArrayList<Terminal> terminals, WaitingHall waitingHall){
        if(instance == null){
            lock.lock();
            if(instance == null){
                instance =  new Airport(plains, terminals, waitingHall);
            }
            lock.unlock();
        }

        return instance;
    }

    public ArrayList<Plain> getPlains() {
        return plains;
    }

    public ArrayList<Terminal> getTerminals() {
        return terminals;
    }

    public WaitingHall getWaitingHall() {
        return waitingHall;
    }

    private Airport(ArrayList<Plain> plains, ArrayList<Terminal> terminals, WaitingHall waitingHall) {
        this.plains = plains;
        this.terminals = terminals;
        this.waitingHall = waitingHall;
    }
}
