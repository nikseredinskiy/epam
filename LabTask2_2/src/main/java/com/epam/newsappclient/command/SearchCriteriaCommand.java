package com.epam.newsappclient.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsappclient.entity.NewsVO;
import com.epam.newsappclient.entity.SearchCriteria;
import com.epam.newsappclient.exception.ServiceExeption;
import com.epam.newsappclient.service.AuthorService;
import com.epam.newsappclient.service.NewsService;
import com.epam.newsappclient.service.NewsVOService;
import com.epam.newsappclient.service.TagService;
import com.epam.newsappclient.util.Parameter;

public class SearchCriteriaCommand implements ActionCommand {

	@Autowired
	NewsVOService newsVOService;

	@Autowired
	NewsService newsService;

	@Autowired
	AuthorService authorService;

	@Autowired
	TagService tagService;

	private Logger logger = Logger.getLogger(SearchCriteriaCommand.class);

	public String execute(HttpServletRequest request) {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsVO> newsList = new ArrayList<NewsVO>();
		double newsCount;

		if (!request.getParameter(Parameter.AUTHOR_ID).isEmpty()) {
			Long authorId = Long.parseLong(request.getParameter(Parameter.AUTHOR_ID));
			searchCriteria.setAuthorId(authorId);
			request.setAttribute(Parameter.AUTHOR_ID, authorId);
		}

		if (request.getParameterValues(Parameter.TAG_ID) != null) {
			String[] tagIds = (String[]) request.getParameterValues(Parameter.TAG_ID);
			List<Long> tagIdList = new ArrayList<Long>();
			for (String temp : tagIds) {
				tagIdList.add(Long.parseLong(temp));
			}
			searchCriteria.setTagId(tagIdList);
			request.setAttribute(Parameter.TAG_ID, tagIdList);
		}
		
		Long page = Long.parseLong(request.getParameter(Parameter.PAGE));
		try {

			newsList = newsVOService.getNewsBySeacrh(searchCriteria, page,
					Parameter.NEWS_ON_PAGE);
			newsCount = (double) newsService.countNewsBySeacrh(searchCriteria);

			request.setAttribute(Parameter.NEWS_LIST, newsList);
			request.setAttribute(Parameter.AUTHORS, authorService.getAllAuthors());
			request.setAttribute(Parameter.TAGS, tagService.getAllTags());
			request.setAttribute(Parameter.OPERATION, Parameter.SEARCH_RESULT);
			request.setAttribute(Parameter.PAGE_COUNT,
					Math.ceil(newsCount / Parameter.NEWS_ON_PAGE));
			request.setAttribute(Parameter.CURRENT_PAGE, page);
			
		} catch (ServiceExeption e) {
			logger.error("Service Exception in SearchCriteriaCommand " + e, e);
		}

		return Parameter.NEWS_LIST_PAGE;
	}

}
