package com.epam.task.dao;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.config.DBUnitConfig;
import com.epam.task.entity.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
public class CommentDAOTest extends DBUnitConfig{
	
	@Autowired
	private CommentDAO commentDAO;
	
	public CommentDAOTest(){
		super();
	}
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/comment/comment-data.xml"));

		tester.setDataSet(beforeData);
		tester.onSetup();
	}
	
	@Test
	public void testAddComment() throws Exception{
		commentDAO.add(new Comment(2L, "Comment text", new Date()));
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/comment/comment-data-expected.xml"));
		
		ReplacementDataSet rDataSet = new ReplacementDataSet(expectedData);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		rDataSet.addReplacementObject("[create_date]", dateFormat.format(new Date()));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "COMMENT_ID", "CREATION_DATE" };
		Assertion.assertEqualsIgnoreCols(rDataSet, actualData,"COMMENTS", ignore);
	}
	
	@Test 
	public void testDeleteComment() throws Exception{
		commentDAO.delete(new Comment(1L, 2L, "TASK COMMENT", new Date()));
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/comment/comment-del-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {  };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"COMMENTS", ignore);
	}
	
	@Test
	public void testDeleteNewsComment() throws Exception{
		commentDAO.deleteNewsComments(2L);
		
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(new File(
				"src/test/resources/dataset/comment/comment-delnc-data-expected.xml"));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = {  };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData,"COMMENTS", ignore);
	}

}
