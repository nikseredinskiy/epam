<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div style="margin: 10px;">
				<sf:form method="post" modelAttribute="newsVO"
					action="/NewsApp/saveNews">
					<table>
						<tr>
							<td><c:out value="Title: " /></td>
							<td><sf:input path="news.title" /> <sf:errors
									path="news.title" cssClass="error" /></td>
						</tr>
						<tr>
							<td><c:out value="Short text: " /></td>
							<td><sf:textarea path="news.shortText" cols="40" rows="5"></sf:textarea>
								<sf:errors path="news.shortText" cssClass="error" /></td>
						</tr>
						<tr>
							<td><c:out value="Full text: " /></td>
							<td><sf:textarea path="news.fullText"
									placeholder="Text full text here..." cols="40" rows="5"></sf:textarea>
								<sf:errors path="news.fullText" cssClass="error" /></td>
						</tr>
						<tr>
							<td><c:out value="Author: " /></td>
							<td><sf:select path="author.id">
									<c:forEach items="${authors}" var="author">
										<sf:option value="${author.getId() }">
											<c:out value="${author.getName()}" />
										</sf:option>
									</c:forEach>
								</sf:select></td>
						</tr>
						<tr>
							<td><c:out value="Tags: " /></td>
							<td><sf:checkboxes items="${tags}" path="tags"
									itemValue="id" itemLabel="name" /></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" value="Add news" /></td>
						</tr>
					</table>
				</sf:form>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
