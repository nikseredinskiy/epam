package com.epam.task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import com.epam.task.dao.NewsDAO;
import com.epam.task.entity.News;
import com.epam.task.entity.SearchCriteria;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;

@Service
public class NewsService {
	
	@Autowired
	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public Long insertNews(News news) throws ServiceExeption {
		Long id = null;
		
		try {
			id = this.newsDAO.insert(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
		
		return id;
	}

	public void updateNews(News news) throws ServiceExeption {
		try {
			this.newsDAO.update(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while update()", e);
		}
	}

	
	public void deleteNews(News news) throws ServiceExeption {
		try {
			this.newsDAO.delete(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}

	public List<News> getAllNews() throws ServiceExeption {
		try {
			return this.newsDAO.getAllNews();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllNews()", e);
		}
	}

	public News getNews(Long newsId) throws ServiceExeption {
		try {
			return this.newsDAO.getNews(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNews()", e);
		}
	}

	public List<News> getNewsByCommCount() throws ServiceExeption {
		try {
			return this.newsDAO.getNewsByCommCount();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsByCommCount()", e);
		}
	}

	public int countAllNews() throws ServiceExeption {
		try {
			return this.newsDAO.countAllNews();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while countAllNews()", e);
		}
	}
	
	public List<News> getNewsBySeacrh(SearchCriteria searchCriteria) throws ServiceExeption{
		try {
			return this.newsDAO.getNewsBySeacrh(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsBySearch()", e);
		}
	}

}
