package by.training.task.entity;

/**
 * Created by Nik on 23.09.2015.
 */
public class PopSong extends Song {
    private String author;
    private String singer;

    public PopSong(){

    }

    public PopSong(String name, String author, String singer, String genre, int length, int size, String format) {
        super(name, genre, length, size, format);
        this.author = author;
        this.singer = singer;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", " + '\'' + author + '\'' +
                ", " + '\'' + singer + '\'';
    }
}
