package by.training.task.creator;

import by.training.task.action.DiscActionJavaE;
import by.training.task.entity.Disc;
import by.training.task.exception.DiscException;

import java.util.function.Supplier;

/**
 * Created by Nik on 12.10.2015.
 */
public class DiscCreatorJavaE {

    public static Disc createMusicDisk() throws DiscException{
        Supplier<Disc> discSupplier = Disc::new;
        Disc disc = discSupplier.get();

        DiscActionJavaE.recordDisk(disc, SongCreator.createSongs());

        return disc;
    }
}
