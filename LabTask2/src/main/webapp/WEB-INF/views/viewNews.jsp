<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div style="margin: 10px;">

				<sf:form method="GET" modelAttribute="searchCriteria"
					action="/NewsApp/searchResult/page/1">
					<sf:select path="authorId">
						<sf:option value="">Select author</sf:option>
						<c:forEach items="${authors}" var="author">
							<sf:option value="${author.getId() }">
								<c:out value="${author.getName()}" />
							</sf:option>
						</c:forEach>
					</sf:select>
					<sf:select path="tagId" multiple="multiple" size="1">
						<sf:option value="">Select tags</sf:option>
						<c:forEach items="${tags}" var="tag">
							<sf:option value="${tag.getId() }">
								<c:out value="${tag.getName()}" />
							</sf:option>
						</c:forEach>
					</sf:select>
					<input type="submit" value="Filter" />
				</sf:form>

				<c:forEach items="${news}" var="newsVO">
					<p>
						<b> <c:out value="${newsVO.getNews().getTitle()}" />
						</b> (by
						<c:out value="${newsVO.getAuthor().getName()}" />
						) <i> <c:out value="Creation date: " /> <c:out
								value="${newsVO.getNews().getCreationDate()}" />
						</i>
					</p>
					<p>
						<c:out value="${newsVO.getNews().getShortText()}" />
					</p>
					<p>
						<c:forEach items="${newsVO.getTags()}" var="tag">
							<i> <c:out value="${tag.getName()}" /> <c:out value="; " />
							</i>
						</c:forEach>
						<c:out value="Comments(" />
						<c:out value="${newsVO.getComments().size()}" />
						<c:out value=")" />
						
						<spring:url value="/viewNews/${newsVO.getNews().getId()}"
							var="viewFullNewsUrl" htmlEscape="true" />
						<a href="${viewFullNewsUrl}">View full text</a>
						
						<spring:url value="/editNews/${newsVO.getNews().getId()}"
							var="editNewsUrl" htmlEscape="true" />
						<a href="${editNewsUrl}">Edit</a>
						
						<spring:url value="/deleteNews/${newsVO.getNews().getId()}"
							var="deleteNewsUrl" htmlEscape="true" />
						<a href="${deleteNewsUrl}">Delete</a>
					</p>
					<p>----</p>
				</c:forEach>

				<c:if test="${operation == 'viewNews'}">
					<c:forEach var="page" begin="1" end="${pageCount}">
						<spring:url value="/viewNews/page/${page}" var="viewNextPage"
							htmlEscape="true" />
						<a href="${viewNextPage}"><c:out value="${page}" /></a>
					</c:forEach>
				</c:if>

				<c:if test="${operation == 'searchResult'}">
					<p>
						<c:forEach var="page" begin="1" end="${pageCount}">
							<sf:form id="form${page}" method="GET"
								modelAttribute="searchCriteria"
								action="/NewsApp/searchResult/page/${page}" style='float:left; margin-right: 4px;'>
								<sf:hidden path="authorId" />
								<sf:hidden path="tagId" />
								<a href="#"
									onclick="document.getElementById('form${page}').submit(); return false;">
									<c:out value="${page}" />
								</a>
							</sf:form>

						</c:forEach>
					</p>
				</c:if>

			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
