package by.training.task.creator;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Nik on 05.10.2015.
 */
public class TextCreator {


    public static String createText(String path) {
        String text = "";
        try {
            FileInputStream inFile = new FileInputStream(path);
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            text = new String(str);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Can't find file!", e);
        } catch (IOException e) {
            throw new RuntimeException("Can't read text!", e);
        }
        return text;
    }
}
