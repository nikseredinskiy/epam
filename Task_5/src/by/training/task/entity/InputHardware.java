package by.training.task.entity;

/**
 * Created by Nik on 17.10.2015.
 */
public class InputHardware extends Hardware {
    private int keyCount;
    private boolean hasNumPad;
    private boolean hasAdditionalKey;

    public InputHardware(){

    }

    public InputHardware(int keyCount, boolean hasNumPad, boolean hasAdditionalKey) {
        this.keyCount = keyCount;
        this.hasNumPad = hasNumPad;
        this.hasAdditionalKey = hasAdditionalKey;
    }

    public InputHardware(String name, String origin, int price, boolean isCritical, Type type, int keyCount, boolean hasNumPad, boolean hasAdditionalKey) {
        super(name, origin, price, isCritical, type);
        this.keyCount = keyCount;
        this.hasNumPad = hasNumPad;
        this.hasAdditionalKey = hasAdditionalKey;
    }

    public int getKeyCount() {
        return keyCount;
    }

    public void setKeyCount(int keyCount) {
        this.keyCount = keyCount;
    }

    public boolean isHasNumPad() {
        return hasNumPad;
    }

    public void setHasNumPad(boolean hasNumPad) {
        this.hasNumPad = hasNumPad;
    }

    public boolean isHasAdditionalKey() {
        return hasAdditionalKey;
    }

    public void setHasAdditionalKey(boolean hasAdditionalKey) {
        this.hasAdditionalKey = hasAdditionalKey;
    }

    @Override
    public String toString() {
        return super.toString() + " InputHardware{" +
                "keyCount=" + keyCount +
                ", hasNumPad=" + hasNumPad +
                ", hasAdditionalKey=" + hasAdditionalKey +
                '}';
    }
}
