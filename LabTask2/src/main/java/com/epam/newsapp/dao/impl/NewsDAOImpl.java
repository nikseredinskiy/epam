package com.epam.newsapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsapp.dao.NewsDAO;
import com.epam.newsapp.entity.News;
import com.epam.newsapp.entity.SearchCriteria;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.util.ConnectionUtil;

@Repository
public class NewsDAOImpl implements NewsDAO {

	@Autowired
	private DataSource dataSource;

	private final String INSERT = "INSERT INTO NEWS VALUES(NEWS_ID_SEQ.NEXTVAL,?,?,?,CURRENT_TIMESTAMP,SYSDATE)";
	private final String UPDATE = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = SYSDATE WHERE NEWS_ID = ?";
	private final String DELETE = "DELETE NEWS WHERE NEWS_ID = ?";
	private final String GET_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
	private final String GET_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private final String GET_NEWS_BY_COMMENT_COUNT = "SELECT ROWNUM R, NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, TOTAL_COUNT FROM (SELECT NEWS.*, COUNT(*) AS TOTAL_COUNT FROM COMMENTS FULL JOIN NEWS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY NEWS.MODIFICATION_DATE DESC, TOTAL_COUNT DESC)";
	private final String GET_NEWS_ON_PAGE = "SELECT R, NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, TOTAL_COUNT FROM (SELECT ROWNUM R, NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, TOTAL_COUNT FROM (SELECT NEWS.*, COUNT(*) AS TOTAL_COUNT FROM COMMENTS FULL JOIN NEWS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY NEWS.MODIFICATION_DATE DESC, TOTAL_COUNT DESC)) WHERE R >= ? AND R <= ?";
	private final String SEARCH_CRITERIA = "SELECT DISTINCT NT.NEWS_ID FROM NEWS_TAG NT INNER JOIN NEWS_AUTHOR NA ON NA.NEWS_ID = NT.NEWS_ID ";
	private final String COUNT_NEWS = "SELECT COUNT(NEWS_ID) FROM NEWS";
	
	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	public long insertNews(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = -1;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(INSERT, new String[] { "NEWS_ID" });
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());

			ps.executeUpdate();
			rs = ps.getGeneratedKeys();

			if (rs.next() && rs != null) {
				id = Long.parseLong(rs.getString(1));
			}

		} catch (SQLException e) {
			throw new DAOException("Error while insert()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}

		return id;
	}

	public long updateNews(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		long newsId = -1;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setLong(4, news.getId());

			ps.executeUpdate();

			newsId = news.getId();

		} catch (SQLException e) {
			throw new DAOException("Error while update()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}

		return newsId;
	}

	public void deleteNews(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, newsId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}

	public List<News> getAllNews() throws DAOException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			News news = null;
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			rs = st.executeQuery(GET_ALL_NEWS);
			while (rs.next()) {
				news = new News(rs.getInt("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while getAllNews()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}
		return newsList;
	}

	public News getNews(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		News news = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(GET_NEWS);
			ps.setLong(1, newsId);

			rs = ps.executeQuery();
			if (rs.next()) {
				news = new News(rs.getInt("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNews()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		return news;
	}

	public List<News> getNewsByCommCount() throws DAOException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			News news = null;

			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();

			rs = st.executeQuery(GET_NEWS_BY_COMMENT_COUNT);
			while (rs.next()) {
				news = new News(rs.getInt("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));

				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNewsByCommCount()", e);
		} finally {
			ConnectionUtil.close(rs, st, conn, dataSource);
		}

		return newsList;
	}

	public int countAllNews() throws DAOException {
		try {
			Connection conn = null;
			Statement st = null;
			ResultSet rs = null;
			int count = 0;

			try {
				conn = DataSourceUtils.getConnection(dataSource);
				st = conn.createStatement();

				rs = st.executeQuery(COUNT_NEWS);
				while (rs.next()) {
					count = rs.getInt(1);
				}

			} catch (SQLException e) {
				throw new DAOException("Error while getNewsByCommCount()", e);
			} finally {
				ConnectionUtil.close(rs, st, conn, dataSource);
			}

			return count;
		} catch (DAOException e) {
			throw new DAOException("Error while countAllNews()", e);
		}

	}

	public List<News> getNewsBySeacrh(SearchCriteria searchCriteria,
			long pageNum, long newsOnPage) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<News> newsList = new ArrayList<News>();
		String sql = "SELECT NEWS_ID FROM(SELECT ROWNUM R, NEWS_ID FROM ( "
				+ SEARCH_CRITERIA;
		boolean isAuthor = false;
		boolean isTags = false;
		int position = 1;

		Long authorId = searchCriteria.getAuthorId();
		if (authorId != null) {
			sql += "WHERE NA.AUTHOR_ID = ? ";
			isAuthor = true;
		}

		List<Long> tagId = searchCriteria.getTagId();
		if (tagId != null) {

			isTags = true;

			for (int i = 0; i < tagId.size(); i++) {
				if (tagId.get(i) != null) {
					sql += "INTERSECT " + SEARCH_CRITERIA
							+ " WHERE NT.TAG_ID = ? ";
				}
			}
		}

		sql += ")) WHERE R >= ? AND R <= ?";
		
		try {
			News news = null;

			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(sql);
			if (isAuthor) {
				ps.setLong(position, authorId);
				position++;
			}

			if (isTags) {
				for (int i = 0; i < tagId.size(); i++) {
					ps.setLong(position, tagId.get(i));
					position++;
				}
			}

			ps.setLong(position, 1 + newsOnPage * (pageNum - 1));
			position++;
			ps.setLong(position, newsOnPage * pageNum);

			rs = ps.executeQuery();

			while (rs.next()) {
				news = getNews(rs.getInt("NEWS_ID"));

				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while getNewsBySeacrh()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}

		return newsList;
	}

	public int countNewsBySeacrh(SearchCriteria searchCriteria)
			throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int position = 1;
		int count = 0;

		String sql = "SELECT COUNT(NEWS_ID) FROM ( " + SEARCH_CRITERIA;
		boolean isAuthor = false;
		boolean isTags = false;

		Long authorId = searchCriteria.getAuthorId();
		if (authorId != null) {
			sql += "WHERE NA.AUTHOR_ID = ? ";
			isAuthor = true;
		}

		List<Long> tagId = searchCriteria.getTagId();
		if (tagId != null) {

			isTags = true;

			for (int i = 0; i < tagId.size(); i++) {
				if (tagId.get(i) != null) {
					sql += "INTERSECT " + SEARCH_CRITERIA
							+ " WHERE NT.TAG_ID = ? ";
				}
			}
		}

		sql += ")";
		
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(sql);
			if (isAuthor) {
				ps.setLong(position, authorId);
				position++;
			}

			if (isTags) {
				for (int i = 0; i < tagId.size(); i++) {
					ps.setLong(position, tagId.get(i));
					position++;
				}
			}

			rs = ps.executeQuery();
			
			while (rs.next()) {
				count = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new DAOException("Error while countNewsBySeacrh()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}

		return count;
	}

	public List<News> getNewsOnPage(long pageNum, long newsOnPage)
			throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			News news = null;
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(GET_NEWS_ON_PAGE);

			ps.setLong(1, 1 + newsOnPage * (pageNum - 1));
			ps.setLong(2, newsOnPage * pageNum);

			rs = ps.executeQuery();

			while (rs.next()) {
				news = new News(rs.getInt("NEWS_ID"), rs.getString("TITLE"),
						rs.getString("SHORT_TEXT"), rs.getString("FULL_TEXT"),
						rs.getDate("CREATION_DATE"),
						rs.getDate("MODIFICATION_DATE"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsOnPage()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		return newsList;
	}

}
