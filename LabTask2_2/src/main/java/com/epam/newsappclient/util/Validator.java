package com.epam.newsappclient.util;

public class Validator {
	private Validator() {
	}

	public static boolean isValid(String text) {
		if ((text == null) || (text.isEmpty())) {
			return false;
		}
		if (text.length() > 100) {
			return false;
		}
		
		return true;
	}
}
