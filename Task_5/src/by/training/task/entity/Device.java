package by.training.task.entity;

import java.util.ArrayList;

/**
 * Created by Nik on 14.10.2015.
 */
public class Device {
    private ArrayList<Hardware> hardwares = new ArrayList<Hardware>();

    public Device(){

    }

    public Device(ArrayList<Hardware> hardwares){
        this.hardwares = hardwares;
    }

    public ArrayList<Hardware> getHardwares() {
        return hardwares;
    }

    public void setHardwares(ArrayList<Hardware> hardwares) {
        this.hardwares = hardwares;
    }
}
