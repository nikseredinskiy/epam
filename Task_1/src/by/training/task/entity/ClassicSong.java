package by.training.task.entity;

/**
 * Created by Nik on 23.09.2015.
 */
public class ClassicSong extends Song {
    private String composer;
    private int year;
    private String type;
    private String tonality;

    public ClassicSong(){

    }

    public ClassicSong(String name, String genre, int length, int size, String format, String composer,
                        int year, String type, String tonality) {
        super(name, genre, length, size, format);
        this.composer = composer;
        this.year = year;
        this.type = type;
        this.tonality = tonality;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTonality() {
        return tonality;
    }

    public void setTonality(String tonality) {
        this.tonality = tonality;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", " + '\'' + composer + '\'' +
                ", " + year +
                ", " + '\'' + type + '\'' +
                ", " + '\'' + tonality + '\''
                ;
    }
}
