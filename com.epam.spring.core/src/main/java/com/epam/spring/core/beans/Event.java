package com.epam.spring.core.beans;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.io.EndianUtils;

public class Event {
	private int id;
	private String msg;
	private Date date;
	private DateFormat df;
	final static int END_OF_DAY = 17;
	
	public Event(Date date, DateFormat df) {
		super();
		this.id = new Random().nextInt(10);
		this.date = date;
		this.df = df;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public static boolean isDay(){
		int time = new Random().nextInt(12) + 8;
		return time < END_OF_DAY ?  true : false;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", msg=" + msg + ", date=" + df.format(date) + "]";
	}
	
	

}
