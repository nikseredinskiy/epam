<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div style="margin: 10px;">
				<p>
					<b> <c:out value="${newsVO.getNews().getTitle()}" />
					</b> (by
					<c:out value="${newsVO.getAuthor().getName()}" />
					) <i> <c:out value="Creation date: " /> <c:out
							value="${newsVO.getNews().getCreationDate()}" />
					</i>
				</p>
				<p>
					<c:out value="${newsVO.getNews().getFullText()}" />
				</p>
				<p>
					<c:forEach items="${newsVO.getTags()}" var="tag">
						<i> <c:out value="${tag.getName()}" /> <c:out value="; " />
						</i>
					</c:forEach>
					<c:out value="Comments(" />
					<c:out value="${newsVO.getComments().size()}" />
					<c:out value=")" />
					<spring:url value="/editNews/${newsVO.getNews().getId()}"
						var="editNewsUrl" htmlEscape="true" />
					<a href="${editNewsUrl}">Edit</a>
				</p>
				<p>----</p>
				<table>
					<c:forEach items="${newsVO.getComments()}" var="comment">
						<tr>
							<td><c:out value="${comment.getCreationDate()} " /></td>
							<td></td>
						</tr>
						<tr>
							<td><c:out value="${comment.getCommentText()}" /></td>
							<td><spring:url
									value="/viewNews/${newsVO.getNews().getId()}/deleteComment/${comment.getId()}"
									var="deleteComment" htmlEscape="true" /> <sf:form
									action="${deleteComment}" method="post">
									<input type="submit" value="x">
								</sf:form></td>
						</tr>
					</c:forEach>
				</table>
				<p>
					<sf:form method="post" modelAttribute="comment"
						action="/NewsApp/saveComment">
						<table>
							<tr>
								<td><c:out value="Comment: " /> <sf:hidden path="newsID" /></td>
							</tr>
							<tr>
								<td><sf:textarea path="commentText" cols="40" rows="5" /></td>
							</tr>
							<tr>
								<td><input type="submit" value="Save" /></td>

							</tr>
						</table>
					</sf:form>
				</p>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
