package by.training.task.creator;

import by.training.task.action.DiscActionFun;
import by.training.task.entity.Disc;
import by.training.task.exception.DiscException;

import java.util.function.Supplier;

/**
 * Created by Nik on 12.10.2015.
 */
public class DiscCreatorFun{

    public static Disc createMusicDisk() throws DiscException{
        Supplier<Disc> discSupplier = Disc::new;
        Disc disc = discSupplier.get();

        DiscActionFun.recordDisk(disc, SongCreator.createSongs());

        return disc;
    }
}
