package by.training.task.entity;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Nik on 29.09.2015.
 */
public class WaitingHall {

    private int id;
    private int capacity;
    private ArrayList<Passenger> passengers = new ArrayList<Passenger>();
    private Lock lock = new ReentrantLock();

    public WaitingHall(){

    }

    public WaitingHall(int id, int capacity) {
        this.id = id;
        this.capacity = capacity;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public void addPassenger(Passenger passenger){
        lock.lock();
        this.passengers.add(passenger);
        lock.unlock();
    }

    public void removePassenger(Passenger passenger){
        lock.lock();
        this.passengers.remove(passenger);
        lock.unlock();
    }

    @Override
    public String toString() {
        return "WaitingHall{" +
                "id=" + id +
                ", passengers=" + passengers +
                ", capacity=" + capacity +
                '}';
    }
}
