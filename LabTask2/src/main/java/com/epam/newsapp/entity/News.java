package com.epam.newsapp.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class News {
	private long id;

	@Size(min = 1, max = 30, message = "The title must be beetween 1 and 30 characters long.")
	private String title;

	@Size(min = 1, max = 100, message = "The shortText must be beetween 1 and 30 characters long.")
	private String shortText;

	@Size(min = 1, max = 2000, message = "The fullText must be beetween 1 and 30 characters long.")
	private String fullText;

	@NotNull
	private Date creationDate;

	@NotNull
	private Date modifitationDate;

	public News() {

	}

	public News(long id, String title, String shortText, String fullText,
			Date creationDate, Date modifitationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modifitationDate = modifitationDate;
	}

	public News(String title, String shortText, String fullText,
			Date creationDate, Date modifitationDate) {
		super();
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modifitationDate = modifitationDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifitationDate() {
		return modifitationDate;
	}

	public void setModifitationDate(Date modifitationDate) {
		this.modifitationDate = modifitationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((modifitationDate == null) ? 0 : modifitationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id != other.id)
			return false;
		if (modifitationDate == null) {
			if (other.modifitationDate != null)
				return false;
		} else if (!modifitationDate.equals(other.modifitationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modifitationDate=" + modifitationDate + "]";
	}

}
