package com.epam.newsapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsapp.dao.CommentDAO;
import com.epam.newsapp.entity.Comment;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.util.ConnectionUtil;

@Repository
public class CommentDAOImpl implements CommentDAO{
	
	@Autowired
	private DataSource dataSource;
	
	private final String INSERT = "INSERT INTO COMMENTS VALUES(COMMENT_ID_SEQ.NEXTVAL,?,?,SYSDATE)";
	private final String DELETE_BY_ID = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final String DELETE = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private final String GET_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";
	
	public void setDataSource(DataSource dateSource) {
		this.dataSource = dateSource;
	}

	public long addComment(Comment comment) throws DAOException{
		long id = -1;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(INSERT,new String [] {"COMMENT_ID"});
			ps.setLong(1, comment.getNewsID());
			ps.setString(2, comment.getCommentText());
			
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs.next() && rs != null){
				id = Long.parseLong(rs.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while add()", e);
		} finally {
			ConnectionUtil.close(rs, ps, conn, dataSource);
		}
		
		return id;
	}

	public void deleteComment(long commentId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE_BY_ID);
			ps.setLong(1, commentId);
						
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while delete()", e);

		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public void deleteNewsComments(long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, newsId);
						
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Error while deleteNewsComment()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
	}
	
	public List<Comment> getNewsComments(long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> comments = new ArrayList<Comment>();

		try {
			Comment comment = null;
			
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(GET_BY_NEWS_ID);
			ps.setLong(1, newsId);
				
			rs = ps.executeQuery();
			while(rs.next()){
				comment = new Comment(rs.getLong("COMMENT_ID"), rs.getLong("NEWS_ID"), rs.getString("COMMENT_TEXT"), rs.getDate("CREATION_DATE"));
				
				comments.add(comment);
			}
			
		} catch (SQLException e) {
			throw new DAOException("Error while getNewsComments()", e);
		} finally {
			ConnectionUtil.close(ps, conn, dataSource);
		}
		
		return comments;
	}
}
