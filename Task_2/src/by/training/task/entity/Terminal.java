package by.training.task.entity;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Nik on 29.09.2015.
 */
public class Terminal {
    private enum State{FREE, ENGAGED}

    private int id;
    private Plain currentPlain;
    private State state = State.FREE;
    private Semaphore semaphore;
    private Lock lock = new ReentrantLock();

    public Terminal(){

    }

    public Terminal(int id){
        this.id = id;
        this.semaphore = new Semaphore(1);
    }

    public Terminal(Plain currentPlain) {
        this.currentPlain = currentPlain;
    }

    public int getId(){
        return this.id;
    }

    public Plain getCurrentPlain() {
        return currentPlain;
    }

    public State getState() {
        return state;
    }

    public void removeCurrentPlain(){
        lock.lock();
        currentPlain = null;
        state = State.FREE;
        lock.unlock();
    }

    public void setCurrentPlain(Plain plain){
        lock.lock();
        currentPlain = plain;
        state = State.ENGAGED;
        lock.unlock();
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public void setSemaphore(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public String toString() {
        return "Terminal{" +
                "id=" + id +
                ", state=" + state +
                '}';
    }
}
