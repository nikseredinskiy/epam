package com.epam.task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.task.dao.AuthorDAO;
import com.epam.task.entity.Author;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;

@Service
public class AuthorService {
	
	@Autowired
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public Long createAuthor(Author author) throws ServiceExeption{
		Long id = null;
		try {
			 id = this.authorDAO.create(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while createAuthor() " + e, e);
		}
		
		return id;
	}
	
	public Long updateAuthor(Author author) throws ServiceExeption{
		Long id = null;
		try {
			 id = this.authorDAO.update(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while updateAuthor() " + e, e);
		}
		
		return id;
	}
	
	public void deleteAuthor(Author author) throws ServiceExeption{
		try {
			 this.authorDAO.delete(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteAuthor() " + e, e);
		}
		
	}
	
	public List<Author> getAllAuthors() throws ServiceExeption{
		List<Author> authorList = null;
		try {
			authorList =  this.authorDAO.getAllAuthors();
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllAuthors()", e);
		}
		
		return authorList;
	}
	
	public Author getNewsAuthor(Long newsId) throws ServiceExeption{
		try {
			return this.authorDAO.getNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsAuthor()", e);
		}
	}
	
	public void insertNewsAuthor(Long newsId, Author author) throws ServiceExeption{
		try {
			this.authorDAO.insertNewsAuthor(newsId, author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	
	public void deleteNewsAuthor(Long newsId) throws ServiceExeption{
		try {
			this.authorDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsAuthor()", e);
		}
	}
}
