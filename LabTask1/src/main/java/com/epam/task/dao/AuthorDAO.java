package com.epam.task.dao;

import java.util.List;

import com.epam.task.entity.Author;
import com.epam.task.exception.DAOException;

public interface AuthorDAO {
	/**
	 * @param author
	 * @return generated author id
	 * @throws DAOException
	 */
	Long create(Author author) throws DAOException;

	/**
	 * 
	 * @param author
	 * @return author id
	 * @throws DAOException
	 */
	Long update(Author author) throws DAOException;
	
	/**
	 * 
	 * @param author
	 * @throws DAOException
	 */
	void delete(Author author) throws DAOException;
	
	/**
	 * 
	 * @return list of all authors
	 * @throws DAOException
	 */
	List<Author> getAllAuthors() throws DAOException;
	
	/**
	 * Insert link between news and author in NEWS_AUTHOR
	 * 
	 * @param newsId
	 * @param author
	 * @throws DAOException
	 */
	void insertNewsAuthor(Long newsId, Author author) throws DAOException;

	/**
	 * Delete link between news and author from NEWS_AUTHOR
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsAuthor(Long newsId) throws DAOException;

	/**
	 * @param newsId
	 * @return News author
	 * @throws DAOException
	 */
	Author getNewsAuthor(Long newsId) throws DAOException;
}
