package com.epam.task.dao;

import java.util.List;

import com.epam.task.entity.Comment;
import com.epam.task.exception.DAOException;

public interface CommentDAO {
	/**
	 * @param comment
	 * @return generated comment id
	 * @throws DAOException
	 */
	Long add(Comment comment) throws DAOException;

	/**
	 * 
	 * @param comment
	 * @return comment id
	 * @throws DAOException
	 */
	Long update(Comment comment) throws DAOException;
	
	/**
	 * @param comment
	 * @throws DAOException
	 */
	void delete(Comment comment) throws DAOException;

	/**
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsComments(Long newsId) throws DAOException;

	/**
	 * @param newsId
	 * @return Comments list of news
	 * @throws DAOException
	 */
	List<Comment> getNewsComments(Long newsId) throws DAOException;
}
