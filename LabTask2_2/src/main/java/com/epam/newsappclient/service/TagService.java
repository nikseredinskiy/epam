package com.epam.newsappclient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsappclient.dao.TagDAO;
import com.epam.newsappclient.entity.Tag;
import com.epam.newsappclient.exception.DAOException;
import com.epam.newsappclient.exception.ServiceExeption;

@Service
public class TagService {
	
	@Autowired
	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public Long createTag(Tag tag) throws ServiceExeption{
		Long id;
		try {
			id = tagDAO.createTag(tag);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while create()", e);
		}
		
		return id;
	}
	
	public Long updateTag(Tag tag) throws ServiceExeption{
		Long id;
		try {
			id = tagDAO.update(tag);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while update()", e);
		}
		
		return id;
	}
	
	public void deleteTag(Long tagId) throws ServiceExeption{
		try {
			tagDAO.deleteNTByTagId(tagId);
			tagDAO.delete(tagId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}
	
	public void insertNewsTag(Long newsId, List<Tag> tags) throws ServiceExeption{
		try {
			tagDAO.insertNewsTag(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public void updateNewsTag(Long newsId, List<Tag> tags) throws ServiceExeption{
		try {
			tagDAO.updateNewsTag(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public List<Tag> getNewsTag(Long newsId) throws ServiceExeption{
		try {
			List<Tag> tags = tagDAO.getNewsTag(newsId);
			
			return tags;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsTag()", e);
		}
	}
	

	public void deleteNewsTag(Long newsId) throws ServiceExeption{
		try {
			tagDAO.deleteNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsTag()", e);
		}
	}
	
	public List<Tag> getAllTags() throws ServiceExeption{
		try {
			List<Tag> tags = tagDAO.getAllTags();
			
			return tags;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllTags()", e);
		}
	}

}
