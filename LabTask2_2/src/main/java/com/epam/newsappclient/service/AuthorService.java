package com.epam.newsappclient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsappclient.dao.AuthorDAO;
import com.epam.newsappclient.entity.Author;
import com.epam.newsappclient.exception.DAOException;
import com.epam.newsappclient.exception.ServiceExeption;

@Service
public class AuthorService {
	
	@Autowired
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public Long createAuthor(Author author) throws ServiceExeption{
		Long id;
		try {
			 id = authorDAO.createAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while create() " + e, e);
		}
		
		return id;
	}
	
	public long updateAuthor(Author author) throws ServiceExeption{
		Long id;
		try {
			 id = authorDAO.updateAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while updateAuthor() " + e, e);
		}
		
		return id;
	}
	
	public void expireAuthor(Author author) throws ServiceExeption{
		try {
			 authorDAO.expireAuthor(author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while updateAuthor() " + e, e);
		}
	}
	
	public Author getNewsAuthor(long newsId) throws ServiceExeption{
		try {
			Author author = authorDAO.getNewsAuthor(newsId);
			
			return author;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsAuthor()", e);
		}
	}
	
	public void insertNewsAuthor(long newsId, Author author) throws ServiceExeption{
		try {
			authorDAO.insertNewsAuthor(newsId, author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	public void updateNewsAuthor(long newsId, Author author) throws ServiceExeption{
		try {
			authorDAO.updateNewsAuthor(newsId, author);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
	}
	
	
	public void deleteNewsAuthor(long newsId) throws ServiceExeption{
		try {
			authorDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsAuthor()", e);
		}
	}
	
	public List<Author> getAllAuthors() throws ServiceExeption{
		try{
			List<Author> list = authorDAO.getAllAuthors();
			
			return list;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllAuthors()", e);
		}
	}
}
