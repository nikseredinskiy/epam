package by.training.task.entity;

import by.training.task.exception.DiscException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Nik on 22.09.2015.
 */
public class Disc implements Serializable{

    private final int CAPACITY = 4700;
    private int freeSpace;
    private ArrayList<Song> songs;
    private int length;

    public Disc(){
        this.freeSpace = CAPACITY;
    }

    public int getCapacity() {
        return CAPACITY;
    }

    public int getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(int freeSpace) {
        this.freeSpace = freeSpace;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }

    @Override
    public String toString() {
        return "Disc " +
                ", capacity=" + CAPACITY +
                ", freeSpace=" + freeSpace +
                ", songs: " + '\n' + songs +
                '\n' + "Full length: " + length +
                "min";
    }

}
