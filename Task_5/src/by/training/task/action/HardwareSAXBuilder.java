package by.training.task.action;

import by.training.task.entity.Hardware;
import org.xml.sax.XMLReader;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Nik on 20.10.2015.
 */
public class HardwareSAXBuilder {

        static Logger logger = Logger.getLogger(HardwareSAXBuilder.class);

    private Set<Hardware> hardwares;
    private HardwareHandler hardwareHandler;
    private XMLReader reader;

    public HardwareSAXBuilder(){
        hardwareHandler = new HardwareHandler();
        try{
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(hardwareHandler);
        } catch (SAXException e) {
            logger.error("SAXParser error", e);
        }
    }

    public Set<Hardware> getHardwares(){
        return hardwares;
    }

    public void buildSetHardwares(String fileName){
        try{
            reader.parse(fileName);
        } catch (SAXException e) {
            logger.error("SAXParser error", e);
        } catch (IOException e) {
            logger.error("I/O error", e);
        }
        hardwares = hardwareHandler.getHardwares();
    }
}
