package by.training.task.entity;

/**
 * Created by Nik on 23.09.2015.
 */
public class FolkSong extends Song {
    private String country;
    private int year;

    public FolkSong(){

    }

    public FolkSong(String name, String genre, int length, int size, String format, String country, int year){
        super(name, genre, length, size, format);
        this.country = country;
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", " + '\'' + country + '\'' +
                ", " + year;
    }
}
