package com.epam.newsapp.dao;

import java.util.List;

import com.epam.newsapp.entity.Author;
import com.epam.newsapp.exception.DAOException;

public interface AuthorDAO {
	/**
	 * @param author
	 * @return generated author id
	 * @throws DAOException
	 */
	long createAuthor(Author author) throws DAOException;
	
	Long updateAuthor(Author author) throws DAOException;	

	/**
	 * Insert link between news and author in NEWS_AUTHOR
	 * 
	 * @param newsId
	 * @param author
	 * @throws DAOException
	 */
	void insertNewsAuthor(long newsId, Author author) throws DAOException;
	
	void expireAuthor(Author author) throws DAOException;

	void updateNewsAuthor(long newsId, Author author) throws DAOException;
	
	/**
	 * Delete link between news and author from NEWS_AUTHOR
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsAuthor(long newsId) throws DAOException;

	/**
	 * @param newsId
	 * @return News author
	 * @throws DAOException
	 */
	Author getNewsAuthor(long newsId) throws DAOException;
	
	/**
	 * 
	 * @return List of all news
	 * @throws DAOException
	 */
	List<Author> getAllAuthors() throws DAOException;
}
