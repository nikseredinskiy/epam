<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">

	<tiles:putAttribute name="title">
		Page <c:url value="${currentPage}" />|News Portal
	</tiles:putAttribute>
	
	<tiles:putAttribute name="body">
		<div class="body">
			<div style="margin: 10px;">
				<form method="get">
					<input type="hidden" name="command" value="searchCriteria">
					<select name="authorId">
						<option value="">Select author</option>
						<c:forEach items="${authors}" var="author">
							<option value="${author.getId() }">
								<c:out value="${author.getName()}" />
							</option>
						</c:forEach>
					</select> <select name="tagId" multiple="multiple" size="1">
						<option value="">Select tags</option>
						<c:forEach items="${tags}" var="tag">
							<option value="${tag.getId() }">
								<c:out value="${tag.getName()}" />
							</option>
						</c:forEach>
					</select> <input type="hidden" name="page" value="1"> <input
						type="submit" value="Filter" />
				</form>
				<form method="post" action="/NewsClient/" id="reset-form">
					<input type="hidden" name="command" value="emptyCommand"> <input
						type="submit" form="reset-form" value="Reset" />
				</form>

				<c:forEach items="${newsList}" var="newsVO">
					<p>
						<b> <c:out value="${newsVO.getNews().getTitle()}" />
						</b> (by
						<c:out value="${newsVO.getAuthor().getName()}" />
						) <i> <c:out value="Creation date: " /> <c:out
								value="${newsVO.getNews().getCreationDate()}" />
						</i>
					</p>
					<p>
						<c:out value="${newsVO.getNews().getShortText()}" />
					</p>
					<p>
						<c:forEach items="${newsVO.getTags()}" var="tag">
							<i> <c:out value="${tag.getName()}" /> <c:out value="; " />
							</i>
						</c:forEach>
						<c:out value="Comments(" />
						<c:out value="${newsVO.getComments().size()}" />
						<c:out value=")" />

						<a
							href="news?command=viewFullNews&page=${currentPage}&newsId=${newsVO.getNews().getId()}">View
							Full News</a>

					</p>
					<p>----</p>
				</c:forEach>

				<c:if test="${operation == 'viewNews'}">
					<c:forEach var="page" begin="1" end="${pageCount}">
						<a href="home?command=viewNews&page=${page}"><c:out
								value="${page}" /></a>
					</c:forEach>
				</c:if>

				<c:if test="${operation == 'searchResult'}">
					<p>
						<c:forEach var="page" begin="1" end="${pageCount}">
							<form id="form${page}" method="GET"
								style='float: left; margin-right: 4px;'>
								<input type="hidden" name="command" value="searchCriteria">
								<input type="hidden" name="authorId" value="${authorId}">
								<c:forEach items="${tagId}" var="temp">
									<input type="hidden" name="tagId" value="${temp}">
								</c:forEach>
								<input type="hidden" name="page" value="${page}"> <a
									href="#"
									onclick="document.getElementById('form${page}').submit(); return false;">
									<c:out value="${page}" />
								</a>
							</form>
						</c:forEach>
					</p>
				</c:if>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>