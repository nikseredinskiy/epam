package com.epam.newsapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.dao.CommentDAO;
import com.epam.newsapp.entity.Comment;
import com.epam.newsapp.exception.DAOException;
import com.epam.newsapp.exception.ServiceExeption;


@Service
public class CommentService {
	
	@Autowired
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public long addComment(Comment comment) throws ServiceExeption{
		long id;
		
		try {
			id = this.commentDAO.addComment(comment);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while add()", e);
		}
		
		return id;
	}
	
	public void deleteComment(long commentId) throws ServiceExeption{
		try {
			this.commentDAO.deleteComment(commentId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}
	
	
	public void deleteNewsComment(long newsId) throws ServiceExeption{
		try {
			this.commentDAO.deleteNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while deleteNewsComent()", e);
		}
	}
	
	public List<Comment> getNewsComments(long newsId) throws ServiceExeption{
		try {
			return this.commentDAO.getNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsComments()", e);
		}
	}
}
