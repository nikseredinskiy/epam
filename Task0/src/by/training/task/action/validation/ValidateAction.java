package by.training.task.action.validation;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by Nik on 14.10.2015.
 */
public class ValidateAction {
    public static void validate(){
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        String fileName = "data/hardware.xml";
        String schemaName = "data/hardware.xsd";
        SchemaFactory schemaFactory = SchemaFactory.newInstance(language);

        File schemaLocation = new File(schemaName);
        try{
            Schema schema = schemaFactory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);

        } catch (SAXException e) {
        } catch (IOException e) {
        }
    }
}
