package com.epam.newsappclient.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsappclient.dao.NewsDAO;
import com.epam.newsappclient.entity.News;
import com.epam.newsappclient.entity.SearchCriteria;
import com.epam.newsappclient.exception.DAOException;
import com.epam.newsappclient.exception.ServiceExeption;

@Service
public class NewsService {
	
	@Autowired
	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public long insertNews(News news) throws ServiceExeption {
		long id;
		try {
			id = newsDAO.insertNews(news);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while insert()", e);
		}
		
		return id;
	}

	public long updateNews(News news) throws ServiceExeption {
		news.setModifitationDate(new Date());
		try {
			long newsId = newsDAO.updateNews(news);
			
			return newsId;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while update()", e);
		}
	}

	
	public void deleteNews(long newsId) throws ServiceExeption {
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e) {
			throw new ServiceExeption("Error while delete()", e);
		}
	}

	public List<News> getAllNews() throws ServiceExeption {
		try {
			List<News> news = newsDAO.getAllNews();
			return news;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getAllNews()", e);
		}
	}

	public News getNews(long newsId) throws ServiceExeption {
		try {
			News news = newsDAO.getNews(newsId);
			
			return news;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNews()", e);
		}
	}

	public List<News> getNewsByCommCount() throws ServiceExeption {
		try {
			List<News> news = newsDAO.getNewsByCommCount();
			
			return news;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsByCommCount()", e);
		}
	}

	public int countAllNews() throws ServiceExeption {
		try {
			int count = newsDAO.countAllNews();
			
			return count;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while countAllNews()", e);
		}
	}
	
	public List<News> getNewsBySeacrh(SearchCriteria searchCriteria, long pageNum, long newsOnPage) throws ServiceExeption{
		try {
			List<News> news = newsDAO.getNewsBySeacrh(searchCriteria, pageNum, newsOnPage);
			
			return news;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsBySearch()", e);
		}
	}
	
	public int countNewsBySeacrh(SearchCriteria searchCriteria) throws ServiceExeption{
		try {
			int count = newsDAO.countNewsBySeacrh(searchCriteria);
			
			return count;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while countNewsBySearch()", e);
		}
	}
	
	public List<News> getNewsOnPage(long pageNum, long newsOnPage) throws ServiceExeption{
		try {
			List<News> news = newsDAO.getNewsOnPage(pageNum, newsOnPage);
			return news;
		} catch (DAOException e) {
			throw new ServiceExeption("Error while getNewsOnPage()", e);
		}
	}

}
