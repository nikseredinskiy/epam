package com.epam.task.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task.dao.impl.CommentDAOImpl;
import com.epam.task.entity.Comment;
import com.epam.task.exception.DAOException;
import com.epam.task.exception.ServiceExeption;
import com.epam.task.util.TestUtil;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {
	private static Logger logger = Logger.getLogger(CommentServiceTest.class);
	
	private CommentDAOImpl commentDAO;
	
	@Autowired
	private CommentService commentService;
	
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	
	@Before
	public void setupMock(){
		commentDAO = mock(CommentDAOImpl.class);
		
		commentService.setCommentDAO(commentDAO);
	}
	
	@Test
	public void addTest(){
		Comment comment = TestUtil.createComment(1L, 1L);
		try {
			when(commentDAO.add(comment)).thenReturn(comment.getId());
		} catch (DAOException e) {
			e.printStackTrace();
		}
		try {
			assertEquals(commentService.addComment(comment), comment.getId());
		} catch (ServiceExeption e) {
			logger.error("Error while addTest()", e);
		}
	}
	
	@Test
	public void getNewsCommentsTest(){
		List<Comment> commList1 = new ArrayList<Comment>();
		commList1.add(TestUtil.createComment(1L, 1L));
		commList1.add(TestUtil.createComment(2L, 1L));
		
		List<Comment> commList2 = new ArrayList<Comment>();
		commList2.add(TestUtil.createComment(3L, 2L));
		
		try {
			when(commentDAO.getNewsComments(1L)).thenReturn(commList1);
			when(commentDAO.getNewsComments(2L)).thenReturn(commList2);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		
		try {
			assertEquals(commentService.getNewsComments(1L), commList1);
		} catch (ServiceExeption e) {
			logger.error("Error while getNewsCommentsTest()", e);
		}
	}
}
