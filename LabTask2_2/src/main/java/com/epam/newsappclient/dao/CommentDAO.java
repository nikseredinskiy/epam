package com.epam.newsappclient.dao;

import java.util.List;

import com.epam.newsappclient.entity.Comment;
import com.epam.newsappclient.exception.DAOException;

public interface CommentDAO {
	/**
	 * @param comment
	 * @return generated comment id
	 * @throws DAOException
	 */
	long addComment(Comment comment) throws DAOException;
	
	/**
	 * @param commentId
	 * @throws DAOException
	 */
	void deleteComment(long commentId) throws DAOException;

	/**
	 * @param newsId
	 * @throws DAOException
	 */
	void deleteNewsComments(long newsId) throws DAOException;

	/**
	 * @param newsId
	 * @return Comments list of news
	 * @throws DAOException
	 */
	List<Comment> getNewsComments(long newsId) throws DAOException;
}
