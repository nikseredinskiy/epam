package com.epam.newsapp.entity;

public class User {
	private long id;
	private String userName;
	private String login;
	private String password;
	
	public User(){
		
	}
	
	public User(long id, String userName, String login, String password) {
		super();
		this.id = id;
		this.userName = userName;
		this.login = login;
		this.password = password;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
}
