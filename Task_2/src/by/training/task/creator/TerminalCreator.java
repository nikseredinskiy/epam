package by.training.task.creator;

import by.training.task.entity.Terminal;

import java.util.ArrayList;

/**
 * Created by Nik on 29.09.2015.
 */
public class TerminalCreator {
    public ArrayList<Terminal> createTerminal(){
        ArrayList<Terminal> terminals = new ArrayList<Terminal>();

        terminals.add(new Terminal(0));
        terminals.add(new Terminal(1));

        return terminals;
    }
}
