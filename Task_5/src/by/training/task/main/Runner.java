package by.training.task.main;

import by.training.task.action.HardwareDOMBuilder;
import by.training.task.action.HardwareSAXBuilder;
import by.training.task.action.HardwareStAXBuilder;
import by.training.task.action.ValidateAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Nik on 14.10.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        ValidateAction.validate();

       /* HardwareSAXBuilder saxBuilder = new HardwareSAXBuilder();
        saxBuilder.buildSetHardwares("data/hardware.xml");
        logger.debug(saxBuilder.getHardwares());*/

        /*HardwareDOMBuilder domBuilder = new HardwareDOMBuilder();
        domBuilder.buildSetHardwares("data/hardware.xml");
        logger.debug(domBuilder.getHardwares());*/

        HardwareStAXBuilder stAXBuilder = new HardwareStAXBuilder();
        stAXBuilder.buildSetHardwares("data/hardware.xml");
        logger.debug(stAXBuilder.getHardwares());
    }
}
